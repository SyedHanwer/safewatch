<!--header start here-->

<header class="header">

    <div class="container head_bg">

        <nav class="navbar navbar-expand-lg navbar-light">

            <a class="navbar-brand new-brand" href="{{ url('/') }}">

                <img alt="logo" src="{{ asset('public/theme/images/Safewatch Logo_White.svg')}}">

            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

                <span class="navbar-toggler-icon"></span>

            </button>

            <div class="collapse navbar-collapse" id="navbarNav">

                <ul class="navbar-nav ml-auto">

                @guest

                    <li class="nav-item">

                        <a class="nav-link active" href="{{ url('/') }}">home</a>

                    </li>

                @else

                    <li class="nav-item">

                        <a class="nav-link active" href="{{ url('/home') }}">Home</a>

                    </li>

                @endguest

                    <li class="nav-item">

                        <a class="nav-link" href="#">Support</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link btn-pink" href="{{ route('contactus') }}">Contact us</a>

                    </li>

                    @guest

                    <li class="nav-item">

                        <a class="nav-link btn-yellow" href="{{ route('login') }}">{{ __('Login') }}</a>

                    </li>

                    @else

                    <li class="nav-item">

                        <a class="nav-link btn-yellow" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                            @csrf

                        </form>

                    </li>

                    @endguest

                </ul>

            </div>

        </nav>

    </div>

</header>

<!--header ends here-->