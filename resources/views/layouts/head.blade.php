<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <title>{{ config('app.name', 'SafeWatch') }}</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
  <!-- Bootstrap Core CSS -->
  <link href="{{ asset('public/theme/css/bootstrap.css')}}" rel="stylesheet">
  <!-- Custom CSS -->
 
  <link href="{{ asset('public/theme/css/aos.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/css/animate.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/css/hover.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/css/style.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/css/responsive.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/css/fonts.css')}}" rel="stylesheet">
  <link href="{{ asset('public/theme/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
{{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.8.23/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>--}}
    <script src="{{ asset('public/theme/js/jquery.min.js')}}"></script>


    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('public/theme/js/owl.carousel.js')}}"></script>

</head>
