<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
<script src="{{asset('public/theme/js/aos.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js" type="text/javascript"></script>

<script>
    AOS.init();
    $('.editbtn-input').on('click', function(){
		$('.safewatch-form').addClass('active').siblings().removeClass('active');
	});
	$('.savebtn').on('click', function(){
		$('.safewatch-form').removeClass('active');
	});

</script>

