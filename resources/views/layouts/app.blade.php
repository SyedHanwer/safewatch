<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--include head start here-->

 @include("layouts.head")
<!--include head start end-->
<body>
    <!--header start here-->
    @include("layouts.header")
    <!--header ends here-->
    <div id="app">
        @yield('content')
    </div>
    <!--footer start here-->
    @include("layouts.footer")
    <!--footer ends here-->
</body>
@include("layouts.footer-scripts")


</html>
