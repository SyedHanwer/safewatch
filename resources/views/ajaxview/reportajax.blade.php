<div class="userindex-slider">
    <table class="slider-table table table-borderless">
        <thead>
        <tr>
            <th class="table-th">Date</th>
            <th class="table-th">Description</th>
            <th class="table-th">Total Saving</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data['annual_reports']) !=0)
        @foreach($data['annual_reports'] as $row)
        <tr>
            <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
            <td class="w-90">Invoice {{$row->invoice_no}} -&nbsp;&nbsp;{{$row->details}}</td>
            <td class="text-green">£{{$row->saving}}</td>
        </tr>
        @endforeach
        @else
        <tr style="text-align: center;">
            <td colspan=3>There is no data!</td>
        </tr>
        @endif
        </tbody>
    </table>
</div>
