<div class="herotabs-content-in">
    <div class="row">
        <div class="col-lg-12">
            <div class="herotabs-content-left">
                <h2>Clients</h2>
            </div>
            <div class="manage-client-dropdown-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="dropdown-blockin-left">
                            <div class="blockin-header">
                                <h1>Manage Clients</h1>
                                <button type="button" class="hero-btnred newclient" id="newclient">New Client</button>
                            </div>
                            <div class="energy-drop mb-0">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group mb-0">
                                            <select class="form-control selectdata" id="myclient" name="myclient">
                                                <option>My Clients</option>
                                                @if(!empty($data['myclient']))
                                                    @foreach($data['myclient'] as $val)
                                                        <option value="{{ $val->id }}">{{ $val->forename }} {{ $val->surname }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-0">
                                            <select class="form-control selectdata" id="myagent" name="myagent">
                                                <option>My Agent's Clients</option>
                                                @if(!empty($data['myagent']))
                                                    @foreach($data['myagent'] as $val)
                                                        @php
                                                            //get all my agent client
                                                            $myagentClients = \CommonHelper::instance()->myagentClient($val->id);
                                                        @endphp
                                                        @foreach($myagentClients as $agclient)
                                                            <option value="{{ $agclient->id }}">{{ $agclient->forename }} {{ $agclient->surname }}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="clientInfoBox"  style="display: none">
                        <div class="Thompson-Broadley">
                            <div class="broadley-header">
                                <h4 class="name-cls"></h4>
                                <h4>ID: <span class="client_id-cls"></span></h4>
                            </div>
                            <ul class="broadley-ul">
                                <li>
                                    <span class="address1-cls"></span>
                                    <span class="homeno-cls"></span>
                                </li>
                                <li>
                                    <span class="address2-cls"></span>
                                    <span class="mobileno-cls"></span>
                                </li>
                                <li>
                                    <span class="address3-cls"></span>
                                    <br>
                                    <span class="dob-cls"></span>
                                </li>
                                <li>
                                    <span class="postcode-show"></span>
                                    <span class="email-cls"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="userindex-slider bg-none tracking-tabs">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-Info-tab" data-toggle="tab" href="#nav-Info"
                           role="tab" aria-controls="nav-Info" aria-selected="true">Client Info</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-profiletrack-tab" data-toggle="tab"
                           href="#nav-profiletrack" data-view="jobs" role="tab" aria-controls="nav-profiletrack"
                           aria-selected="false">Jobs</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-tracked-tab" data-toggle="tab"
                           href="#nav-tracked" data-view="lhc" role="tab" aria-controls="nav-tracked"
                           aria-selected="false">LHC</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-Invoices-tab" data-toggle="tab"
                           href="#nav-Invoices" data-view="invoices"
                           role="tab" aria-controls="nav-Invoices" aria-selected="false">Invoices</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-report-tab" data-toggle="tab"
                           href="#nav-report" data-view="annual_reports"
                           role="tab"
                           aria-controls="nav-report" aria-selected="false">Annual Reports</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-product-tab" data-toggle="tab"
                           href="#nav-product" data-view="products"
                           role="tab" aria-controls="nav-product" aria-selected="false">Products</a>
                        <a class="nav-item nav-link ajaxViewClient" id="nav-agentcontact-tab" data-toggle="tab"
                           href="#nav-agentcontact" data-view="agentcontact" role="tab" aria-controls="nav-agentcontact"
                           aria-selected="false">Contact</a>
                    </div>
                </nav>
                <div class="tab-content contact-tab" id="nav-tabContent">
                    <div class="tab-pane fade  show active" id="nav-Info" role="tabpanel"
                         aria-labelledby="nav-Info-tab">
                        <div class="userindex-slider newclientclone" id="newclientclone">
                            <form action="#" method="POST" class="safewatch-form clientfrm" id="clientfrm" >
                                <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="forename">Forename <span class="star">*</span></label>
                                            <input type="text" class="form-control client forename-cls" tabindex="1" id="forename" name="forename" value="" required autocomplete="forename" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="surname">Surname <span class="star">*</span></label>
                                            <input type="text" class="form-control client surname-cls" tabindex="2" id="surname" name="surname" value="" required autocomplete="surname" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="dob">D.O.B</label>
                                            <input type="text" class="form-control client dob-cls datepicker" placeholder="Enter D.O.B" tabindex="11" id="dob" name="dob" value="" required autocomplete="dob" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="client_id">Agent <span class="star">*</span></label>
                                            <input type="text" class="form-control client client_id-cls" tabindex="12" id="client_id" name="client_id" value="" required autocomplete="dob" autofocus>
                                            <input type="hidden" class="form-control client id-cls" placeholder="" id="id" name="id" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="border-line"></div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="address1">Address <span class="star">*</span></label>
                                            <input type="text" class="form-control client address1-cls" tabindex="3" id="address1" name="address1" value="" required autocomplete="address1" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="home_no">Landline</label>
                                            <input type="text" class="form-control client homeno-cls" tabindex="7" id="home_no" name="home_no" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group mine-checkbox lt-txt">
                                            <label for="preferred_contact">Preferred Contact Methods</label>
                                            <div class="new pb">
                                                <!-- <div class="form-group">
                                                   <label for="Mobile">Mobile</label>

                                                   <input type="checkbox" id="Mobile">

                                                   </div>

                                                   <div class="form-group">

                                                   <label for="Email">Email</label>

                                                   <input type="checkbox" id="Email">

                                                   </div>

                                                   <div class="form-group">

                                                   <label for="Post">Post</label>

                                                   <input type="checkbox" id="Post">

                                                   </div> -->
                                                <div class="form-group">
                                                    <label class="formfield"><strong>Mobile</strong>
                                                        <input type="checkbox" class="checkedmark client" id="preferredMobile" name="preferred[]" value="Mobile">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="formfield"><strong>Email</strong>
                                                        <input type="checkbox" class="checkedmark client" id="preferredEmail" name="preferred[]" value="Email">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="formfield"><strong>Post</strong>
                                                        <input type="checkbox" class="checkedmark client" id="preferredPost" name="preferred[]" value="Post">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 d-none">
                                        <div class="form-group">
                                            <label for="dateoffirstcontact">Date of First Contact</label>
                                            <input type="text" class="form-control client firstcontact datepicker" placeholder="Date of First Contact" id="dateoffirstcontact" name="dateoffirstcontact" value="" autocomplete="dateoffirstcontact" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="address2">Town/City</label>
                                            <input type="text" class="form-control client address2-cls" tabindex="4" id="address2" name="address2" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="mobile_no">Mobile<span class="star">*</span></label>
                                            <input type="text" class="form-control client mobileno-cls" tabindex="8" id="mobile_no" name="mobile_no" value="" required autocomplete="mobile_no" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- <div class="form-group">
                                           <label for="preferred_contact">Membership Contact Methods</label>
                                           <input type="text" class="form-control client preferred_contact" placeholder="-" id="preferred_contact" name="preferred_contact" value="">
                                           </div>-->
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="address3">County</label>
                                            <input type="text" class="form-control client address3-cls" tabindex="5" id="address3" name="address3" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="email">Email <span class="star">*</span></label>
                                            <input type="email" class="form-control client email-cls" tabindex="9" id="email" name="email" value="" required autocomplete="email" autofocus>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="member_label w-100 meb-txt" for="membershiptype">Membership Type <span class="star">*</span></label>
                                        <div class="new brdr">
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input membershiptype" name="membershiptype" value="Free">Free
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input membershiptype" name="membershiptype" value="Paid">Paid
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 d-none">
                                        <div class="form-group">
                                            <label for="preferred_contact">Membership Contact Methods</label>
                                            <input type="text" class="form-control client preferred_contact" placeholder="-" id="preferred_contact" name="preferred_contact" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group pt">
                                            <label for="postcode">Postcode</label>
                                            <input type="text" class="form-control client postcode-cls" tabindex="6" id="postcode" name="postcode" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group pt">
                                            <label for="referredby">Referred by</label>
                                            <input type="text" class="form-control client referredby" tabindex="10" id="referredby" name="referredby" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group pt">
                                            <label for="dateofmembersince">SafeWatch Member Since</label>
                                            <input type="text" class="form-control client membersince datepicker" tabindex="13" placeholder="Select Date" id="dateofmembersince" name="dateofmembersince" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 d-none">
                                        <div class="border-line"></div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea type="text" class="form-control client notes-cls" placeholder="" id="notes" name="notes"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="button" class="hero-btnred editbtn-input" data-loginid="{{ Auth::user()->id}}" id="editbtn">Edit</button>
                                        <button type="button" class="hero-btnred submitbtn" id="updatebtn" data-val="update" style="display:none;">Update</button>
                                        <button type="button" class="hero-btnred submitbtn" id="savebtn" data-val="save" style="display:none;">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="nav-profiletrack" role="tabpanel" aria-labelledby="nav-profiletrack-tab">
                        <div class="jobsajax-cls" id="jobs"></div>
                    </div>
                    <div class="tab-pane fade" id="nav-tracked" role="tabpanel" aria-labelledby="nav-tracked-tab">
                        <div class="lhcajax-cls" id="lhc"></div>
                    </div>
                    <div class="tab-pane fade" id="nav-Invoices" role="tabpanel" aria-labelledby="nav-Invoices-tab">
                        <div class="invoicesajax-cls" id="invoices"></div>
                    </div>
                    <div class="tab-pane fade" id="nav-report" role="tabpanel" aria-labelledby="nav-report-tab">
                        <div class="annual_reportsajax-cls" id="annual_reports"></div>
                    </div>
                    <div class="tab-pane fade" id="nav-product" role="tabpanel" aria-labelledby="nav-product-tab">
                        <div class="productsajax-cls" id="products"></div>
                    </div>
                    <div class="tab-pane fade" id="nav-agentcontact" role="tabpanel" aria-labelledby="nav-agentcontact-tab">
                        <div class="agentcontactajax-cls" id="agentcontact"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.nav-link', function(){
        var text = $(this).text();
        if(text == 'Client Info'){
            $('#newclient').show();
        }else{
            $('#newclient').hide();
        }
    });

    var keyupstatus = 1;
    $(document).on('click', '#newclient', function (e) {
        $('#clientfrm').trigger("reset");
        $('#editbtn').hide();
        $('#updatebtn').hide();
        $('#client_id').val('{{Auth::user()->forename}} {{Auth::user()->surname}}');
        $('#savebtn').show();
        keyupstatus = 2;
    });
    $('.client').keyup(function () {
        if (keyupstatus == 1) {
            showbtn();
        }
    });
    $(function () {
        $('.checkedmark').click(function () {
            var val = [];
            $(':checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            $('.preferred_contact').val(val);
            if (keyupstatus == 1) {
                showbtn();
            }
        });
        $('.membershiptype').click(function () {
            if (keyupstatus == 1) {
                showbtn();
            }
        });

        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "1900:2030",
            dateFormat: 'dd/mm/yy'
        });
    });
    function showbtn() {
        $('#editbtn').hide();
        $('#savebtn').hide();
        $('#updatebtn').show();
    }

    $('#clientfrm').on('click', '.submitbtn', function (e) {
        e.preventDefault();
        // var frmdata = $('#clientfrm').serializeArray();
        var data = {};
        data['status'] = $(this).attr("data-val");
        data['forename'] = $('#forename').val();
        data['surname']  = $('#surname').val();
        data['client_id'] = $('#client_id').val();
        data['id'] = $('#id').val();
        data['dob'] = $('#dob').val();
        data['address1'] = $('#address1').val();
        data['home_no'] = $('#home_no').val();
        data['dateoffirstcontact'] = $('#dateoffirstcontact').val();
        data['address2'] = $('#address2').val();
        data['mobile_no'] = $('#mobile_no').val();
        data['dateofmembersince'] = $('#dateofmembersince').val();
        data['address3'] = $('#address3').val();
        data['email'] =$('#email').val();
        data['preferred_contact'] = $('#preferred_contact').val();
        data['postcode'] = $('#postcode').val();
        data['referredby'] = $('#referredby').val();
        data['membershiptype'] = $("input[name='membershiptype']:checked").val();
        data['notes'] = $('#notes').val();
        if(data != ''){
            $.ajax({
                type: "post",
                async: true,
                cache: false,
                url: "{{route('clientupdate')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    data: data,
                },
                dataType: "json",
                success: function(res) {
                    if($.isEmptyObject(res.error)){
                        $(".print-error-msg").css('display','none');
                        if(res.status == 'update'){
                            toastr.success('Success!', 'Data Updated Successfully!');
                        }else if(res.status == 'save'){
                            $('#clientfrm').trigger("reset");
                            setTimeout(function() {
                                $('a[href="#pills-home3"]').trigger("click");
                            }, 6000);
                            toastr.success('Success!', 'Data Added Successfully!');
                        }else{
                            toastr.error('error!', 'Some Problem Here. Try Again!');
                        }
                    }else{
                        printErrorMsg(res.error);
                        toastr.error('error!', 'Some Fields are Empty. Please Fill!');
                    }
                }
            });
            e.stopImmediatePropagation();
            return false;
        }
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }

    $('#clientfrm').on('click', '#editbtn', function(e) {
        var id = $(this).attr('data-loginid');
        if (id != '') {
            $.ajax({
                type: "post",
                url: "{{route('viewclient')}}",
                dataType: "json",
                data: {
                    '_token': "{{csrf_token()}}",
                    id: id,
                },
                success: function (res) {
                    console.log(res.data.preferred_contact);
                    if (res.status == 'success') {
                        $('.client_id-cls').text(res.data.client_id);
                        $('.name-cls').text(res.data.forename + ' ' + res.data.surname);
                        $('.address1-cls').text(res.data.address1);
                        $('.homeno-cls').text(res.data.home_no);
                        $('.address2-cls').text(res.data.address2);
                        $('.mobileno-cls').text(res.data.mobile_no);
                        $('.address3-cls').text(res.data.address3);
                        $('.postcode-show').text(res.data.postcode);
                        $('.dob-cls').text(res.data.dob);
                        $('.email-cls').text(res.data.email);
                        $('.client_id-cls').val(res.data.client_id);
                        $('.forename-cls').val(res.data.forename);
                        $('.surname-cls').val(res.data.surname);
                        $('.id-cls').val(res.data.id);
                        $('.dob-cls').val(res.data.dob);
                        $('.address1-cls').val(res.data.address1);
                        $('.homeno-cls').val(res.data.home_no);
                        $('.firstcontact').val(res.data.dateoffirstcontact);
                        $('.address2-cls').val(res.data.address2);
                        $('.mobileno-cls').val(res.data.mobile_no);
                        $('.membersince').val(res.data.dateofmembersince);
                        $('.address3-cls').val(res.data.address3);
                        $('.email-cls').val(res.data.email);
                        $('.postcode-cls').val(res.data.postcode);
                        $('.referredby').val(res.data.referredby);
                        $('.preferred_contact').val(res.data.preferred_contact);
                        $('.notes-cls').val(res.data.notes);
                        $('input:radio[name="membershiptype"][value="' +res.data.membershiptype+'"]').prop('checked', true);
                        if (res.data.preferred_contact && res.data.preferred_contact.length > 0) {
                            var preferredArr = res.data.preferred_contact.split(',');
                            preferredArr.forEach(function(item) {
                                if(item ==='Mobile'){
                                    $("#preferredMobile").prop("checked", true);
                                }else if(item ==='Email'){
                                    $("#preferredEmail").prop("checked", true);
                                }else if(item ==='Post'){
                                    $("#preferredPost").prop("checked", true);
                                }
                            });
                        }
                        $('#editbtn').hide();
                        $('#savebtn').hide();
                        $('#updatebtn').show();
                        keyupstatus = 1;
                    }else{
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
        }
    });


    $(".selectdata").change(function() {
        $('#editbtn').hide();
        $('#updatebtn').show();
        $('#savebtn').hide();
        keyupstatus = 1;
        var id = $(this).val();
        if (id != '') {
            $.ajax({
                type: "post",
                url: "{{route('viewclient')}}",
                dataType: "json",
                data: {
                    '_token': "{{csrf_token()}}",
                    id: id,
                },
                success: function (res) {
                    if (res.status == 'success') {
                        $('#clientInfoBox').show();
                        $("#preferredMobile").prop("checked", false);
                        $("#preferredEmail").prop("checked", false);
                        $("#preferredPost").prop("checked", false);

                        $('.client_id-cls').text(res.data.client_id);
                        $('.name-cls').text(res.data.forename + ' ' + res.data.surname);
                        $('.address1-cls').text(res.data.address1);
                        $('.homeno-cls').text(res.data.home_no);
                        $('.address2-cls').text(res.data.address2);
                        $('.mobileno-cls').text(res.data.mobile_no);
                        $('.address3-cls').text(res.data.address3);
                        $('.postcode-show').text(res.data.postcode);
                        $('.dob-cls').text(res.data.dob);
                        $('.email-cls').text(res.data.email);
                        $('.client_id-cls').val(res.data.client_id);
                        $('.forename-cls').val(res.data.forename);
                        $('.surname-cls').val(res.data.surname);
                        $('.id-cls').val(res.data.id);
                        $('.dob-cls').val(res.data.dob);
                        $('.address1-cls').val(res.data.address1);
                        $('.homeno-cls').val(res.data.home_no);
                        $('.firstcontact').val(res.data.dateoffirstcontact);
                        $('.address2-cls').val(res.data.address2);
                        $('.mobileno-cls').val(res.data.mobile_no);
                        $('.membersince').val(res.data.dateofmembersince);
                        $('.address3-cls').val(res.data.address3);
                        $('.email-cls').val(res.data.email);
                        $('.postcode-cls').val(res.data.postcode);
                        $('.referredby').val(res.data.referredby);
                        $('.preferred_contact').val(res.data.preferred_contact);
                        $('.notes-cls').val(res.data.notes);
                        $('input:radio[name="membershiptype"][value="' +res.data.membershiptype+'"]').prop('checked', true);
                        if (res.data.preferred_contact && res.data.preferred_contact.length > 0) {
                            var preferredArr = res.data.preferred_contact.split(',');
                            preferredArr.forEach(function(item) {
                                if(item ==='Mobile'){
                                    $("#preferredMobile").prop("checked", true);
                                }else if(item ==='Email'){
                                    $("#preferredEmail").prop("checked", true);
                                }else if(item ==='Post'){
                                    $("#preferredPost").prop("checked", true);
                                }
                            });
                        }
                    }else{
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
        }
    });

    $(document).on('click', '.ajaxViewClient', function(e) {
        e.preventDefault();
        var view = $(this).attr("data-view");
        var id = $('#id').val();
        $.LoadingOverlay("show");
        if(view != ''){
            $.ajax({
                type: "post",
                async: true,
                url: "{{route('ajaxViewClient')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    view: view,
                    id: id
                },
                success: function(data) {
                    $('#'+view).html(data.html);
                    $.LoadingOverlay("hide");
                },error: function() {
                    $.LoadingOverlay("hide");
                    toastr.warning('Alert!', 'Loading Problem. Try Again!');
                }
            });
            e.stopImmediatePropagation();
            return false;
        }
    });
</script>