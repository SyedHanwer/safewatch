<div class="container herotabs-content-in">
    <div class="row">
        <div class="col-lg-12">
            <div class="user-index">
                <h2>Manage</h2>
                <h4 class="subtitle">Monthly Membership Income</h4>
                <div class="membership-month">
                    <div class="orange-box">Clients</div>
                    <ul>
                        <li>
                            <div class="membership-income-box">
                                <h4 class="text-main">Personal</h4>
                                <?php
                                $clitentTotal = Auth::User()->where('role', 3)->where('membershiptype', 'Paid')->where('parent_id',Auth::user()->id)->count();
                                $agents = Auth::User()->where('role', 2)->where('parent_id',Auth::user()->id)->get();
                                $agentTotal = $agents->count();
                                $tAgentPaidClient = 0;
                                foreach ($agents as $val){
                                    $agentPaidClient = Auth::User()->where('role', 3)
                                        ->where('membershiptype', 'Paid')
                                        ->where('parent_id', $val->id)
                                        ->count();
                                    $tAgentPaidClient = $tAgentPaidClient +$agentPaidClient;
                                }
                                $incomeClient = $clitentTotal*4;
                                $incomeAgent = $tAgentPaidClient*1;
                                $incomeMonthly = $incomeClient+$incomeAgent;
                                ?>
                                <p>{{$clitentTotal}}</p>
                            </div>
                            <div class="membership-income-box">
                                <h4 class="text-main">Income</h4>
                                <p>£{{$incomeClient}}</p>
                            </div>
                        </li>
                        <li>
                            <div class="membership-income-box">
                                <h4 class="text-main">Agents</h4>
                                <p>{{$agentTotal}}</p>
                            </div>
                            <div class="membership-income-box">
                                <h4 class="text-main">Income</h4>
                                <p>£{{$incomeAgent}}</p>
                            </div>
                        </li>
                        <li>
                            <div class="membership-income-box income-text-right">
                                <h4 class="text-main">Monthly Income</h4>
                                <p class="text-green">£{{$incomeMonthly}}</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <h4 class="subtitle">Commissions</h4>
                <div class="userindex-slider">
                    <table class="slider-table table table-borderless">
                        <thead>
                        <tr>
                            <th class="table-th">Date</th>
                            <th class="table-th">Invoice Number </th>
                            <th class="table-th">Client</th>
                            <th class="table-th">Agent</th>
                            <th class="table-th">Invoice Total </th>
                            <th class="table-th">Commission</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalcomm = 0;
                        ?>
                        @if(count($data['commission']) != 0)
                            @foreach($data['commission'] as $val)
                                <?php
                                    if(!empty($val->commission) && is_numeric($val->commission)){
                                        $totalcomm = $totalcomm + $val->commission;
                                    }
                                ?>
                                <tr class="rowcls">
                                    <td>{{date('d/m/Y', strtotime($val->created_at))}}</td>
                                    <td>{{$val->invoice_no}}</td>
                                    @if(!empty($val->forename) && $val->role ==3)
                                        <td>{{$val->forename}} {{$val->surname}}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    @if(!empty($val->forename) && $val->role ==2)
                                        <td>{{$val->forename}} {{$val->surname}}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td class="text-green">£{{$val->invoiceTotalAmount}}</td>
                                    <td class="text-green">£{{$val->commission}}
                                        <button type="button" class="view-red commInvoicePreView" data-invoiceID="{{$val->invoice_id}}" data-toggle="modal" data-target="#invoicePreViewModal">view</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="running-total">
                        <ul class="running-total-in">
                            <li>
                                <p>Running total</p>
                            </li>
                            <li>
                                <p class="text-green" id="runtotal">£{{ $totalcomm }}</p>
                                <button type="button" id="requestbtn" data-comm="{{$totalcomm}}" class="Request-btn">Request</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <h4 class="subtitle">Commission History</h4>
                <div class="userindex-slider">
                    <table class="slider-table table table-borderless">
                        <thead>
                        <tr>
                            <th class="table-th">Date required</th>
                            <th class="table-th">Date paid</th>
                            <th class="table-th">Commission</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($data['commission_history']->created_at))
                        <tr>
                            <td class="w-txt">{{ date('d/m/Y', strtotime($data['commission_history']->created_at)) }}</td>
                            <td class="w-txt">@if(!empty($data['commission_history']->date_paid)){{ date('d/m/Y', strtotime($data['commission_history']->date_paid)) }} @endif</td>
                            <td class="text-green w-txt" id="commSumid">£ @if(!empty($data['commissionSum']))
                                    {{$data['commissionSum']}} @endif
                                <button class="view-red">view</button>
                            </td>
                        </tr>
                        @else
                            <tr style="text-align: center;"><td colspan="3">Commission History Not Found!</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <h4 class="subtitle" >
                    <span>Manage Agents</span>
                    <button class="btn-red" id="newagentbtn">New Agent</button>
                </h4>
                <div class="new-agent" id="new-agent" style="display: none;">
                    <div class="userindex-slider newclientclone" id="newclientclone">
                        <form action="#" method="POST" class="safewatch-form clientfrm" id="clientfrm" >
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="forename">Forename</label>
                                        <input type="text" class="form-control client forename-cls" placeholder="" id="forename" name="forename" value="" required>
                                        <span style="color: red; display: none;" id="fornamerequired">Forename is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="surname">Surname</label>
                                        <input type="text" class="form-control client surname-cls" placeholder="" id="surname" name="surname" value="" required>
                                        <span style="color: red; display: none;" id="surnamerequired">Surename is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="id">Agent ID</label>
                                        <input type="text" class="form-control client client_id-cls" placeholder="" id="client_id" name="client_id" value="" readonly>
                                        <input type="hidden" class="form-control client id-cls" placeholder="" id="id" name="id" value="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="dob">D.O.B</label>
                                        <input type="text" class="form-control client dob-cls datepickerM" placeholder="" id="dob" name="dob" value="" required>
                                        <span style="color: red; display: none;" id="dobrequired">DateOfBirth is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-top:15px;">
                                    <div class="border-line"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address1">Address</label>
                                        <input type="text" class="form-control client address1-cls" placeholder="" id="address1" name="address1" value="" required>
                                        <span style="color: red; display: none;" id="addressrequired">Address is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_no">Home Number</label>
                                        <input type="text" class="form-control client homeno-cls" placeholder="" id="home_no" name="home_no" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="dateoffirstcontact">Date of First Contact</label>
                                        <input type="text" class="form-control client firstcontact datepickerM" placeholder="" id="dateoffirstcontact" name="dateoffirstcontact" value="" required>
                                        <span style="color: red; display: none;" id="dofrequired">Date Of First Contract is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address2">Town/City</label>
                                        <input type="text" class="form-control client address2-cls" placeholder="" id="address2" name="address2" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="mobile_no">Mobile Number</label>
                                        <input type="text" class="form-control client mobileno-cls" placeholder="" id="mobile_no" name="mobile_no" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="referredby">Referred by</label>
                                        <input type="text" class="form-control client referredby" placeholder="" id="referredby" name="referredby" value="{{Auth::User()->forename}} {{Auth::User()->surname}}" readonly>
                                        <span style="color: red; display: none;" id="refrequired">Referred by is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address3">County</label>
                                        <input type="text" class="form-control client address3-cls" placeholder="" id="address3" name="address3" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control client email-cls" placeholder="" id="email" name="email" value="" required>
                                        <span style="color: red; display: none;" id="emailrequired">Email is Required!.</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="hero-btnred submitbtn" id="savebtn" data-val="save">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="edit-agent" id="edit-agent" style="display: none;margin-top: 15px;">
                    <div class="manage-client-dropdown-block" style="display: none;" id="agentviewdiv">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="Thompson-Broadley">
                                    <div class="broadley-header">
                                        <h4 class="name-cls" id="agentname"></h4>
                                        <h4>ID: <span class="client_id-cls" id="agentId"></span></h4>
                                    </div>
                                    <ul class="broadley-ul">
                                        <li>
                                            <span class="address1-cls" id="agentaddress1"></span>
                                            <span class="homeno-cls" id="agenthome"></span>
                                        </li>
                                        <li>
                                            <span class="address2-cls" id="agentaddress2"></span>
                                            <span class="mobileno-cls" id="agentno"></span>
                                        </li>
                                        <li>
                                            <span class="address3-cls" id="agentaddress3"></span>
                                            <br>
                                            <span class="dob-cls" id="agentdob">DOB</span>
                                        </li>
                                        <li>
                                            <span class="postcode-show" id="agentPost"></span>
                                            <span class="email-cls" id="agentemail"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="userindex-slider editclientclone" id="editclientclone">
                        <form action="#" method="POST" class="safewatch-form clientfrm" id="agentfrm" >
                            <input type="hidden"  id="txtagentid" name="txtagentid" value="">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="forename">Forename</label>
                                        <input type="text" class="form-control client forename-cls" placeholder="" id="forename1" name="forename" value="" required>
                                        <span style="color: red; display: none;" id="forenamerequired">Forename is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="surname">Surname</label>
                                        <input type="text" class="form-control client surname-cls" placeholder="" id="surname1" name="surname" value="" required>
                                        <span style="color: red; display: none;" id="surnamerequired1">Surname is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="id">Agent ID</label>
                                        <input type="text" class="form-control client client_id-cls" placeholder="" id="client_id1" name="client_id" value="" readonly>
                                        <input type="hidden" class="form-control client id-cls" placeholder="" id="id1" name="id" value="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="dob">D.O.B</label>
                                        <input type="text" class="form-control client dob-cls datepickerM" placeholder="" id="dob1" name="dob" value="" required>
                                        <span style="color: red; display: none;" id="dobrequired1">DateOfBirth is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="border-line"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address1">Address Line 1</label>
                                        <input type="text" class="form-control client address1-cls" placeholder="" id="address11" name="address1" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="home_no">Home Number</label>
                                        <input type="text" class="form-control client homeno-cls" placeholder="" id="home_no1" name="home_no" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="dateoffirstcontact">Date of First Contact</label>
                                        <input type="text" class="form-control client firstcontact datepickerM" placeholder="" id="dateoffirstcontact1" name="dateoffirstcontact" value="" required>
                                            <span style="color: red; display: none;" id="dofrequired1">Date Of First Contract is Required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address2">Address Line 2</label>
                                        <input type="text" class="form-control client address2-cls" placeholder="" id="address21" name="address2" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="mobile_no">Mobile Number</label>
                                        <input type="text" class="form-control client mobileno-cls" placeholder="" id="mobile_no1" name="mobile_no" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
               				<div class="form-group">
                                        	<label for="preferred_contact">Membership Contact Methods</label>
                                        	<input type="text" class="form-control client preferred_contact" placeholder="" id="preferred_contact1" name="preferred_contact" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="address3">Address Line 3</label>
                                        <input type="text" class="form-control client address3-cls" placeholder="" id="address31" name="address3" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control client email-cls" placeholder="" id="email1" name="email" value="" required>
                                        <span style="color: red; display: none;" id="emailrequired1">Email Address is Required.</span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group mine-checkbox">
                                        <label for="preferred_contact">Preferred Contact Methods</label>
                                        <div class="new">
                                            <div class="form-group">
                                                <label class="formfield"><strong>Mobile</strong>
                                                    <input type="checkbox" class="checkedmark client" id="preferredcontact1" name="preferred[]" value="Mobile">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="formfield"><strong>Email</strong>
                                                    <input type="checkbox" class="checkedmark client" id="preferredcontact1" name="preferred[]" value="Email">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="formfield"><strong>Post</strong>
                                                    <input type="checkbox" class="checkedmark client" id="preferredcontact1" name="preferred[]" value="Post">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="postcode">Postcode</label>
                                        <input type="text" class="form-control client postcode-cls" placeholder="" id="postcode1" name="postcode" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="referredby">Referred by</label>
                                        <input type="text" class="form-control client referredby" placeholder="" id="referredby1" name="referredby" value="" required >
                                        <span style="color: red; display: none;" id="reffrequired1">Referred by is Required!</span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="border-line"></div>
                                </div>
                                <div class="col-lg-12 mt-3">
                                    <div class="form-group">
                                        <label for="notes">Notes</label>
                                        <textarea type="text" class="form-control client notes-cls" placeholder="" id="notes1" name="notes"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="hero-btnred updatebtn" id="updatebtn" data-val="update" >Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <h4 class="subtitle"></h4>
                <div class="userindex-slider">
                    <table class="slider-table table table-borderless">
                        <thead>
                        <tr>
                            <th class="table-th">Agent</th>
                            <th class="table-th alg-txt">No of Clients</th>
                            <th class="table-th w-txt">No of Agents </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data['myagent']) != 0)
                            @foreach($data['myagent'] as $val)
                                <tr class="rowcls{{$val->id}}">
                                    <td>{{$val->forename}} {{$val->surname}}</td>
                                    <td class="pll">
                                        {{ Auth::User()->where('parent_id',$val->id)->where('role',3)->count() }}
                                    </td>
                                    <td class="plll">
                                        {{ Auth::User()->where('parent_id',$val->id)->where('role',2)->count() }}
                                    </td>
                                    <td class="text-green w-txt2">
                                        <button type="button" class="view-red agentview" data-val="{{$val->id}}">view</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <h4 class="subtitle">
                    <span>Cashback Tracking</span>
                </h4>
                <div class="userindex-slider bg-none tracking-tabs">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link ajaxViewCash active" id="nav-hometrack-tab" data-toggle="tab"
                               href="#nav-hometrack" data-view="NotYetTracked" role="tab" aria-controls="nav-hometrack"
                               aria-selected="true">Not yet tracked</a>
                            <a class="nav-item nav-link ajaxViewCash" id="nav-profiletrack1-tab"
                               data-toggle="tab" href="#nav-profiletrack1" data-view="ClaimRaised" role="tab"
                               aria-controls="nav-profiletrack1" aria-selected="false">Claim raised</a>
                            <a class="nav-item nav-link ajaxViewCash" id="nav-tracked1-tab" data-toggle="tab"
                               href="#nav-tracked1" data-view="Tracked" role="tab" aria-controls="nav-tracked1"
                               aria-selected="false">Tracked</a>
                            <a class="nav-item nav-link ajaxViewCash" id="nav-paid1-tab" data-toggle="tab"
                               href="#nav-paid1" data-view="Paid"  role="tab" aria-controls="nav-paid1"
                               aria-selected="false">Paid</a>
                            <a class="nav-item nav-link ajaxViewCash" id="nav-delete1-tab" data-toggle="tab"
                               href="#nav-delete1" data-view="CashDeleted" role="tab" aria-controls="nav-delete1" aria-selected="false">Deleted</a>
                        </div>
                    </nav>
                    <div class="tab-content contact-tab" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-hometrack" role="tabpanel" aria-labelledby="nav-hometrack-tab">
                            <div class="NotYetTracked-cls" id="NotYetTracked"></div>
                        </div>

                        <div class="tab-pane fade" id="nav-profiletrack1" role="tabpanel" aria-labelledby="nav-profiletrack1-tab">
                            <div class="ClaimRaised-cls" id="ClaimRaised"></div>
                        </div>

                        <div class="tab-pane fade" id="nav-tracked1" role="tabpanel" aria-labelledby="nav-tracked1-tab">
                            <div class="Tracked-cls" id="Tracked"></div>
                        </div>

                        <div class="tab-pane fade" id="nav-paid1" role="tabpanel" aria-labelledby="nav-paid1-tab">
                            <div class="Paid-cls" id="Paid"></div>
                        </div>

                        <div class="tab-pane fade" id="nav-delete1" role="tabpanel" aria-labelledby="nav-delete1-tab">
                            <div class="CashDeleted-cls" id="CashDeleted"></div>
                        </div>

                    </div>
                </div>
                <h4 class="subtitle">
                    <span>Energy Suppliers</span>
                </h4>
                <div class="energy-drop">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group mb-0">
                                <select class="form-control" id="energySelect">
                                    <option value="All">All</option>
                                    @if(count($data['supplier']) !=0)
                                        @foreach($data['supplier'] as $val)
                                            <option value="{{$val->name}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="userindex-slider">
                    <table class="slider-table table table-borderless" id="energytbl">
                        <thead>
                        <tr>
                            <th class="table-th">Energy Type</th>
                            <th class="table-th">Provider</th>
                            <th class="table-th">Benefits</th>
                            <th class="table-th">End Date</th>
                            <th class="table-th">Cancel Fee</th>
                            <th class="table-th">Annual Usage</th>
                            <th class="table-th">LHC Invoice</th>
                        </tr>
                        </thead>
                        <tbody class="energyScls" id="energyScls"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- The Modal start -->
<div class="modal agnt-model" id="invoicePreViewModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div id="showloaderPreview">
                <div class="modal-body-Jobs" id="modal-body-Jobs" style="display: none;">
                    <form class="safewatch-form job-forms" id="jobfrmModel" name="jobfrm" method="post" action="#">
                        <div class="item job-txt">
                            <div class="userindex-slider">
                                <div class="slider-header">
                                    <div class="title">
                                        <p id="invoiceNoJob"></p>
                                        <p id="invoiceDateJob"></p>
                                    </div>
                                    <div class="job-txt-heading">
                                        <h4>Services</h4>
                                    </div>
                                </div>
                                <table class="slider-table table table-borderless">
                                    <thead>
                                    <tr>
                                        <th class="table-th">Service</th>
                                        <th class="table-th">Hrs/Units</th>
                                        <th class="table-th">Normal</th>
                                        <th class="table-th">SafeWatch</th>
                                        <th class="table-th">Saving</th>
                                        <th class="table-th">ToPay</th>
                                    </tr>
                                    </thead>
                                    <tbody id="invoiceBodyJob"></tbody>
                                </table>
                                <div class="saved-year">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="savedyear-block">
                                                <div class="savedyear-blockin">
                                                    <p class="text-main" >Total saved</p>
                                                    <p class="box-green" id="invoiceTSavedJob"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="savedyear-block">
                                                <div class="savedyear-blockin">
                                                    <p>Total to pay</p>
                                                    <p id="invoiceTAmountJob"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-body-Lhc" id="modal-body-Lhc" style="display: none;">
                    <form class="safewatch-form job-forms " action="#" method="post" id="lhcfrmmodel">
                        <div class="item job-txt">
                            <div class="userindex-slider">
                                <div class="slider-header">
                                    <div class="title">
                                        <p id="invoiceNoLhc">Invoices </p>
                                        <p id="invoicesDateLhc"></p>
                                    </div>
                                    <div class="job-txt-heading">
                                        <h4>Lifestyle Healthcheck</h4>
                                    </div>
                                </div>
                                <table class="slider-table table table-borderless">
                                    <thead>
                                    <tr>
                                        <th class="table-th">Item</th>
                                        <th class="table-th">Provider</th>
                                        <th class="table-th">Benefits/Package</th>
                                        <th class="table-th">End Date</th>
                                        <th class="table-th">Monthly</th>
                                        <th class="table-th">Term</th>
                                        <th class="table-th">Saving (term)</th>
                                    </tr>
                                    </thead>
                                    <tbody id="invoiceBodyLhc"></tbody>
                                </table>
                                <div class="saved-year">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="savedyear-block">
                                                <div class="savedyear-blockin">
                                                    <p class="text-main">Total Saved</p>
                                                    <p class="box-green" id="invoiceTSavingLhc"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="savedyear-block">
                                                <div class="savedyear-blockin">
                                                    <p>Members (25% fee)</p>
                                                    <p class="invoiceTAmountLhcM"></p>
                                                </div>
                                                <div class="savedyear-blockin">
                                                    <p>Non-members (50% fee)</p>
                                                    <p class="invoiceTAmountLhcNonM"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer ftr">
                <div class="tw0-btn"></div>
                <button type="button" class="btnnred" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- The Modal end -->
<script>
    $(document).ready(function(){

        $('.datepickerM').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "1900:2030",
            dateFormat: 'dd/mm/yy'
        });

        $('.commInvoicePreView').click(function(){
            $('#showloaderPreview').LoadingOverlay("show", {
                image       : "",
                fontawesome : "fa fa-cog fa-spin"
            });
            var invoiceID = $(this).attr("data-invoiceID");
            $.ajax({
                type: "post",
                async: true,
                dataType: "json",
                url: "{{route('commInvoicePreView')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    invoiceID: invoiceID,
                },
                success: function(res) {
                    $('#showloaderPreview').LoadingOverlay("hide", true);
                    var invoice_type = res.data.invoices.invoice_type;
                    if(invoice_type =='Job'){
                        $('#modal-body-Jobs').show();
                        $('#modal-body-Lhc').hide();
                        $('#invoiceNoJob').text('Invoices '+res.data.invoices.invoice_no);
                        $('#invoiceDateJob').text(res.data.invoices.inv_date);
                        $('#invoiceTSavedJob').text('£ '+res.data.invoices.saving);
                        $('#invoiceTAmountJob').text('£ '+res.data.invoices.total_amount);
                        var html = '';
                        for (var i=0; i<res.data.data.length; i++) {
                            var job = res.data.data[i];
                            if(job.id !=''){
                                html +='<tr><td>'+job.details+'</td><td>'+job.units+'</td><td class="text-pink">£'+job.standard+'</td><td class="text-green">£'+job.member+'</td><td class="text-green">£'+job.saving+'</td><td>£'+job.amount+'</td></tr>';
                            }else{
                                html +='<tr style="text-align: center"><td colspan="6">Data Not Found!</td></tr>';
                            }
                        }
                        $('#invoiceBodyJob').html(html);
                    }
                    if(invoice_type =='LHC'){
                        $('#modal-body-Lhc').show();
                        $('#modal-body-Jobs').hide();
                        $('#invoiceNoLhc').text('Invoices '+res.data.invoices.invoice_no);
                        $('#invoicesDateLhc').text(res.data.invoices.inv_date);
                        $('#invoiceTSavingLhc').text('£ '+res.data.invoices.saving);
                        $('.invoiceTAmountLhcM').text('£ '+res.data.invoices.profit);
                        $('.invoiceTAmountLhcNonM').text('£ '+res.data.invoices.nonMember);
                        var html = '';
                        for (var i=0; i<res.data.data.length; i++) {
                            var lhc = res.data.data[i];

                            var curEndDate = '-';
                            var newEndDate = '-';
                            if(lhc.curenddate !='' && lhc.curenddate !=null){
                                var rs1 = lhc.curenddate.split('-');
                                curEndDate = rs1[2]+'/'+rs1[1]+'/'+rs1[0];
                            }
                            if(lhc.newenddate !='' && lhc.newenddate !=null){
                                var rs2 = lhc.newenddate.split('-');
                                newEndDate = rs2[2]+'/'+rs2[1]+'/'+rs2[0];
                            }

                            if(lhc.id !=''){
                                html +='<tr><td>'+lhc.sname+'</td><td><p class="text-pink">'+lhc.curprovider+'</p><p class="text-main">'+lhc.newprovider+'</p></td><td><p class="text-pink">-</p><p class="text-main">£'+lhc.newsaving+'</p></td><td><p class="text-pink">'+curEndDate+'</p><p class="text-main">'+newEndDate+'</p></td><td><p class="text-pink">£'+lhc.curmonthlycost+'</p><p class="text-main">£'+lhc.newmonthlycost+'</p></td><td><p class="text-pink">£'+lhc.curtotalover+'</p><p class="text-main">£'+lhc.newtotalover+'</p></td><td><p class="text-pink"></p><p class="text-main">£'+lhc.newsaving+'</p></td></tr>';
                            }else{
                                html +='<tr style="text-align: center"><td colspan="7">There is not Data!</td></tr>';
                            }
                        }
                        $('#invoiceBodyLhc').html(html);
                    }
                },error: function() {
                    $('#showloaderPreview').LoadingOverlay("hide", true);
                    toastr.warning('Alert!', 'Loading Problem. Try Again OR Page Reload!');
                }
            });
        });
    });


    $('#newagentbtn').click(function(e){
        e.preventDefault();
        $("#clientfrm")[0].reset();
        $('#agentviewdiv').hide();
        $("#edit-agent").hide('slow');
        $("#new-agent").slideToggle('slow');
    });
    $(document).on('click', '.view-green', function(e) {
        var id = $(this).attr("data-val");
        $.ajax({
            type: "get",
            async: true,
            cache: false,
            url: "{{route('editegent')}}",
            data: {
                '_token': "{{csrf_token()}}",
                id: id,
            },
            success: function (res) {

                $(".edit-agent").show();
                $("#agentid").val(res.id);
                $("#forename1").val(res.forename);
                $("#surname1").val(res.surname);
                $("#client_id1").val(res.client_id);
                $("#dob1").val(res.dob);
                $("#address11").val(res.address1);
                $("#home_no1").val(res.home_no);
                $("#dateoffirstcontact1").val(res.dateoffirstcontact);
                $("#address21").val(res.address2);
                $("#mobile_no1").val(res.mobile_no);
                //$("#dateofmembersince1").val(res.dateofmembersince);
                $("#address31").val(res.address3);
                $("#email1").val(res.email);
                $("#preferred_contact1").val(res.preferred_contact);
                $("#postcode1").val(res.postcode);
                $("#referredby1").val(res.referredby);
                $("#notes1").val(res.notes);
            }

        });
        e.stopImmediatePropagation();
        return false;
    });

    $(document).on('click', '#requestbtn', function (e){
        e.preventDefault();
        var tcomm = $(this).attr("data-comm");
        if(tcomm !=0) {
            $.ajax({
            type: "post",
            async: true,
            cache: false,
            url: "{{route('commrequest')}}",
            data: {
                '_token': "{{csrf_token()}}",
                tcomm: tcomm,
            },
            dataType: "json",
                success: function (res) {

                    if (res.status == 'success') {
                        $(".rowcls").fadeOut("slow");
                        $("#runtotal").text("");
                        $("#commSumid").text('£ '+res.commissionSum);
                        toastr.success('Success!', 'Commission Request Added Successfully!');
                    } else {
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
            e.stopImmediatePropagation();
            return false;
        }else{
            toastr.error('error!', 'Sorry! Running Total is Zero.');
            return false;
        }
    });

    $(document).on('click', '.submitbtn', function(e) {
        var data = {};
        data['status'] = $(this).attr("data-val");
        var forename = $(this).closest("div.newclientclone").find("input[name=forename]").val();
        if(forename == ''){
            $('#fornamerequired').show();        
            return false;
        }else{
            data['forename'] = forename;
            $('#fornamerequired').hide();
        }
        var surname  = $(this).closest("div.newclientclone").find("input[name=surname]").val();
        if(surname == ''){
            $('#surnamerequired').show();        
            return false;
        }else{
            data['surname'] = surname;
            $('#surnamerequired').hide();
        }

        var dob = $(this).closest("div.newclientclone").find("input[name=dob]").val();
        if(dob == ''){
            $('#dobrequired').show();
            return false;
        }else{
            data['dob'] = dob;
            $('#dobrequired').hide();
        }
        var address1 = $(this).closest("div.newclientclone").find("input[name=address1]").val();
        if(address1 == ''){
            $('#addressrequired').show();
            return false;
        }else{
            data['address1'] = address1;
            $('#addressrequired').hide();
        }
        data['home_no'] = $(this).closest("div.newclientclone").find("input[name=home_no]").val();
        var dateoffirstcontact =$(this).closest("div.newclientclone").find("input[name=dateoffirstcontact]").val();
        if(dateoffirstcontact == ''){
            $('#dofrequired').show();
            return false;
        }else{
            data['dateoffirstcontact'] = dateoffirstcontact;
            $('#dofrequired').hide();
        }
        data['address2'] = $(this).closest("div.newclientclone").find("input[name=address2]").val();
        data['mobile_no'] = $(this).closest("div.newclientclone").find("input[name=mobile_no]").val();
        data['dateofmembersince'] =$(this).closest("div.newclientclone").find("input[name=dateofmembersince]").val();
        data['address3'] = $(this).closest("div.newclientclone").find("input[name=address3]").val();
        data['preferred_contact'] = $(this).closest("div.newclientclone").find("input[name=preferred_contact]").val();
        var email = $(this).closest("div.newclientclone").find("input[name=email]").val();
        if(email == ''){
            $('#emailrequired').show();
            return false;
        }else{
            data['email'] = email;
            $('#emailrequired').hide();
        }

        data['preferred_contact'] = $(this).closest("div.newclientclone").find("input[name=preferred_contact]").val();
        data['postcode'] = $(this).closest("div.newclientclone").find("input[name=postcode]").val();
        var referredby = $(this).closest("div.newclientclone").find("input[name=referredby]").val();
        if(referredby == ''){
            $('#refrequired').show();
           
            return false;
        }else{
            data['referredby'] = referredby;
            $('#refrequired').hide();
        }
        data['notes'] = $("#notes").val();
        if(data != ''){
            $.ajax({
                type: "post",
                async: true,
                cache: false,
                url: "{{route('addagent')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    data: data,
                },
                dataType: "json",
                success: function(res) {
                    if(res.status == 'update'){
                        $("#clientfrm")[0].reset();
                        toastr.success('Success!', 'Data Updated Successfully!');
                    }else if(res.status == 'save'){
                        $("#clientfrm")[0].reset();
                        toastr.success('Success!', 'Data Added Successfully!');
                        $("#clientfrm")[0].reset();
                    }else{
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
            e.stopImmediatePropagation();
            return false;
        }
    });
    $(document).on('click', '.updatebtn', function(e) {
        var data = {};
        data['status'] = 'update';
        data['id'] = $("#txtagentid").val();
        data['forename'] = $("#forename1").val();
        var forename = $('#forename1').val();
        if(forename == ''){
            $('#forenamerequired').show();
            return false;
        }else{
            $('#forenamerequired').hide();
        }
        data['surname']  = $("#surname1").val();
        var surname = $('#surname1').val();
        if(surname == ''){
            $('#surnamerequired1').show();
            return false;
        }else{
            $('#surnamerequired1').hide();
        }
        data['client_id'] =$("#client_id1").val();
        data['dob'] = $("#dob1").val();
        var dob = $('#dob1').val();
        if(dob == ''){
            $('#dobrequired1').show();
            return false;
        }else{
            $('#dobrequired1').hide();
        }
        data['address1'] = $("#address11").val();
        data['home_no'] = $("#home_no1").val();
        data['dateoffirstcontact'] =$("#dateoffirstcontact1").val();
        var dateoffirstcontact = $('#dateoffirstcontact1').val();
        if(dateoffirstcontact == ''){
            $('#dofrequired1').show();
            return false;
        }else{
            $('#dofrequired1').hide();
        }
        data['address2'] = $("#address21").val();
        data['mobile_no'] = $("#mobile_no1").val();
        //data['dateofmembersince'] =$("#dateofmembersince1").val();
        data['address3'] = $("#address31").val();
        data['email'] = $("#email1").val();
        var email = $('#email1').val();
        if(email == ''){
            $('#emailrequired1').show();
            return false;
        }else{
            $('#emailrequired1').hide();
        }
        data['preferred_contact'] = $("#notes1").val();
        data['postcode'] =$("#postcode1").val();
        data['referredby'] =$("#referredby1").val();
        var referredby = $('#referredby1').val();
        if(referredby == ''){
            $('#reffrequired1').show();
            return false;
        }else{
            $('#reffrequired1').hide();
        }
        data['notes'] = $("#notes1").val();
        if(data != ''){
            $.ajax({
                type: "post",
                async: true,
                cache: false,
                url: "{{route('addagent')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    data: data,
                },
                dataType: "json",
                success: function(res) {
                    if(res.status == 'update'){
                        toastr.success('Success!', 'Data Updated Successfully!');
                        $(".edit-agent").hide();
                        $("#agentfrm")[0].reset();
                    }else{
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
            e.stopImmediatePropagation();
            return false;
        }
    });

    $(document).on('click', '.agentview', function (e){
        e.preventDefault();
        var id = $(this).attr("data-val");
        $.ajax({
            type: "post",
            async: true,
            cache: false,
            url: "{{route('agentview')}}",
            data: {
                '_token': "{{csrf_token()}}",
                id: id,
            },
            dataType: "json",
            success: function (res) {
                if (res.status == 'success') {

                    $("#agentviewdiv").show('slow');
                    $("#edit-agent").show('slow');
                    $("#txtagentid").val(res.data.id);
                    $('.client_id-cls').text(res.data.client_id);
                    $('.name-cls').text(res.data.forename + ' ' + res.data.surname);
                    $('.address1-cls').text(res.data.address1);
                    $('.homeno-cls').text(res.data.home_no);
                    $('.address2-cls').text(res.data.address2);
                    $('.mobileno-cls').text(res.data.mobile_no);
                    $('.address3-cls').text(res.data.address3);
                    $('.postcode-show').text(res.data.postcode);
                    $('.dob-cls').text(res.data.dob);
                    $('.email-cls').text(res.data.email);
                    $('.client_id-cls').val(res.data.client_id);
                    $('.forename-cls').val(res.data.forename);
                    $('.surname-cls').val(res.data.surname);
                    $('.id-cls').val(res.data.id);
                    $('.dob-cls').val(res.data.dob);
                    $('.address1-cls').val(res.data.address1);
                    $('.homeno-cls').val(res.data.home_no);
                    $('.firstcontact').val(res.data.dateoffirstcontact);
                    $('.address2-cls').val(res.data.address2);
                    $('.mobileno-cls').val(res.data.mobile_no);
                    $('.membersince').val(res.data.dateofmembersince);
                    $('.address3-cls').val(res.data.address3);
                    $('.email-cls').val(res.data.email);
                    $('.postcode-cls').val(res.data.postcode);
                    $('.referredby').val(res.data.referredby);
                    $('.preferred_contact').val(res.data.preferred_contact);
                    $('.notes-cls').val(res.data.notes);

                }else{
                    $('#agentviewdiv').hide();
                    toastr.error('error!', 'Some Problem Here. Try Again!');
                }
            }
        });
        e.stopImmediatePropagation();
        return false;
    });

    $(document).on('click', '.view-yellow', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        console.log(id);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('agentdelete')}}",
                        data: {
                            '_token': "{{csrf_token()}}",
                            id: id
                        },
                        success: function (data) {
                            if (data == 1) {
                                toastr.success('Success!', 'Data Deleted Successfully!');
                                $(".rowcls" + id).fadeOut("slow");
                            } else {
                                swal({
                                    title: "Deleted!",
                                    text: "Data Can't deleted.",
                                    type: "error",
                                    timer: 10000,
                                    showConfirmButton: false
                                });
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Your Record is Safe :)", "error");
                }
            });
    });
    $(function () {
        $('.checkedmark').click(function () {
            var val = [];
            $(':checkbox:checked').each(function (i) {
                val[i] = $(this).val();
            });
            $('.preferred_contact').val(val);
            if (keyupstatus == 0) {
                showbtn();
            }
        });
    });

    $(document).ready(function(){
        energySupplier('All');
        ajaxViewCashBack('NotYetTracked');
    });
    $("#energySelect").change(function() {
        var val = $(this).val();
        energySupplier(val);
    });

    function energySupplier(val){
        var val = val;
        if (val != '') {
            $.ajax({
                type: "post",
                async: true,
                url: "{{route('energySupplier')}}",
                dataType: "json",
                data: {
                    '_token': "{{csrf_token()}}",
                    val: val,
                },
                success: function (res) {
                    console.log(res);
                    if (res.status == 'success') {
                        $('#energyScls').html(res.data);
                    }else{
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }
            });
        }
    }

    $('.ajaxViewCash').click(function(e){
        e.preventDefault();
        var view = $(this).attr("data-view");
        if(view !=''){
            ajaxViewCashBack(view);
        }
    });
    function ajaxViewCashBack(view){
        var view = view;
        $('#'+view).LoadingOverlay("show", {
            image       : "",
            fontawesome : "fa fa-cog fa-spin"
        });
        if(view != ''){
            $.ajax({
                type: "post",
                async: true,
                url: "{{route('ajaxViewCash')}}",
                data: {
                    '_token': "{{csrf_token()}}",
                    view: view,
                },
                success: function(data) {
                    $('#'+view).html(data.html);
                    $('#'+view).LoadingOverlay("hide", true);
                },error: function() {
                    $('#'+view).LoadingOverlay("hide", true);
                    toastr.warning('Alert!', 'Loading Problem. Try Again OR Page Refresh!');
                }
            });
        }
    }

</script>
