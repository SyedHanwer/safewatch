<div class="tab-content contact-tab" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <form class="safewatch-form userindex-slider" id="contactfrm" method="post" action="#">
            <div class="alert alert-danger agcon-error-msg" style="display:none">
                <ul></ul>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" placeholder="" id="subject" name="subject">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="message">Your Message</label>
                        <textarea type="text" class="form-control" placeholder="" id="message" name="message"></textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="formbtn-group">
                        @if(!empty($data['user_id']))
                        <button type="button" id="sendbtn" data-userid="{{ $data['user_id'] }}" class="bg-green">Send</button>
                        @endif
                        <button type="reset" class="bg-red" id="dellbtn">Delete</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
$('#sendbtn').click(function(e) {
    e.preventDefault();
    var userid = $(this).attr("data-userid");
    var subject = $('#subject').val();
    var message  = $('#message').val();
    $.ajax({
        type: "post",
        async: true,
        cache: false,
        url: "{{route('sendmsgtoclient')}}",
        data: {
            '_token': "{{csrf_token()}}",
            userid:userid,
            subject: subject,
            message:message,
        },
        dataType: "json",
        success: function(res) {
            if($.isEmptyObject(res.error)){
                $(".agcon-error-msg").css('display','none');
                if(res.status == 'save'){
                    toastr.success('Success!', 'Thank! Message Send Successfully.');
                    $("#contactfrm")[0].reset();
                }else if(res.status == 'error'){
                    toastr.error("error!", "Sorry! You don\'t select client.");
                }else {
                    toastr.error('error!', 'Some Problem Here. Try Again!');
                }
            }else{
                printErrorMsg(res.error);
                toastr.error('error!', 'Some Fields are Required! Please Fill.');
            }
        }
    });
    e.stopImmediatePropagation();
    return false;
});

function printErrorMsg (msg) {
    $(".agcon-error-msg").find("ul").html('');
    $(".agcon-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".agcon-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}
</script>
