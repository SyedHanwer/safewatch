<div class="userindex-slider">
    @if(!empty($data['user_id']) && count($data['products']) != 0)
    <div class="userindex-slider">
        <form class="safewatch-form product-forms" id="productfrm" name="productfrm" method="post" action="#">
            <div class="alert alert-danger products-error-msg" style="display:none">
                <ul></ul>
            </div>
            @foreach($data['products'] as $val)
	        <div class="row productclone productrow{{$val->id}}">
                <input type="hidden" class="productID" name="productID[]" value="{{$val->id}}">
                <input type="hidden" class="providerID" name="providerID[]" value="{{$val->lid}}">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="services">Service</label>
                        <input type="text" class="form-control services" placeholder="" name="services[]" value="{{$val->name}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="providers">Provider</label>
                        <input type="text" class="form-control providers" placeholder="Enter Provider"  name="providers[]" value="{{$val->newprovider}}" readonly>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="username">User Name</label>
                        <input type="text" class="form-control username" name="username[]" value="{{$val->username?$val->username:''}}" placeholder="">
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" class="form-control password" name="password[]" value="{{$val->pro_password?$val->pro_password:''}}" placeholder="">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="notes">Notes</label>
                        <input type="text" class="form-control notes" name="notes[]" value="{{$val->pro_notes?$val->pro_notes:''}}" placeholder="" >
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="border-line"></div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-12">
                <div class="btn-green-top">
                    <div class="form-group" id="processprod" style="display:none">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="">
                            </div>
                        </div>
                    </div>
                    <button type="button" id="productSave" data-productTo="{{$data['user_id']}}" class="btngreen-save">Save</button>
                </div>
            </div>
        </form>
    </div>
    @endif
</div>
<script>

$(document).on('click', '#productSave', function(e) {
    e.preventDefault();
    //var frmdata = $('#jobfrm').serializeArray();
    var data = {};
    data['productTo'] = $(this).attr("data-productTo");
    data['productID'] = $("input[name='productID[]']").map(function(i, el) { return $(el).val(); }).get();
    data['providerID'] = $("input[name='providerID[]']").map(function(i, el) { return $(el).val(); }).get();
    data['services'] = $("input[name='services[]']").map(function(i, el) { return $(el).val(); }).get();
    data['providers'] = $("input[name='providers[]']").map(function(i, el) { return $(el).val(); }).get();
    data['username'] = $("input[name='username[]']").map(function(){return $(this).val();}).get();
    data['password'] = $("input[name='password[]']").map(function(){return $(this).val();}).get();
    data['notes'] = $("input[name='notes[]']").map(function(){return $(this).val();}).get();
    if(data != ''){
        $.ajax({
            type: "post",
            async: true,
            cache: false,
            url: "{{route('updateproduct')}}",
            data: {
                '_token': "{{csrf_token()}}",
                data: data,
            },
            dataType: "json",
            beforeSend:function()
            {
                $('#productSave').attr('disabled', 'disabled');
                $('#processprod').css('display', 'block');
            },
            success: function(res) {
                if($.isEmptyObject(res.error)){
                    var percentage = 0;
                    var timer = setInterval(function(){
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, res);
                    }, 1000);
                    setTimeout(function() {
                        $('a[href="#nav-product"]').trigger("click");
                    }, 6000);
                }else{
                    productsErrorMsg(res.error);
                    toastr.error('error!', 'Some Fields are Required. Please Fill!');
                    $('#processprod').css('display', 'none');
                    $('#productSave').attr('disabled', false);
                }
            }
        });
        e.stopImmediatePropagation();
        return false;
    }
});

function productsErrorMsg (msg) {
    $(".products-error-msg").find("ul").html('');
    $(".products-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".products-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

function progress_bar_process(percentage, timer, res)
{
    $('.progress-bar').css('width', percentage + '%');
    if(percentage > 100)
    {
        if (res.status == 'save') {
            toastr.success('Success!', 'Data Added Successfully!');
        } else {
            toastr.error('error!', 'Some Problem Here. Try Again!');
        }
        clearInterval(timer);
        $('#processprod').css('display', 'none');
        $('.progress-bar').css('width', '0%');
        $('#productSave').attr('disabled', false);
    }
}

</script>
