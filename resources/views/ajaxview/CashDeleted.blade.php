<div class="userindex-slider">
    <table class="slider-table table table-borderless">
        <thead>
        <tr>
            <th class="table-th">Date</th>
            <th class="table-th">Client name</th>
            <th class="table-th">Type</th>
            <th class="table-th">Company</th>
            <th class="table-th">Details</th>
            <th class="table-th">Cashback </th>
            <th class="table-th">Amount</th>
            <th class="table-th">Action</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data) != 0)
            @foreach($data as $key=>$val)
                <tr class=" row{{$key}}{{$val->id}}">
                    <td>{{date('d/m/Y', strtotime($val->created_at))}}
                        <br><p class="text-sky">(ID: {{$val->id}}), @if(!empty($val->cost)) {{$val->cost}} @endif</p>
                    </td>
                    <td>{{$val->forename}} {{$val->surname}}</td>
                    <td>@if(!empty($val->name)) {{$val->name}} @endif</td>
                    <td>@if(!empty($val->newprovider)) {{$val->newprovider}} @endif</td>
                    <td>@if(!empty($val->details)) {{$val->details}} @endif @if(!empty($val->newdetails)) {{$val->newdetails}} @endif</td>
                    <td>@if(!empty($val->comission_type)) {{$val->comission_type}} @endif @if(!empty($val->newcommissiontype)) {{$val->newcommissiontype}} @endif</td>
                    <td>£@if(!empty($val->amount2)) {{$val->amount2}} @else {{$val->amount}} @endif</td>
                    <td class="text-green">
                        <div class="form-group mb-0">
                            <select class="form-control mydropdown upgradestatus" name="type" data-key="{{$key}}" data-id="{{$val->id}}" data-type="@if(!empty($val->jobno)) JOBS @else LHC @endif">
                                <option value="0">Status</option>
                                <option value="4">Claim raised</option>
                                <option value="3">Tracked</option>
                                <option value="2">Paid</option>
                            </select>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr style="text-align:center;">
                <td colspan="7">There is no data!</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<script>
    $(".upgradestatus").change(function() {
        var key = $(this).attr("data-key");
        var id = $(this).attr("data-id");
        var type = $(this).attr("data-type");
        var status = $(this).val();
        if (status == "") {
            return false;
        }
        $.ajax({
            type: "post",
            url: "{{route('cashBackStatus')}}",
            data: {
                '_token': "{{csrf_token()}}",
                id: id,
                type: type,
                status: status
            },
            success: function(data) {
                if (data == 1) {
                    $(".row"+key+id).empty(".row"+key+id);
                    toastr.success('Success!', 'Status Updated Successfully.');
                }else {
                    toastr.warning('Alert!', 'Some Problem Here. Try Again!');
                }
            }
        });
    });
</script>
