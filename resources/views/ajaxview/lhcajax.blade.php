<?php
$lhcRandno = \CommonHelper::instance()->autoRandomNo('LHC');
$tsaving = 0;
$tamount = 0;
$tnewmonthlycost = 0;
$editInvoiceNo = 0;
$tamountNonM = 0;
?>

<div class="userindex-slider">
    @if(!empty($data['user_id']))
    <form class="safewatch-form job-forms " action="#" method="post" id="lhcfrm">
        <div class="alert alert-danger lhc-error-msg" style="display:none">
            <ul></ul>
        </div>
         {{ csrf_field() }}
        @if(count($data['lhcs']) != 0)
            @foreach($data['lhcs'] as $val)
            <?php
            if(!empty($val->newsaving) && strlen((string)$val->newsaving)){
                $member = 25;
                $nonMember = 50;
                $tsaving = $tsaving + $val->newsaving;
                $tamount = ($member / 100 * $tsaving);
                $tamountNonM = ($nonMember / 100 * $tsaving);
            }
            if(!empty($val->newmonthlycost) && strlen((string)$val->newmonthlycost)){
                $tnewmonthlycost = $tnewmonthlycost + $val->newmonthlycost;
            }
            if(!empty($val->invoice_no) && !empty($val->status) && $val->status ==2){
                $editInvoiceNo = $val->invoice_no;
            }
            ?>
            <div class="row rowcls{{ $val->id }} clonerowcls" >
                <div class="col-lg-1 col-type">
                    <div class="form-group">
                        <input type="hidden" class="saveid" name="saveid[]" value="{{ $val->id ? $val->id:'lhc111' }}">
                        <label for="servicetype">Type</label>
                        <select class="form-control servicetype" name="servicetype[]">
                            <option value="">--Select--</option>
                            @if(!empty($data['services']))
                                @foreach($data['services'] as $key =>$sval)
                                    <option value="{{ $sval->id }}" {{ $sval->id == $val->service_id ? 'selected' : '' }}>{{$sval->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col_term">
                   <div class="inner_term">
                    <div class="form-group">
                        <label for="curprovider">Current Provider</label>
                        <input type="text" class="form-control curprovider" placeholder="" name="curprovider[]" value="{{ $val->curprovider }}">
                    </div>
                    <div class="form-group">
                        <label for="newprovider">New Provider</label>
                        <input type="text" class="form-control newprovider" placeholder="" name="newprovider[]" value="{{ $val->newprovider }}">
                    </div>
                  </div>  
                </div>
                <div class="col-lg-1 col_term">
                   <div class="inner_term"> 
                    <div class="form-group">
                        <label for="curdetails">Details</label>
                        <input type="text" class="form-control curdetails" placeholder="" name="curdetails[]" value="{{ $val->curdetails }}">
                    </div>
                    <div class="form-group">
                        <label for="newdetails">Details</label>
                        <input type="text" class="form-control newdetails" placeholder="" name="newdetails[]" value="{{ $val->newdetails }}">
                    </div>
                  </div>  
                </div>
                <div class="col-lg-1 col_term">
                    <div class="inner_term">
                    <div class="form-group">
                        <label for="curenddate">End Date</label>
                        <input type="text" class="end-date curenddate dateSlash" placeholder="DD/MM/YYYY" name="curenddate[]" value="@if(!empty($val->curenddate)) {{ date('d/m/Y', strtotime($val->curenddate)) }} @endif">
                    </div>
                    <div class="form-group">
                        <label for="newenddate">End Date</label>
                        <input type="text" class="end-date newenddate dateSlash" placeholder="DD/MM/YYYY" name="newenddate[]" value="@if(!empty($val->newenddate)) {{ date('d/m/Y', strtotime($val->newenddate)) }} @endif">
                    </div>
                </div>
                </div>
                <div class="col-lg-1 col_term">
                   <div class="inner_term"> 
                    <div class="form-group usagesDiv">
                        <label for="usages">Usage</label>
                        <input type="text" class="form-control usages calAmtLhc" placeholder="" name="usages[]" value="{{$val->usages }}">
                    </div>
                    <div class="form-group">
                        <label for="term">Term</label>
                        <select class="form-control term calAmtLhc" name="term[]">
                            <option value="12" {{ $val->term == '12' ? 'selected' : '' }}>12</option>
                            <option value="13" {{ $val->term == '13' ? 'selected' : '' }}>13</option>
                            <option value="14" {{ $val->term == '14' ? 'selected' : '' }}
                            >14</option>
                            <option value="15" {{ $val->term == '15' ? 'selected' : '' }}
                            >15</option>
                            <option value="16" {{ $val->term == '16' ? 'selected' : '' }}
                            >16</option>
                            <option value="17" {{ $val->term == '17' ? 'selected' : '' }}
                            >17</option>
                            <option value="18" {{ $val->term == '18' ? 'selected' : '' }}
                            >18</option>
                            <option value="19" {{ $val->term == '19' ? 'selected' : '' }}
                            >19</option>
                            <option value="20" {{ $val->term == '20' ? 'selected' : '' }}
                            >20</option>
                            <option value="21" {{ $val->term == '21' ? 'selected' : '' }}
                            >21</option>
                            <option value="22" {{ $val->term == '22' ? 'selected' : '' }}
                            >22</option>
                            <option value="23" {{ $val->term == '23' ? 'selected' : '' }}
                            >23</option>
                            <option value="24" {{ $val->term == '24' ? 'selected' : '' }}
                            >24</option>
                            <option value="36" {{ $val->term == '36' ? 'selected' : '' }}
                            >36</option>
                        </select>
                    </div>
                </div>
                </div>
                <div class="col-lg-1 col_term">
                  <div class="inner_term">  
                    <div class="form-group">
                        <label for="curmonthlycost">Monthly Cost</label>
                        <input type="text" class="form-control curmonthlycost calAmtLhc" placeholder="" name="curmonthlycost[]" value="{{ $val->curmonthlycost }}">
                    </div>
                    <div class="form-group">
                        <label for="newmonthlycost">Monthly Cost</label>
                        <input type="text" class="form-control newmonthlycost calAmtLhc" placeholder="" name="newmonthlycost[]" value="{{ $val->newmonthlycost }}">
                    </div>
                   </div> 
                </div>
                <div class="col-lg-1 col_term">
                  <div class="inner_term">  
                    <div class="form-group">
                        <label for="curtotalover">Total over term</label>
                        <input type="text" class="form-control curtotalover calTAmtLhc" placeholder="" name="curtotalover[]" value="{{ $val->curtotalover }}">
                    </div>
                    <div class="form-group">
                        <label for="newtotalover">Total over term</label>
                        <input type="text" class="form-control newtotalover calTAmtLhc" placeholder="" name="newtotalover[]" value="{{ $val->newtotalover }}">
                    </div>
                  </div>  
                </div>
                <div class="col-lg-1 col_term">
                   <div class="inner_term"> 
                    <div class="form-group">
                        <label for="curcancelfee">Cancel fee</label>
                        <input type="text" class="form-control curcancelfee" placeholder="" name="curcancelfee[]" value="{{ $val->curcancelfee }}">
                    </div>
                    <div class="form-group">
                        <label for="newcancelfee">Cancel fee</label>
                        <input type="text" class="form-control newcancelfee" placeholder="" name="newcancelfee[]" value="{{ $val->newcancelfee }}">
                    </div>
                   </div> 
                </div>
                <div class="col-lg-1 col_term">
                   <div class="inner_term"> 
                    <div class="form-group" style="visibility: hidden;">
                        <label for="cursaving">Saving</label>
                        <input type="text" class="form-control cursaving" placeholder="" name="cursaving[]" value="{{ $val->cursaving }}">
                    </div>
                    <div class="form-group green-input" >
                        <label for="newsaving">Saving</label>
                        <input type="text" class="form-control newsaving" placeholder="" name="newsaving[]" value="{{ $val->newsaving }}">
                    </div>
                   </div> 
                </div>
                <div class="col-lg-1 col_term">
                  <div class="inner_term">  
                    <div class="form-group exclude-check">
                        <label class="formfield"><strong>Exclude</strong>
                        <input type="checkbox" class="exclude" name="exclude[]" value="Exclude" {{ $val->exclude == 'Exclude' ? 'checked' : '' }}>
                        <input type="hidden" class="exclude_hidden" name="exclude_hidden[]" value="{{$val->exclude}}" >
                        <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="form-group input-blue">
                        <label for="newcommissiontype">Cashback</label>
                        <input type="text" class="form-control newcommissiontype" placeholder="" name="newcommissiontype[]" value="{{ $val->newcommissiontype }}">
                    </div>
                  </div>  
                </div>
                <div class="col-lg-1 col_term">
                    <div class="inner_term">
                    <div class="form-group delete-group">
                        <button type="button" name="deldata" class="delbtn-red deldatalhc " data-id="{{$val->id}}">Delete</button>
                    </div>
                    <div class="form-group input-blue">
                        <label for="amountlhc">Amount</label>
                        <input type="text" class="form-control amountlhc calAmtLhc" placeholder="" name="amountlhc[]" value="{{ $val->amount }}">
                    </div>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="border-line"></div>
                </div>
            </div>
            @endforeach
        @else
        <div class="row clonerowcls">
            <div class="col-lg-1 col-type">
                <div class="form-group">
                    <label for="servicetype">Type</label>
                    <input type="hidden" class="saveid" name="saveid[]" value="lhc111">
                    <select class="form-control servicetype" name="servicetype[]">
                        <option value="">--Select--</option>
                        @if(!empty($data['services']))
                            @foreach($data['services'] as $key =>$val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col_term">
               <div class="inner_term">
                <div class="form-group">
                    <label for="curprovider">Current Provider</label>
                    <input type="text" class="form-control curprovider" placeholder="" name="curprovider[]">
                </div>
                <div class="form-group">
                    <label for="newprovider">New Provider</label>
                    <input type="text" class="form-control newprovider" placeholder="" name="newprovider[]">
                </div>
              </div>  
            </div>
            <div class="col-lg-1 col_term">
               <div class="inner_term"> 
                <div class="form-group">
                    <label for="curdetails">Details</label>
                    <input type="text" class="form-control curdetails" placeholder="" name="curdetails[]">
                </div>
                <div class="form-group">
                    <label for="newdetails">Details</label>
                    <input type="text" class="form-control newdetails" placeholder="" name="newdetails[]">
                </div>
               </div> 
            </div>
            <div class="col-lg-1 col_term">
               <div class="inner_term"> 
                <div class="form-group">
                    <label for="curenddate">End Date</label>
                    <input type="text" class="end-date curenddate dateSlash" placeholder="DD/MM/YYYY" name="curenddate[]">
                </div>
                <div class="form-group">
                    <label for="newenddate">End Date</label>
                    <input type="text" class="end-date newenddate dateSlash" placeholder="DD/MM/YYYY" name="newenddate[]">
                </div>
               </div> 
            </div>
            <div class="col-lg-1 col_term">
                <div class="inner_term">
                <div class="form-group usagesDiv">
                    <label for="usages">Usage</label>
                    <input type="text" class="form-control usages calAmtLhc" placeholder="" name="usages[]">
                </div>
                <div class="form-group">
                    <label for="term">Term</label>
                    <select class="form-control term calAmtLhc" name="term[]">
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="36">36</option>
                    </select>
                </div>
                </div>
            </div>
            <div class="col-lg-1 col_term">
              <div class="inner_term">  
                <div class="form-group">
                    <label for="curmonthlycost">Monthly Cost</label>
                    <input type="text" class="form-control curmonthlycost calAmtLhc" placeholder="" name="curmonthlycost[]">
                </div>
                <div class="form-group">
                    <label for="newmonthlycost">Monthly Cost</label>
                    <input type="text" class="form-control newmonthlycost calAmtLhc" placeholder="" name="newmonthlycost[]">
                </div>
              </div>  
            </div>
            <div class="col-lg-1 col_term">
              <div class="inner_term">  
                <div class="form-group">
                    <label for="curtotalover">Total over term</label>
                    <input type="text" class="form-control curtotalover calTAmtLhc" placeholder="" name="curtotalover[]">
                </div>
                <div class="form-group">
                    <label for="newtotalover">Total over term</label>
                    <input type="text" class="form-control newtotalover calTAmtLhc" placeholder="" name="newtotalover[]">
                </div>
              </div>  
            </div>
            <div class="col-lg-1 col_term">
              <div class="inner_term">  
                <div class="form-group">
                    <label for="curcancelfee">Cancel fee</label>
                    <input type="text" class="form-control curcancelfee" placeholder="" name="curcancelfee[]">
                </div>
                <div class="form-group">
                    <label for="newcancelfee">Cancel fee</label>
                    <input type="text" class="form-control newcancelfee" placeholder="" name="newcancelfee[]">
                </div>
               </div> 
            </div>
            <div class="col-lg-1 col_term">
               <div class="inner_term"> 
                <div class="form-group" style="visibility: hidden;">
                    <label for="cursaving">Saving</label>
                    <input type="text" class="form-control cursaving" placeholder="" name="cursaving[]">
                </div>
                <div class="form-group green-input">
                    <label for="newsaving">Saving</label>
                    <input type="text" class="form-control newsaving" placeholder="" name="newsaving[]">
                </div>
               </div> 
            </div>
            <div class="col-lg-1 col_term">
                <div class="inner_term">
                 <div class="form-group exclude-check">
                        <label class="formfield"><strong>Exclude</strong>
                        <input type="checkbox" class="exclude" name="exclude[]" value="Exclude">
                        <input type="hidden" class="exclude_hidden" name="exclude_hidden[]" value="" >
                        <span class="checkmark"></span>
                        </label>
                    </div>
                <div class="form-group input-blue">
                    <label for="newcommissiontype">Cashback</label>
                    <input type="text" class="form-control newcommissiontype" placeholder="" name="newcommissiontype[]">
                </div>
            </div>
            </div>
            <div class="col-lg-1 col_term">
                <div class="inner_term">
                <div class="form-group">
                    <button type="button" name="deldata" class="delbtn-red deldata">Delete</button>
                </div>
                <br>
                <div class="form-group input-blue">
                    <label for="amountlhc">Amount</label>
                    <input type="text" class="form-control amountlhc calAmtLhc" placeholder="" name="amountlhc[]">
                </div>
            </div>
            </div>
            <div class="col-md-12">
                <div class="border-line"></div>
            </div>
        </div>
        @endif
        <input type="hidden" class="txtuserIdlhc-Cls" name="txtuserId" value="{{ $data['user_id'] }}" />
        <input type="hidden" class="invoice_no" name="invoice_no" value="{{ $editInvoiceNo? $editInvoiceNo : $lhcRandno}}" />
        <input type="hidden" class="txttotalamount" name="txttotalamount" value="{{$tamount}}" />
        <input type="hidden" class="txttotalamountNonM" name="txttotalamountNonM" value="{{$tamountNonM}}" />

        <input type="hidden" class="totalSavingLhc" name="totalSavingLhc" value="{{$tsaving}}" />
        <input type="hidden" class="tnewmonthlycost" name="tnewmonthlycost" value="{{$tnewmonthlycost}}" />

    </form>
    <div class="new-button">
        <button type="button" id="newlhc"  class="btngreen newlhc">New</button>
    </div>
    <div class="main-button-bar">
        <div class="barleft">
            <div id="showmsglhc" class="form-group alert alert-danger showmsglhc" style="display: none;"></div>
            <div class="form-group" id="processlhc" style="display:none">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbarlhc" aria-valuemin="0" aria-valuemax="100" style="">
                    </div>
                </div>
            </div>
	     <div class="row">
            <div class="col-md-5">
            <button type="button" id="lhcsave" data-val="save" data-userid="{{ $data['user_id'] }}" class="bg-btngreen lhcsave">Save</button>
             <button type="button" id="lhcprepareinvoicechk" class="bg-btnred-graid" data-toggle="modal" data-target="#lhcmyModal" >Prepare Invoice</button>
            </div>
            <div class="col-lg-7">
                  <ul class="barright">
            <li class="border-green">
                <span>Total Saving</span>
                <span class="totalSavingLhc">£{{ $tsaving }}</span>
            </li>
            <li class="border-red">
                <span>Invoice Total</span>
                <span class="totalAmountLhc">£{{ $tamount }}</span>
            </li>
        </ul>
            </div>
        </div>
        </div>
    </div>
    @endif
</div>

<!-- The Modal start -->
<div class="modal agnt-model" id="lhcmyModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="safewatch-form job-forms " action="#" method="post" id="lhcfrmmodel">
                    <div class="item job-txt">
                        <?php
                        $lhctsaving = 0;
                        $lhctamount = 0;
                        ?>
                        <div class="userindex-slider">
                            <div class="slider-header">
                                <div class="title">
                                    @if(!empty($data['lhcs'][0]->invoice_no))
                                    <p id="invoicesNoLhc">Invoices {{ $data['lhcs'][0]->invoice_no}}</p>
                                    @else
                                        <p id="invoicesNoLhc">Invoices {{ $lhcRandno}}</p>
                                    @endif

                                    @if(!empty($data['lhcs'][0]->created_at))
                                        <p>{{ date('d/m/y', strtotime($data['lhcs'][0]->created_at)) }}</p>
                                    @else
                                        <p>{{ date('d/m/y')}}</p>
                                    @endif
                                </div>
                                <div class="job-txt-heading">
                                    <h4>Lifestyle Healthcheck</h4>
                                    <p><a class="doller-li" href="javascript:void(0)" onclick="printDiv()"><i class=" icon fa fa-print" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                            <table class="slider-table table table-borderless">
                                <thead>
                                <tr>
                                    <th class="table-th">Item</th>
                                    <th class="table-th">Provider</th>
                                    <th class="table-th">Benefits/Package</th>
                                    <th class="table-th">End Date</th>
                                    <th class="table-th">Monthly</th>
                                    <th class="table-th">Term</th>
                                    <th class="table-th">Saving (term)</th>
                                </tr>
                                </thead>
                                <tbody id="invoicePreviewLhc">
                                @if(count($data['lhcs']) !=0)
                                    @foreach($data['lhcs'] as $val)
                                        <?php
                                        if(!empty($val->newsaving) && is_numeric($val->newsaving)){
                                            $lhctsaving = $lhctsaving + $val->newsaving;
                                        }
                                        if(!empty($val->amount) && is_numeric($val->amount)){
                                            $lhctamount = $lhctamount + $val->amount;
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                {{$val->name}}
                                                @if(!empty($data['services']))
                                                    @foreach($data['services'] as $sval)
                                                        @if($sval->id == $val->service_id)
                                                            {{$sval->name }}
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                <p class="text-pink">{{$val->curprovider}}</p>
                                                <p class="text-main">{{$val->newprovider}}</p>
                                            </td>
                                            <td>
                                                <p class="text-pink">-</p>
                                                <p class="text-main">£{{$val->newsaving}}</p>
                                            </td>
                                            <td>
                                                <p class="text-pink">{{$val->curenddate}}</p>
                                                <p class="text-main">{{$val->newenddate}}</p>
                                            </td>
                                            <td>
                                                <p class="text-pink">£{{$val->curmonthlycost}}</p>
                                                <p class="text-main">£{{$val->newmonthlycost}}</p>
                                            </td>
                                            <td>
                                                <p class="text-pink">£{{$val->curtotalover}}</p>
                                                <p class="text-main">£{{$val->newtotalover}}</p>
                                            </td>
                                            <td>
                                                <p class="text-pink"></p>
                                                <p class="text-main">£{{$val->newsaving}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center;"><td colspan="7">Data Not Found!</td></tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="saved-year">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="savedyear-block">
                                            <div class="savedyear-blockin">
                                                <p class="text-main">Total Saved</p>
                                                <p class="box-green totalSavingLhc">£{{$lhctsaving}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="savedyear-block">
                                            <div class="savedyear-blockin">
                                                <p>Members (25% fee)</p>
                                                <p class="totalAmountLhc">£{{$lhctamount}}</p>
                                            </div>
                                            <div class="savedyear-blockin">
                                                <p>Non-members (50% fee)</p>
                                                <p class="totalAmounNonMtLhc">£{{$lhctamount}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer ftr">
                <div class="tw0-btn">
                    <button type="button" id="prepareinvoicelhc" class="btnngreen" data-dismiss="modal">Send To Client</button>
                    <button type="button" id="invoiceditlhc" class="btnnred" data-dismiss="modal">Edit</button>
                </div>
                <button type="button" class="btnnred" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- The Modal end -->
<script>
$(document).ready(function(){
    $('.dateSlash').bind('keyup','keydown', function(event) {
        var inputLength = event.target.value.length;
        if (event.keyCode != 8){
            if(inputLength === 2 || inputLength === 5){
                var thisVal = event.target.value;
                thisVal += '/';
                $(event.target).val(thisVal);
            }
        }
    });

    $(".exclude").click(function(){
        var excludeVal = $(this).closest("div.clonerowcls").find("input:checkbox[name='exclude[]']:checked").val();
        var exclude_hiddenField = $(this).closest("div.clonerowcls").find("input[name='exclude_hidden[]']");
        $(exclude_hiddenField).val(excludeVal);
    });

    $('.calAmtLhc').on('keyup change', function () {
        var term = $(this).closest("div.clonerowcls").find("select[name='term[]']").val();
        var curmonthlycost = $(this).closest("div.clonerowcls").find("input[name='curmonthlycost[]']").val();
        var newmonthlycost = $(this).closest("div.clonerowcls").find("input[name='newmonthlycost[]']").val();
        var curtotaloverField = $(this).closest("div.clonerowcls").find("input[name='curtotalover[]']");
        var newtotaloverField = $(this).closest("div.clonerowcls").find("input[name='newtotalover[]']");

        if(term !='' && curmonthlycost !='' && $.isNumeric(term) && $.isNumeric(curmonthlycost)){
            var totaloverterm = parseFloat(term) * parseFloat(curmonthlycost);
            $(curtotaloverField).val(totaloverterm.toFixed(2));
        }
        if(term !='' && newmonthlycost !='' && $.isNumeric(term) && $.isNumeric(newmonthlycost)){
            var newTotaloverTerm = parseFloat(term) * parseFloat(newmonthlycost);
            $(newtotaloverField).val(newTotaloverTerm.toFixed(2));
        }

        var curtotaloverVal = curtotaloverField.val();
        var newtotaloverVal = newtotaloverField.val();
        if(curtotaloverVal !='' && newtotaloverVal !='' && $.isNumeric(curtotaloverVal) && $.isNumeric(newtotaloverVal)){
            var newSaving = parseFloat(curtotaloverVal) - parseFloat(newtotaloverVal);
            var newsavingField = $(this).closest("div.clonerowcls").find("input[name='newsaving[]']");
            $(newsavingField).val(newSaving.toFixed(2));

            var newTotalSaving = 0;
            var member = 25;
            $("input[name='newsaving[]']").map(function(){
                newTotalSaving += parseFloat($(this).val());
                $('.totalSavingLhc').text('£ '+newTotalSaving.toFixed(2));
            }).get();
            var memberPer = (member / 100 * newTotalSaving)
            $('.totalAmountLhc').text('£ '+memberPer.toFixed(2));
        }
    });

    $('.calTAmtLhc').on('keyup change', function () {
        var term = $(this).closest("div.clonerowcls").find("select[name='term[]']").val();
        var curmonthlycostField = $(this).closest("div.clonerowcls").find("input[name='curmonthlycost[]']");
        var newmonthlycostField = $(this).closest("div.clonerowcls").find("input[name='newmonthlycost[]']");
        var curtotaloverField = $(this).closest("div.clonerowcls").find("input[name='curtotalover[]']");
        var newtotaloverField = $(this).closest("div.clonerowcls").find("input[name='newtotalover[]']");
        var newsavingField = $(this).closest("div.clonerowcls").find("input[name='newsaving[]']");

        var curtotaloverVal = curtotaloverField.val();
        var newtotaloverVal = newtotaloverField.val();

        if(term !='' && curtotaloverVal !='' && $.isNumeric(curtotaloverVal)){
            var curmonthlycost = parseFloat(curtotaloverVal) / parseFloat(term);
            $(curmonthlycostField).val(curmonthlycost.toFixed(2));
        }
        if(term !='' && newtotaloverVal !='' && $.isNumeric(newtotaloverVal)){
            var newmonthlycost = parseFloat(newtotaloverVal) / parseFloat(term);
            $(newmonthlycostField).val(newmonthlycost.toFixed(2));
        }

        if(curtotaloverVal !='' && newtotaloverVal !='' && $.isNumeric(curtotaloverVal) && $.isNumeric(newtotaloverVal)){
            var newSaving = parseFloat(curtotaloverVal) - parseFloat(newtotaloverVal);
            $(newsavingField).val(newSaving.toFixed(2));

            var newTotalSaving = 0;
            var member = 25;
            $("input[name='newsaving[]']").map(function(){
                newTotalSaving += parseFloat($(this).val());
                $('.totalSavingLhc').text('£ '+newTotalSaving.toFixed(2));
            }).get();
            var memberPer = (member / 100 * newTotalSaving)
            $('.totalAmountLhc').text('£ '+memberPer.toFixed(2));
        }
    });

    $('#lhcprepareinvoicechk').click(function(){
        $('#invoiceditlhc').show();

        var servicetypeVal = $('.servicetype').val();
        if(servicetypeVal =='' || typeof servicetypeVal == 'undefined'){
            $("#lhcmyModal .close").click();
            $('#showmsglhc').show();
            $('#showmsglhc').text('Sorry! Firstly you enter data then you can create invoices.');
            return false;
        }
        $('#showmsglhc').hide();

        var servicetype = $("select[name='servicetype[]']");
        var curprovider = $("input[name='curprovider[]']");
        var newprovider = $("input[name='newprovider[]']");
        var curenddate = $("input[name='curenddate[]']");
        var newenddate = $("input[name='newenddate[]']");
        var curmonthlycost = $("input[name='curmonthlycost[]']");
        var newmonthlycost = $("input[name='newmonthlycost[]']");
        var curtotalover = $("input[name='curtotalover[]']");
        var newtotalover = $("input[name='newtotalover[]']");
        var usages = $("input[name='usages[]']");
        var term = $("input[name='term[]']");
        var newsaving = $("input[name='newsaving[]']");
        var exclude_hidden = $("input[name='exclude_hidden[]']");

        var newTotalSaving = 0;
        var tnewmonthlycost = 0;
        var html = '';
        for (var i=0; i<servicetype.length; i++) {
            if(curprovider[i].value !=''){
                if(exclude_hidden[i].value !='Exclude'){
                    if(curenddate[i].value !='' && !(curenddate[i].value.match(/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/))){
                        $("#lhcmyModal .close").click();
                        $('#showmsglhc').show();
                        $('#showmsglhc').text('The current end date.'+i+' does not match the format DD/MM/YYYY');
                        return false;
                    }
                    if(newenddate[i].value !='' && !(newenddate[i].value.match(/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/))){
                        $("#lhcmyModal .close").click();
                        $('#showmsglhc').show();
                        $('#showmsglhc').text('The new end date.'+i+' does not match the format DD/MM/YYYY');
                        return false;
                    }
                    newTotalSaving += parseFloat(newsaving[i].value);
                    tnewmonthlycost += parseFloat(newmonthlycost[i].value);
                    var servicetypeName = $("option:selected", servicetype[i]).text();
                    $('#prepareinvoicelhc').show();
                    $('#invoiceditlhc').show();
                    html +='<tr><td>'+servicetypeName+'</td><td><p class="text-pink">'+curprovider[i].value+'</p><p class="text-main">'+newprovider[i].value+'</p></td><td><p class="text-pink">-</p><p class="text-main">£'+newsaving[i].value+'</p></td><td><p class="text-pink">'+curenddate[i].value+'</p><p class="text-main">'+newenddate[i].value+'</p></td><td><p class="text-pink">£'+curmonthlycost[i].value+'</p><p class="text-main">£'+newmonthlycost[i].value+'</p></td><td><p class="text-pink">£'+curtotalover[i].value+'</p><p class="text-main">£'+newtotalover[i].value+'</p></td><td><p class="text-pink"></p><p class="text-main">£'+newsaving[i].value+'</p></td></tr>';
                }
            }else{
                html +='<tr style="text-align: center"><td colspan="7">There is not Found!</td></tr>';
                $('#prepareinvoicelhc').hide();
                $('#invoiceditlhc').hide();
            }
        }
        $('#invoicePreviewLhc').html(html);

        $('.totalSavingLhc').text('£ '+newTotalSaving.toFixed(2));
        $('.totalSavingLhc').val(newTotalSaving.toFixed(2));

        var member = 25;
        var nonMember = 50;
        var memberPer = (member / 100 * newTotalSaving);
        var nonMemberPer = (nonMember / 100 * newTotalSaving)
        $('.totalAmountLhc').text('£ '+memberPer.toFixed(2));
        $('.txttotalamount').val(memberPer.toFixed(2));
        $('.totalAmounNonMtLhc').text('£ '+nonMemberPer.toFixed(2));
        $('.txttotalamountNonM').val(nonMemberPer.toFixed(2));
        $('.tnewmonthlycost').val(tnewmonthlycost.toFixed(2));
    });

    $('#invoiceditlhc').click(function(){
        $("#lhcmyModal .close").click();
    });
});

$(document).on('click', '#prepareinvoicelhc', function(e) {
    e.preventDefault();
    var status = 'save';
    $.ajax({
       type: "post",
        async: true,
        cache: false,
       url: "{{route('lhcPrepareInvoice')}}",
       data: {formdata : $("#lhcfrm").serialize(), "_token": "{{ csrf_token() }}"
       },
       beforeSend:function()
       {
           $('#prepareinvoicelhc').attr('disabled', 'disabled');
           $('#processlhc').css('display', 'block');
       },
        success: function (res) {
           
            var percentage = 0;
            var timer = setInterval(function(){
                percentage = percentage + 20;
                progress_bar_process(percentage, timer, res, status);
            }, 1000);
            setTimeout(function() {
                $('a[href="#nav-tracked"]').trigger("click");
            }, 6000);
        }
    });
    e.stopImmediatePropagation();
    return false;
});

$(".servicetype").change(function() {
    var id = $(this).val();
    var usagesField = $(this).closest("div.clonerowcls").find(".usagesDiv");
    if(id ==1){
        $(usagesField).show();
    }else if(id ==2){
        $(usagesField).show();
    }else{
        $(usagesField).hide();
    }
});

$(document).on('click', '.lhcsave', function(e) {
    e.preventDefault();
    //var frmdata = $('#jobfrm').serializeArray();
    var data = {};
    data['status'] = $(this).attr("data-val");
    data['userid'] = $(this).attr("data-userid");
    data['saveid'] = $("input[name='saveid[]']").map(function(){return $(this).val();}).get();
    data['services_type'] = $("select[name='servicetype[]']").map(function(){return $(this).val();}).get();
    data['current_provider'] = $("input[name='curprovider[]']").map(function(){return $(this).val();}).get();
    data['new_provider'] = $("input[name='newprovider[]']").map(function(){return $(this).val();}).get();
    data['curdetails'] = $("input[name='curdetails[]']").map(function(){return $(this).val();}).get();
    data['newdetails'] = $("input[name='newdetails[]']").map(function(){return $(this).val();}).get();
    data['current_end_date'] = $("input[name='curenddate[]']").map(function(){return $(this).val();}).get();
    data['new_end_date'] = $("input[name='newenddate[]']").map(function(){return $(this).val();}).get();
    data['exclude_hidden'] = $("input[name='exclude_hidden[]']").map(function(){return $(this).val();}).get();

    // var curenddate = $("input[name='curenddate[]']");
    // var newenddate = $("input[name='newenddate[]']");
    // var exclude_hiddenVal = $("input[name='exclude_hidden[]']");
    // var current_end_date = [];
    // var new_end_date = [];
    // var exclude_hidden = [];
    // for (var i=0; i<curenddate.length; i++) {
    //
    //     if(exclude_hiddenVal[i].value =='Exclude'){
    //         if(!(curenddate[i].value.match(/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/))){
    //             $('#showmsglhc').show();
    //             $('#showmsglhc').text('The current end date.'+i+' does not match the format DD/MM/YYYY');
    //             return false;
    //         }
    //     }else{
    //         if(!(newenddate[i].value.match(/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/))){
    //             $("#lhcmyModal .close").click();
    //             $('#showmsglhc').show();
    //             $('#showmsglhc').text('The new end date.'+i+' does not match the format DD/MM/YYYY');
    //             return false;
    //         }
    //     }
    //     $('#showmsglhc').hide();
    //     current_end_date[i] = curenddate[i].value;
    //     new_end_date[i] = newenddate[i].value;
    //     exclude_hidden[i] = exclude_hiddenVal[i].value;
    // }


    data['usages'] = $("input[name='usages[]']").map(function(){return $(this).val();}).get();
    data['term'] = $("select[name='term[]']").map(function(){return $(this).val();}).get();
    data['current_monthly_cost'] = $("input[name='curmonthlycost[]']").map(function(){return $(this).val();}).get();
    data['new_monthly_cost'] = $("input[name='newmonthlycost[]']").map(function(){return $(this).val();}).get();
    data['current_total_over_term'] = $("input[name='curtotalover[]']").map(function(){return $(this).val();}).get();
    data['new_total_over_term'] = $("input[name='newtotalover[]']").map(function(){return $(this).val();}).get();
    data['current_cancel_fee'] = $("input[name='curcancelfee[]']").map(function(){return $(this).val();}).get();
    data['new_cancel_fee'] = $("input[name='newcancelfee[]']").map(function(){return $(this).val();}).get();
    data['cursaving'] = $("input[name='cursaving[]']").map(function(){return $(this).val();}).get();
    data['newsaving'] = $("input[name='newsaving[]']").map(function(){return $(this).val();}).get();
    data['newcommissiontype'] = $("input[name='newcommissiontype[]']").map(function(){return $(this).val();}).get();
    data['amount'] = $("input[name='amountlhc[]']").map(function(){return $(this).val();}).get();
    data['exclude'] = $("input:checkbox[name='exclude[]']:checked").map(function(){return $(this).val();}).get();
    if(data != ''){
        $.ajax({
            type: "post",
            async: true,
            cache: false,
            url: "{{route('lhcsave')}}",
            data: {
                '_token': "{{csrf_token()}}",
                data: data,
            },
            dataType: "json",
            beforeSend:function()
            {
                $('#lhcsave').attr('disabled', 'disabled');
                $('#processlhc').css('display', 'block');
            },
            success: function(res) {
                if($.isEmptyObject(res.error)){
                    $(".lhc-error-msg").css('display','none');
                    var percentage = 0;
                    var timer = setInterval(function(){
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, res, status);
                    }, 1000);
                    setTimeout(function() {
                        $('a[href="#nav-tracked"]').trigger("click");
                    }, 6000);
                }else{
                    jobErrorMsg(res.error);
                    toastr.error('error!', 'Some Fields are Required. Please Fill!');
                    $('#processlhc').css('display', 'none');
                    $('#lhcsave').attr('disabled', false);
                    $('#prepareinvoicelhc').attr('disabled', false);
                }
            }
        });
        e.stopImmediatePropagation();
        return false;
    }
});

function jobErrorMsg (msg) {
    $(".lhc-error-msg").find("ul").html('');
    $(".lhc-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".lhc-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

function progress_bar_process(percentage, timer, res, status)
{
    $('.progress-bar').css('width', percentage + '%');
    if(percentage > 100)
    {
        if (res.status == 'save') {
            toastr.success('Success!', 'Data Added Successfully!');
            if(status =='save'){
                //toastr.success('Success!', 'Data Added Successfully!');
            }else if(status =='update'){
                toastr.success('Success!', 'Data Updated Successfully!');
            }
        } else {
            toastr.error('error!', 'Some Problem Here. Try Again!');
        }
        clearInterval(timer);
        $('#processlhc').css('display', 'none');
        $('.progress-bar').css('width', '0%');
        $('#lhcsave').attr('disabled', false);
        $('#prepareinvoicelhc').attr('disabled', false);
    }
}

$(document).ready(function(){
    $('#newlhc').click(function(e) {
        e.preventDefault();
		
		// $('.curenddate').datepicker('destroy');
		// $('.newenddate').datepicker('destroy');
        var clone = $('.clonerowcls:last').clone(true);
        clone.find("input").val("");
        clone.find('select option').removeAttr('selected');
        clone.find('.deldatalhc ').removeClass('deldatalhc');
        clone.find('.delbtn-red ').addClass('deldata');
        $(clone).find('.saveid').val('lhc111');
        $(clone).find('.exclude').val('Exclude');
        $(clone).insertAfter(".clonerowcls:last");
		
		// var i = 0;
        // $('.newenddate').each(function () {
        //     $(this).attr("id",'newdate' + i).datepicker();
        //     i++;
        // });
        // var i = 0;
        // $('.curenddate').each(function () {
        //     $(this).attr("id",'curenddate' + i).datepicker();
        //     i++;
        // });
        return false;
    });

    $('.clonerowcls').on('click', ".deldata", function() {
        $(this).closest('.clonerowcls').remove();
    });

    // $('.clonerowcls').on('focus', ".datepicker", function() {
    //     $(this).removeClass('hasDatepicker').datepicker({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: "dd/mm/yy",
    //         yearRange: "1900:2030",
    //         dateFormat: 'dd/mm/yy'
    //     });
    // });

});

$(document).on('click', '.deldatalhc', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "{{route('lhcdelete')}}",
                    data: {
                        '_token': "{{csrf_token()}}",
                        id: id
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success('Success!', 'Data Deleted Successfully!');
                            $(".rowcls" + id).load(" .rowcls" + id);
                        } else {
                            swal({
                                title: "Deleted!",
                                text: "Data Can't deleted.",
                                type: "error",
                                timer: 10000,
                                showConfirmButton: false
                            });
                        }
                    }
                });
            } else {
                swal("Cancelled", "Your Record is Safe :)", "error");
            }
        });
});

function printDiv(){
    var contents = $("#lhcmyModal").html();
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>Print View</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.
    frameDoc.document.write('<link href="http://localhost/safewatch/public/css/app.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/style.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/fonts.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);

}
</script>
