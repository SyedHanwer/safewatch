<?php
$jobrandno = \CommonHelper::instance()->autoRandomNo('JOBS');
$tsaving = 0;
$tamount = 0;
$tcost = 0;
$editInvoiceNo = 0;
?>
<div class="userindex-slider">
    @if(!empty($data['user_id']))
    <form class="safewatch-form job-forms" id="jobfrm" name="jobfrm" method="post" action="#">
        <div class="alert alert-danger jobs-error-msg" style="display:none">
            <ul></ul>
        </div>
        {{ csrf_field() }}
        @if(count($data['jobs']) != 0)
            @foreach($data['jobs'] as $val)
                <?php
                if(!empty($val->saving) && strlen((string)$val->saving)){
                    $tsaving = $tsaving + $val->saving;
                }
                if(!empty($val->amount) && strlen((string)$val->amount)){
                    $tamount = $tamount + $val->amount;
                }
                if(!empty($val->cost) && strlen((string)$val->cost)){
                    $tcost = $tcost + $val->cost;
                }
                if(!empty($val->invoice_no) && !empty($val->status) && $val->status ==2){
                    $editInvoiceNo = $val->invoice_no;
                }
                ?>
                <div class="row rowcls{{ $val->id }} newjobclone" >
                    <input type="hidden" class="form-control jobsidEdit " placeholder="" name="jobsid[]" value="{{$val->id ? $val->id: 'job222' }}">
                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="jobno">Job no.</label>
                            <input type="text" class="form-control jobno" placeholder="Enter Job no." id="" name="jobno[]" value="{{ $val->jobno? $val->jobno: $jobrandno }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="details">Details</label>
                            <input type="text" class="form-control details" placeholder="Enter Details" id="" name="details[]" value="{{ $val->details }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="units">Units</label>
                            <input type="text" class="form-control units calAmt" placeholder="Enter Units 1" id="" name="units[]" value="{{ $val->units }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="standard">Standard</label>
                            <input type="text" class="form-control standard calAmt" placeholder="Enter Standard £20.00" id="" name="standard[]" value="{{ $val->standard }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="member">Member</label>
                            <input type="text" class="form-control member calAmt" placeholder="Enter Member £20.00" id="" name="member[]" value="{{ $val->member }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="cost">Cost</label>
                            <input type="text" class="form-control cost" placeholder="Enter Cost £20.00" id="" name="cost[]" value="{{ $val->cost }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group green-input">
                            <label for="saving">Saving</label>
                            <input type="text" class="form-control saving" placeholder="Enter Saving £20.00" id="" name="saving[]" value="{{ $val->saving }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group input-red">
                            <label for="amount">Total</label>
                            <input type="text" class="form-control amount" placeholder="Enter Amount £50.00" id="" name="amount[]" value="{{ $val->amount }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group input-blue">
                            <label for="comission_type">Cashback</label>
                            <input type="text" class="form-control comission_type" placeholder="Enter Cashback" id="" name="comission_type[]" value="{{ $val->comission_type }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group input-blue">
                            <label for="amount2">Amount</label>
                            <input type="text" class="form-control amount2" placeholder="" id="" name="amount2[]" value="{{ $val->amount2 }}">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="form-group">
                            <button type="button" class="delbtn-red delData " data-id="{{$val->id}}">Delete</button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="border-line"></div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="row rowcls newjobclone" >
                <input type="hidden" class="form-control jobsid" name="jobsid[]" value="job222">
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="jobno">Job no.</label>
                        <input type="text" class="form-control jobno" placeholder="" id="" name="jobno[]" value="{{$jobrandno}}" readonly>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="details">Details</label>
                        <input type="text" class="form-control details" placeholder="" id="" name="details[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="units">Units</label>
                        <input type="text" class="form-control units calAmt" placeholder="" id="" name="units[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="standard">Standard</label>
                        <input type="text" class="form-control standard calAmt" placeholder="" id="" name="standard[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="member">Member</label>
                        <input type="text" class="form-control member calAmt" placeholder="" id="" name="member[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <label for="cost">Cost</label>
                        <input type="text" class="form-control cost" placeholder="" id="" name="cost[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group green-input">
                        <label for="saving">Saving</label>
                        <input type="text" class="form-control saving" placeholder="" id="" name="saving[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group input-red">
                        <label for="amount">Total</label>
                        <input type="text" class="form-control amount" placeholder="" id="" name="amount[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group input-blue">
                        <label for="comission_type">Cashback</label>
                        <input type="text" class="form-control comission_type" placeholder="" id="" name="comission_type[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group input-blue">
                        <label for="amount2">Amount</label>
                        <input type="text" class="form-control amount2" placeholder="" id="" name="amount2[]" value="">
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="form-group">
                        <button type="button" class="delbtn-red clonerowcls" >Delete</button>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="border-line"></div>
                </div>
            </div>
        @endif
        <input type="hidden" class="txtuserId-cls" name="txtuserId" value="{{ $data['user_id'] }}" />
        <input type="hidden" class="txttotalamount" name="txttotalamount" value="{{$tamount}}" />
        <input type="hidden" class="txtTotalProfit" name="txtTotalProfit" value="" />
        <input type="hidden" class="txttotalSaving" name="txttotalSaving" value="{{$tsaving}}" />
        <input type="hidden" class="txttotalCost" name="txttotalCost" value="{{$tcost}}" />
        <input type="hidden" class="invoiceTotal" id="invoiceTotal" name="invoiceTotal" value="">
        <input type="hidden" class="editInvoiceNo" id="editInvoiceNo" name="editInvoiceNo" value="{{$editInvoiceNo}}">
    </form>
    <div class="new-button">
        <button type="button" class="btngreen jobnew" id="jobnew">New</button>
    </div>

    <div class="main-button-bar">
        <div class="barleft">
            <div id="showmsg" class="form-group alert alert-danger showmsg" style="display: none;"></div>
            <div class="form-group" id="process" style="display:none">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style=""></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <button type="button" id="jobsave" name="jobsave" data-val="save" data-userid="{{$data['user_id'] }}" class="bg-btngreen jobssubmit">Save</button>
                    <button type="button" id="prepareinvoicechk" class="bg-btnred-graid" data-toggle="modal" data-target="#jobsmyModal" >Prepare Invoice</button>
                </div>
                <div class="col-md-7">
                    <ul class="barright">
                        <li class="border-green">
                            <span>Total Saving</span>
                            <span id="jobtsaving">£{{ $tsaving }}</span>
                        </li>
                        <li class="border-red">
                            <span>Invoice Total</span>
                            <span id="jobtamount">£{{ $tamount }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- The Modal start -->
<div class="modal agnt-model" id="jobsmyModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="safewatch-form job-forms" id="jobfrmModel" name="jobfrm" method="post" action="#">
                    <div class="item job-txt">
                        <div class="userindex-slider">
                            <div class="slider-header">
                                <?php
                                $stsaving = 0;
                                $stamount = 0;
                                ?>
                                <div class="title">
                                    <p id="invoicesNO"> @if(!empty($data['jobs'][0]->jobno)) {{ 'Invoices '
                                    .$data['jobs'][0]->jobno}}@endif</p>

                                    @if(!empty($data['jobs'][0]->created_at))
                                        <p>{{ date('d/m/y', strtotime($data['jobs'][0]->created_at))
                                            }}</p>
                                    @else
                                        <p>{{ date('d/m/y')}}</p>
                                    @endif
                                </div>
                                <div class="job-txt-heading">
                                    <h4>Services</h4>
                                    <p><a class="doller-li" href="javascript:void(0)" onclick="printDiv()"><i class=" icon fa fa-print" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                            <table class="slider-table table table-borderless">
                                <thead>
                                <tr>
                                    <th class="table-th">Service</th>
                                    <th class="table-th">Hrs/Units</th>
                                    <th class="table-th">Normal</th>
                                    <th class="table-th">SafeWatch</th>
                                    <th class="table-th">Saving</th>
                                    <th class="table-th">ToPay</th>
                                </tr>
                                </thead>
                                <tbody id="inPreViewTbl">
                                @if(count($data['jobs']) !=0)
                                    @foreach($data['jobs'] as $val)
                                        <?php
                                        if(!empty($val->saving) && is_numeric($val->saving)){
                                            $stsaving = $stsaving + $val->saving;
                                        }
                                        if(!empty($val->amount) && is_numeric($val->amount)){
                                            $stamount = $stamount + $val->amount;
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$val->details}}</td>
                                            <td>{{$val->units}}</td>
                                            <td class="text-pink">£{{$val->standard}}</td>
                                            <td class="text-green">£{{ $val->member }}</td>
                                            <td class="text-green">£{{$val->saving}}</td>
                                            <td>£{{$val->amount}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center;"><td colspan="6">Data Not Found!</td></tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="saved-year">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="savedyear-block">
                                            <div class="savedyear-blockin">
                                                <p class="text-main" >Total saved</p>
                                                <p class="box-green" id="inTotalSaving">£{{$stsaving}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="savedyear-block">
                                            <div class="savedyear-blockin">
                                                <p>Total to pay</p>
                                                <p id="inTotalAmount">£{{$stamount}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer ftr">
                <div class="tw0-btn">
                    <button type="button" id="jobprepareinvoice" class="btnngreen" data-dismiss="modal">Send To Client</button>
                    <button type="button" id="invoicedit" class="btnnred" data-dismiss="modal">Edit</button>
                </div>
                <button type="button" class="btnnred" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- The Modal end -->
<script type="text/javascript">
$(document).ready(function(){
    $('.calAmt').keyup(function() {
        var intRegex = /^\d+$/;
        var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;

        var units = $(this).closest("div.newjobclone").find("input[name='units[]']").val();
        var member = $(this).closest("div.newjobclone").find("input[name='member[]']").val();
        var standard = $(this).closest("div.newjobclone").find("input[name='standard[]']").val();
        if(member !='' && units !='' && intRegex.test(units) && intRegex.test(member) || floatRegex.test(member)){
            var amount = parseInt(units) * parseInt(member);
            var amountfield = $(this).closest("div.newjobclone").find("input[name='amount[]']");
            $(amountfield).val(amount);
            var totalAmount = 0;

            $("input[name='amount[]']").map(function(){
                totalAmount += parseInt($(this).val());
                $('#jobtamount').text('£ '+totalAmount);
                $('.txttotalamount').val(totalAmount);
            }).get();

            var invoiceTotal = 0;
            var editInvoiceNo = $('#editInvoiceNo').val();
            if(editInvoiceNo !='' && editInvoiceNo !=0 && editInvoiceNo !=null){
                $('.invoiceTotal').val(editInvoiceNo);
            }else{
                $("input[name='jobno[]']").map(function(){
                    invoiceTotal += parseInt($(this).val());
                    $('.invoiceTotal').val(invoiceTotal);
                }).get();
            }

        }

        if(member !='' && units !='' && standard !='' && intRegex.test(units) && (intRegex.test(member) || floatRegex.test(member)) && (intRegex.test(standard) || floatRegex.test(standard))){
            var saving = (parseInt(standard) - parseInt(member)) * parseInt(units);
            var savingfield = $(this).closest("div.newjobclone").find("input[name='saving[]']");
            $(savingfield).val(saving);
            var totalSaving = 0;
            $("input[name='saving[]']").map(function(){
                totalSaving += parseInt($(this).val());
                $('#jobtsaving').text('£ '+totalSaving);
                $('.txttotalSaving').val(totalSaving);
            }).get();
        }

    });

    $('#prepareinvoicechk').click(function(){
        $('#invoicedit').show();
        var txtuserId = $('.details').val();
        if(txtuserId =='' || typeof txtuserId == 'undefined'){
            $("#jobsmyModal .close").click();
            $('#showmsg').show();
            $('#showmsg').text('Sorry! Firstly you enter data then you can create invoices.');
            return false;
        }
        $('#showmsg').hide();

        var jobno = $("input[name='jobno[]']");
        var details = $("input[name='details[]']");
        var units = $("input[name='units[]']");
        var standard = $("input[name='standard[]']");
        var member = $("input[name='member[]']");
        var saving = $("input[name='saving[]']");
        var amount = $("input[name='amount[]']");
        var html = '';
        for (var i=0; i<jobno.length; i++) {
            if(amount[i].value !=''){
                $('#jobprepareinvoice').show();
                $('#invoicedit').show();
                html +='<tr><td>'+details[i].value+'</td><td>'+units[i].value+'</td><td class="text-pink">£'+standard[i]
                    .value+'</td><td class="text-green">£'+member[i].value+'</td><td class="text-green">£'+saving[i].value+'</td><td>£'+amount[i].value+'</td></tr>';
            }else{
                html +='<tr style="text-align: center"><td colspan="6">Data Not Found!</td></tr>';
                $('#jobprepareinvoice').hide();
                $('#invoicedit').hide();
            }
        }

        $('#inPreViewTbl').html(html);
        var totalAmount = 0;
        $("input[name='amount[]']").map(function(){
            totalAmount += parseInt($(this).val());
            $('.txttotalamount').val(totalAmount);
            $('#inTotalAmount').text('£ '+totalAmount);
        }).get();

        var invoiceTotal = 0;
        var editInvoiceNo = $('#editInvoiceNo').val();
        if(editInvoiceNo !='' && editInvoiceNo !=0 && editInvoiceNo !=null){
            $('.invoiceTotal').val(editInvoiceNo);
            $('#invoicesNO').text('Invoice '+editInvoiceNo);
        }else{
            $("input[name='jobno[]']").map(function(){
                invoiceTotal += parseInt($(this).val());
                $('.invoiceTotal').val(invoiceTotal);
                $('#invoicesNO').text('Invoice '+invoiceTotal);
            }).get();
        }


        var totalSaving = 0;
        $("input[name='saving[]']").map(function(){
            totalSaving += parseInt($(this).val());
            $('.txttotalSaving').val(totalSaving);
            $('#inTotalSaving').text('£ '+totalSaving);
        }).get();

        var totalMember = 0;
        $("input[name='member[]']").map(function(){
            totalMember += parseInt($(this).val());
        }).get();

        var totalCost = 0;
        $("input[name='cost[]']").map(function(){
            totalCost += parseInt($(this).val());
            $('.txttotalCost').val(totalCost);
        }).get();

        var profit = 0;
        if(totalMember !='' && totalCost !=''){
            profit = totalMember - totalCost;
        }else{
            profit = totalMember;
        }
        $('.txtTotalProfit').val(profit);

    });

    $('#invoicedit').click(function(){
        $('.editable').prop("disabled", false);
        $("#jobsmyModal .close").click();
    });
});

    $(document).on('click', '#jobprepareinvoice', function(e) {
        $("#jobsmyModal .close").click();
        var status = 'save';
        $.ajax({
            type: "post",
            url: "{{route('jobPrepareInvoiceSA')}}",
            data: {formdata : $("#jobfrm").serialize(), "_token": "{{ csrf_token() }}"
            },
            async: true,
            cache: false,
            beforeSend:function()
            {
                $('#jobprepareinvoice').attr('disabled', 'disabled');
                $('#process').css('display', 'block');
            },
            success: function (res) {
                var percentage = 0;
                var timer = setInterval(function(){
                    percentage = percentage + 20;
                    progress_bar_process(percentage, timer, res, status);
                }, 1000);
            setTimeout(function() {
                $('a[href="#nav-profiletrack"]').trigger("click");
            }, 6000);
        }
    });
    e.stopImmediatePropagation();
    return false;
});

$(document).ready(function(){
    $('#jobnew').click(function(e){
        e.preventDefault();
        var clone = $('#jobfrm > div.newjobclone:last').clone(true);
        clone.find("input").val("");
        //var randno = Math.floor(1000 + Math.random() * 9000);
        var randno = autoRandomNo('JOBS');
        $(clone).find('.jobno').val(randno);
        clone.find('.delData ').removeClass('delData');
        $(clone).find('.jobsidEdit').addClass('jobsid').removeClass('jobsidEdit');
        clone.find('.delbtn-red ').addClass('clonerowcls');
        $(clone).find('.jobsid').val('job222');
        $(clone).insertAfter('#jobfrm > div.newjobclone:last');
        return false;
    });

    $('.newjobclone').on('click', ".clonerowcls", function (e) {
        $(this).closest('.newjobclone').remove();
        e.preventDefault();
    });

});

function autoRandomNo(val){
     var val = val;
     var returnValue = null;
     $.ajax({
        type: "post",
        async: false,
        dataType: "json",
        url: "{{route('autoRandomNo')}}",
        data: {
            '_token': "{{csrf_token()}}",
            val: val,
        },
        success: function(res) {
            returnValue = res;
        }
    });
     return returnValue;
}

$('.jobssubmit').click(function(e){
    e.preventDefault();
    $("#jobsmyModal .close").click();
    //var frmdata = $('#jobfrm').serializeArray();
    var data = {};
    var status = $(this).attr("data-val");
    data['status'] = status;
    data['userid'] = $(this).attr("data-userid");
    data['jobsid'] = $("input[name='jobsid[]']").map(function(){return $(this).val();}).get();
    data['jobno'] = $("input[name='jobno[]']").map(function(){return $(this).val();}).get();
    data['details'] = $("input[name='details[]']").map(function(){return $(this).val();}).get();
    data['units'] = $("input[name='units[]']").map(function(){return $(this).val();}).get();
    data['standard'] = $("input[name='standard[]']").map(function(){return $(this).val();}).get();
    data['member'] = $("input[name='member[]']").map(function(){return $(this).val();}).get();
    data['cost'] = $("input[name='cost[]']").map(function(){return $(this).val();}).get();
    data['saving'] = $("input[name='saving[]']").map(function(){return $(this).val();}).get();
    data['comission_type'] = $("input[name='comission_type[]']").map(function(){return $(this).val();}).get();
    data['amount'] = $("input[name='amount[]']").map(function(){return $(this).val();}).get();
    data['amount2'] = $("input[name='amount2[]']").map(function(){return $(this).val();}).get();
    if(data != ''){
        $.ajax({
            type: "post",
            url: "{{route('jobsaveSA')}}",
            data: {
                '_token': "{{csrf_token()}}",
                data: data,
            },
            async: true,
            cache: false,
            dataType: "json",
            beforeSend:function()
            {
                $('#jobsave').attr('disabled', 'disabled');
                $('#process').css('display', 'block');
            },
            success: function (res) {
                if($.isEmptyObject(res.error)){
                    $(".jobs-error-msg").css('display','none');
                    var percentage = 0;
                    var timer = setInterval(function(){
                        percentage = percentage + 20;
                        progress_bar_process(percentage, timer, res, status);
                    }, 1000);
                    setTimeout(function() {
                        $('a[href="#nav-profiletrack"]').trigger("click");
                    }, 6000);
                }else{
                    jobErrorMsg(res.error);
                    toastr.error('error!', 'Some Fields are Required. Please Fill!');
                    $('#process').css('display', 'none');
                    $('#jobsave').attr('disabled', false);
                    $('#jobprepareinvoice').attr('disabled', false);
                }

            }
        });
        e.stopImmediatePropagation();
        return false;
    }
});

function jobErrorMsg (msg) {
    $(".jobs-error-msg").find("ul").html('');
    $(".jobs-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".jobs-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

function progress_bar_process(percentage, timer, res, status)
{
    $('.progress-bar').css('width', percentage + '%');
    if(percentage > 100)
    {
        if (res.status == 'save') {
            if(status =='save'){
                toastr.success('Success!', 'Data Added Successfully!');
            }else if(status =='update'){
                toastr.success('Success!', 'Data Updated Successfully!');
            }
        } else {
            toastr.error('error!', 'Some Problem Here. Try Again!');
        }
        clearInterval(timer);
        $('#process').css('display', 'none');
        $('.progress-bar').css('width', '0%');
        $('#jobsave').attr('disabled', false);
        $('#jobprepareinvoice').attr('disabled', false);
    }
}

$(document).on('click', '.delData', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "{{route('jobdeleteSA')}}",
                    data: {
                        '_token': "{{csrf_token()}}",
                        id: id
                    },
                    success: function (data) {
                        if (data == 1) {
                            toastr.success('Success!', 'Data Deleted Successfully!');
                            $(".rowcls" + id).load(" .rowcls" + id);
                        } else {
                            swal({
                                title: "Deleted!",
                                text: "Data Can't deleted.",
                                type: "error",
                                timer: 10000,
                                showConfirmButton: false
                            });
                        }
                    }
                });
            } else {
                swal("Cancelled", "Your Record is Safe :)", "error");
            }
        });
});

function printDiv(){
    var contents = $("#jobsmyModal").html();
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>Print View</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.
    frameDoc.document.write('<link href="http://localhost/safewatch/public/css/app.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/style.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/fonts.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);

}
</script>
