<div class="container herotabs-content-in">
   <div class="row">
      <div class="col-lg-12">
         <div class="user-index">
            <h2>Planner</h2>
            <h4 class="subtitle">Job reminders</h4>
            <div class="userindex-slider">
               <table class="slider-table table table-borderless">
                  <thead>
                     <tr>
                        <th class="table-th">Name</th>
                        <th class="table-th">Date</th>
                        <th class="table-th">Type</th>
                        <th class="table-th">Provider</th>
                        <th class="table-th">Monthly Cost</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(!empty($data))
                     @foreach($data as $val)
                     <tr>
                        <td>{{ $val->curdetails }}</td>
                        <td>{{ date('d/m/Y', strtotime($val->curenddate)) }}</td>
                        <td>{{ $val->sname }}</td>
                        <td>{{ $val->curprovider }}</td>
                        <td>
                           £{{ $val->curmonthlycost }} <button>Mark as <br>Complete</button>
                        </td>
                     </tr>
                     @endforeach
                     @else 
                     <tr>
                        <td>Data Not Found!</td>
                     </tr>
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
