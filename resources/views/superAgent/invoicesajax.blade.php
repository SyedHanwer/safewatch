<div class="userindex-slider">
    <table class="slider-table table table-borderless" id="invoiceTable">
        <thead>
        <tr>
            <th class="table-th">Date</th>
            <th class="table-th">Description</th>
            <th class="table-th">Cost</th>
            <th class="table-th">Saving</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($data['invoices']))
            @foreach($data['invoices'] as $irow)
                <tr id="invoice_id_{{$irow->id}}">
                    <td>{{ date('d/m/Y', strtotime($irow->created_at)) }}</td>
                    <td class="w-80">
                        @if($irow->invoice_type=='Job')
                            Invoice {{$irow->invoice_no}}&nbsp;-&nbsp;&nbsp;JOBS
                        @else
                            Invoice {{$irow->invoice_no}}&nbsp;-&nbsp;Lifestyle Healthcheck
                        @endif
                    </td>
                    <td class="text-green">£{{$irow->cost}}</td>
                    <td>
                        £{{$irow->saving}}
                        <div class="form-group mb-0 float-right">
                            @if($irow->status ==0)
                                <a class="mydropdown" onClick="if (confirm(&quot;Are you sure you want to Edit this Invoice ?&quot;)) { editInvoice('{{$irow->invoice_type}}',{{$irow->id}}) } event.returnValue = false; return false;" style="cursor:pointer;color:#FFFFFF">&nbsp;&nbsp;Edit&nbsp;&nbsp;</a>
                            @else
                                <a class="mydropdown" style="cursor:pointer;color:#FFFFFF">&nbsp;&nbsp;Paid&nbsp;&nbsp;</a>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
<script>
    function editInvoice(invoice_type=0, id=0)
    {
        var invoice_type = invoice_type;
        var id = id;
        $.ajax({
            type: "post",
            async: true,
            dataType: "json",
            url: "{{route('editInvoiceSA')}}",
            data: {
                '_token': "{{csrf_token()}}",
                id: id,
                invoice_type: invoice_type
            },
            success: function(res) {
                $('#nav-Invoices-tab').removeClass("active");
                $('#nav-Invoices').removeClass("active");
                $('#nav-Invoices').removeClass("show");

                if(invoice_type ==='LHC'){
                    $('#nav-tracked-tab').addClass("active");
                    $('#nav-tracked').addClass("active");
                    $('#nav-tracked').addClass("show");
                    $('a[href="#nav-tracked"]').trigger("click");
                }
                if(invoice_type ==='Job'){
                    $('#nav-profiletrack-tab').addClass("active");
                    $('#nav-profiletrack').addClass("active");
                    $('#nav-profiletrack').addClass("show");
                    $('a[href="#nav-profiletrack"]').trigger("click");
                }
            }
        });
    }

</script>
