@extends('layouts.app')
@section('content')
<!--banner start here-->
<section>
   <div class="container home_login">
         <div class="row banner banner-in">
            <div class="col-lg-7" data-aos="fade-right">
               <div class="auth_login">
               <h1>Hi {{ Auth::user()->forename }}.</h1>
               <p>Let’s get to work.</p>
            </div>
            </div>
            <div class="col-lg-5" data-aos="fade-left">
               <div class="banner-img">
                  <img class="typing" src="{{ asset('public/theme/images/SafeWatch Illustrations_Dashboard.svg')}}" />
               </div>
            </div>
         </div>
   </div>
</section>
<!--banner ends here-->
<!--overview start here-->
<section>
   <div class="container">
      <div class="row overview-block">
         <div class="col-lg-12">
            <h2>Overview</h2>
         </div>
         <div class="col-lg-6">
            <ul class="overview-tile">
               <li class="fix-tile">
                  <p>Clients <br>{{ !empty($tmyclient)? $tmyclient: 0 }}</p>
               </li>
               <li>
                  <h4>Commissions</h4>
                  <div class="overview-detail">
                     <div class="overview-detailin">
                        <p class="text-pink">Monthly</p>
                        <p class="text-pink">£ {{ !empty($tmyclient)? $tmyclient*4: 0 }}</p>
                     </div>
                     <div class="overview-detailin">
                        <p class="text-pink">Jobs</p>
                        <p class="text-pink">£0.00</p>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
         <div class="col-lg-6">
            <ul class="overview-tile">
               <li class="fix-tile">
                  <p>Agents <br> {{ !empty($tmyagent)? $tmyagent: 0 }}</p>
               </li>
               <li>
                  <h4>Commissions</h4>
                  <div class="overview-detail">
                     <div class="overview-detailin">
                        <p class="text-pink">Monthly</p>
                        <p class="text-pink">£ {{ !empty($tAgentPaidClient)? $tAgentPaidClient*1: 0 }}</p>
                     </div>
                     <div class="overview-detailin">
                        <p class="text-pink">Jobs</p>
                        <p class="text-pink">£0.00</p>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!--overview ends here-->
<!--hero-tabs start here-->
<section class="parsonal-assitance-tabs">
   <div class="container herotabs">
      <ul class="nav nav-pills herotabs-ul" id="pills-tab" role="tablist">
         <li class="nav-item herotabs-li">
            <a class="herotabs-active active" id="pills-home1-tab" data-toggle="pill" href="#pills-home1" role="tab" aria-controls="pills-home1" aria-selected="true">
               <h5>Inbox</h5>
               <p>Click here <i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="herotabs-li">
            <a class="herotabs-active ajaxViewcls" id="pills-home2-tab" data-toggle="pill" href="#pills-home2" data-view="planner" role="tab" aria-controls="pills-home2" aria-selected="false">
               <h5>Planner</h5>
               <p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="herotabs-li">
            <a class="herotabs-active ajaxViewcls" id="pills-home3-tab" data-toggle="pill" href="#pills-home3" data-view="client" role="tab" aria-controls="pills-home3" aria-selected="false">
               <h5>Clients</h5>
               <p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="nav-item herotabs-li">
            <a class="herotabs-active ajaxViewcls" id="pills-home4-tab" data-toggle="pill" href="#pills-home4" data-view="manage" role="tab" aria-controls="pills-home4" aria-selected="false">
               <h5>Manage</h5>
               <p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
      </ul>
   </div>
   <div class="container tab-content herotabs-content no-pdg" id="pills-tabContent">
      <div class="container  tab-pane fade  show active" id="pills-home1" role="tabpanel" aria-labelledby="pills-home1-tab">
         <div class="container herotabs-content-in">
            <div class="row">
               <div class="col-lg-12">
                  <div class="user-index">
                     <h2>Inbox</h2>
                     <ul class="userindex-ul">
                       @if(!empty($messages))
                        @foreach ($messages as $message)
                        @if($message->msg_type =='inbox')
                        <li>
                           <a class="mark-li" href="#">
                              <div class="index-detail">
                                  <p>{{$message->name}} – {{ $message->subject }}</p>
                                  <p>{{ $message->message }}</p>
                              </div>
                              <p class="index-date">{{ date('d/m/Y', strtotime($message->created_at)) }}</p>
                           </a>
                        </li>
                        @else
                        <li>
                           <a class="dot-li" href="#">
                              <div class="index-detail">
                                 <p>{{ $message->subject }} <small>({{$message->name}})</small>@if(!empty
                                  ($message->invoice_no)) – Invoice {{$message->invoice_no }} Paid @else – Agent
                                    @endif</p>
                                  <p>{{$message->message }}</p>
                              </div>
                              <p class="index-date">{{ date('d/m/Y', strtotime($message->created_at)) }}</p>
                           </a>
                        </li>
                       @endif
                       @endforeach
                       @else
                       <div class="index-detail">
                          <p>There is no data!<p>
                       </div>
                       @endif
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container tab-pane fade" id="pills-home2" role="tabpanel" aria-labelledby="pills-home2-tab">
         <div class="plannerajax-cls" id="planner"></div>
      </div>

      <div class="container tab-pane fade" id="pills-home3" role="tabpanel" aria-labelledby="pills-home3-tab">
         <div class="clientajax-cls" id="client"></div>
      </div>

      <div class="tab-pane fade" id="pills-home4" role="tabpanel" aria-labelledby="pills-home4-tab">
          <div class="clientajax-cls" id="manage"></div>
      </div>
   </div>
</section>
<!--hero-tabs ends here-->
<script>
$(document).on('click', '.ajaxViewcls', function(e) {
    e.preventDefault();
    var view = $(this).attr("data-view");
    $.LoadingOverlay("show");
    if(view != ''){
        $.ajax({
            type: "post",
            async: true,
            url: "{{route('ajaxViewSA')}}",
            data: {
                '_token': "{{csrf_token()}}",
                view: view,
            },
            success: function(data) {
                $('#'+view).html(data.html);
                $.LoadingOverlay("hide");
            },error: function() {
                $.LoadingOverlay("hide");
                toastr.warning('Alert!', 'Loading Problem. Try Again!');
            }
        });
    }
});
</script>
@endsection
