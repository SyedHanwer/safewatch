<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="x-apple-disable-message-reformatting">
	<title>SafeWatch</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<style>
		.bg_white-header{
			background: #474b9a;
			padding: 23px 0px;
		}
		.bg_white{
			background: #ffffff;
		}
		.bg_light{
			background: #e2e1dd;
		}
		.bg_black{
			background: #000000;
		}
		.bg_dark{
			background: rgba(0,0,0,.8);
		}
		.email-section{
			padding:1.5em 2.5em 2.5em 2.5em;
		}
		.heading-section strong{
			color: #7f7f7f;
		}
		.heading-section p{
			color: #6c6c6c;
			font-size: 11.5px;
		}
		.heading-section a{
			color: #fff;
			padding: 15px 0px;
			background-color: #f6c67a;
			float: left;
			width: 165px;
			text-align: center;
			font-size: 13px;
			font-weight: 500;
			text-decoration:none;
			margin-top: 20px;
		}
		h1,h2,h3,h4,h5,h6{
			font-family: 'Poppins', sans-serif;
			color: #000000;
			margin-top: 0;
		}
		body{
			font-family: 'Poppins', sans-serif;
			font-weight: 400;
			font-size: 15px;
			line-height: 1.8;
			color: rgba(0,0,0,.4);
		}
		/*HERO*/
		.hero{
			position: relative;
			z-index: 0;
			height:140px;
		}
		.hero .overlay{
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			content: '';
			width: 100%;
			background-color:linear-gradient(to right, #eb5980, #f07f7e);
			z-index: -1;
		}
		.hero{
    background: linear-gradient(to right, rgba(255,75,112,1) 0%,rgba(255,83,111,1) 17%,rgba(255,104,111,1) 33%,rgba(255,114,112,1) 67%,rgba(244,129,108,1) 83%,rgba(255,134,113,1) 100%);
}
		.hero .text h2{
			color: #ffffff;
			font-size: 40px;
			margin-bottom: 0;
			font-weight: 600;
			line-height: 40px;
		}
		.heading-section-white{
			color: rgba(255,255,255,.8);
		}
		.heading-section-white h2{
			line-height: 1;
			padding-bottom: 0;
		}
		.heading-section-white h2{
			color: #ffffff;
		}
		.CToWUd{
		    margin-top:-14%;
		}
	</style>
</head>
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #e2e1dd;">
<center style="width: 100%; background-color: #e2e1dd;">
	<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
		&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
	</div>
	<div style="max-width: 600px; margin: 0 auto;" class="email-container">
		<!-- BEGIN BODY -->
		<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
			<tr>
				<td valign="top" class="bg_white-header" style="padding: 1.5em 2.5em;">
					<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="40%" class="logo" style="text-align: left;">
								<img src='{{config("app.url")}}public/theme/mails/Safewatch_Logo_White.png' style='width:40%;'/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- end tr -->
				<tr>
          <td valign="middle" class="hero " style="">
          	<div class="overlay"></div>
            <table>
            	<tr>
            		<td>
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            			<div class="text" style="padding: 0 3em; text-align: left;">
            				<h2>New agent <br> registered</h2>
            				
            			</div>
						</table>
            		</td>
					<td  style="text-align:right;">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<img src="{{config("app.url")}}public/theme/mails/SafeWatch_Illustrations_Messages.png" style="margin-right: 70px;margin-top: -45px;width:100%;"/>
					</td>
					</table>
            	</tr>
            </table>
          </td>
	      </tr><!-- end tr -->
	      <tr>
		      <td class="bg_white">
		        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		          <tr>
		            <td class="bg_white email-section">
		            	<div class="heading-section" style="text-align: left; padding: 0 px;">
		              	<strong>Hi {{$data['superForename']?$data['superForename']:''}},</strong>
		              	<p><b>{{$data['toForename']?$data['toForename']:''}} {{$data['toSurname']?$data['toSurname']:''}}</b> is now a registered agent of <b>{{$data['fromForename']?$data['fromForename']:''}} {{$data['fromSurname']?$data['fromSurname']:''}}</b> </p>
		            	
		            	<p style="margin-bottom:0px;">Kind regards,</p>
		            	<strong style="width:100%;float:left;margin-top:-7px;">The SafeWatch team</strong>
						<a href="{{url('/login')}}">log in</a>
						</div>
		            	
		            </td>
						</tr>
						<!-- end: tr -->
						<tr>
							<td class="bg_light email-section" style="width: 100%;padding: 0em 0em 0em 0em;">
								<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td valign="middle" width="50%">
											<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
												<tr>
													<td>
														<p style="font-size: 11px;color: #6c6c6c;">View in Browser  |  Unsubscribe  |  Contact</p>
													</td>
												</tr>
											</table>
										</td>
										<td valign="middle" width="50%">
											<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
												<tr>
													<td class="text-services" style="text-align:right; ">
														<div class="heading-section">
															<p style="font-size: 11px;color: #6c6c6c; "> © Copyright 2020 SafeWatch. All rights reserved.</p>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<!-- end: tr -->
					</table>
				</td>
			</tr>
			<!-- end:tr -->
			<!-- 1 Column Text + Button : END -->
		</table>
	</div>
</center>
</body>
</html>
