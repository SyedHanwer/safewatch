@extends('layouts.app')

@section('content')

<section> 

   <div class="container">

      <div class="row banner-in banner bg-contact">

         <div class="col-lg-7" data-aos="fade-right">

            <form class="login-form" action="{{route('addcontact')}}" method="POST" enctype="multipart/form-data">

               {{csrf_field()}}

               <h1>Contact us</h1>

               <p>

                  You can get in touch with us by calling

                  one of the team on 01274 288788

                  or by completing the form below. 

               </p>

               <div class="form-group">

                  <label for="name">Your Name</label>

                  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" aria-describedby="name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                  @error('name')

                  <span class="invalid-feedback" role="alert">

                  <strong>{{ $message }}</strong>

                  </span>

                  @enderror

               </div>

               <div class="form-group">

                  <label for="phone">Telephone</label>

                  <input type="text" class="form-control  @error('telphone') is-invalid @enderror" id="phone" name="telphone"  value="{{ old('telphone') }}" required autocomplete="telphone" autofocus>

                  @error('telphone')

                  <span class="invalid-feedback" role="alert">

                  <strong>{{ $message }}</strong>

                  </span>

                  @enderror

               </div>

               <div class="form-group">

                  <label for="message">Your Message</label>

                  <textarea class="form-control @error('message') is-invalid @enderror" id="message" rows="5" name="message" value="{{ old('message') }}" required autocomplete="message" autofocus></textarea>

                  @error('message')

                  <span class="invalid-feedback" role="alert">

                  <strong>{{ $message }}</strong>

                  </span>

                  @enderror

               </div>

               <div class="banner-btn">

                  <button class="btn-yellow" type="submit" name="save" value="save">SUBMIT</button>
               </div>

            </form>

            </div>

            <div class="col-lg-5" data-aos="fade-left">

               <div class="banner-img">

                  <img src="{{asset('public/theme/images/bannernew1.png')}}"/>

               </div>

            </div>

         </div>

      </div>


</section>

@endsection