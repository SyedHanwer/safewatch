@extends('layouts.app')
@section('content')
    <!--banner start here-->
    <section>
        <div class="container">
            <div class="row banner banner-in bg-contact">
                <div class="col-lg-7" data-aos="fade-right">
                    <div class="banner-btn">
                        <form class="login-form" action="{{route('registrationSave')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="passwordToken" value="{{ $data->passwordToken }}">
                            <h1>Take control of your <span>finances with our</span> expert support</h1>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password-confirm">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" autofocus>
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="banner-btn">
                                <button class="btn-yellow" type="submit" name="save" value="save">SAVE</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5" data-aos="fade-left">
                    <div class="banner-img">
                        <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Safe.svg')}}" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--banner ends here-->
@endsection