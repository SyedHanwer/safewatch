@extends('layouts.app')
@section('content')
<!--banner start here-->
<section>
    <div class="container"> 
            <div class="row banner-in banner login-baner">
                <div class="col-lg-6" data-aos="fade-right">
                     @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <form method="POST" class="login-form" action="{{ route('login') }}">
                    @csrf
                        <h1>Welcome back</h1>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus aria-describedby="emailHelp">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="banner-btn">
                            <button type="submit" class="btn-yellow">{{ __('Login') }}</button>
                        </div>

                    </form>
                </div>
                <div class="col-lg-6" data-aos="fade-left">
                    <div class="banner-img">
                        <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Safe.svg')}}" />
                    </div>
                </div>
            </div>
    </div>
</section>
<!--banner ends here-->
@endsection
