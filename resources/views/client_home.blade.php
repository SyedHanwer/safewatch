@extends('layouts.app')
@section('content')
    <!--banner start here-->
    <section class=" container banner">
        <div class="container">
            <div class="banner-in">
                <div class="row">
                    <div class="col-lg-7" data-aos="fade-right">
                        <h1>Welcome back <span class="d-block">{{ Auth::user()->forename }}</span></h1>
                        <p>How can we help you today?</p>
                    </div>
                    <div class="col-lg-5" data-aos="fade-left">
                        <div class="banner-img">
                            <img class="typing" src="{{ asset('public/theme/images/SafeWatch Illustrations_Messages.svg')}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--banner ends here-->

    <!--hero-tabs start here-->
    <section class=" parsonal-assitance-tabs">
        <div class="herotabs  container ">
            <ul class="nav nav-pills herotabs-ul" id="pills-tab" role="tablist">
                <li class="nav-item herotabs-li">
                    <a class="herotabs-active active " id="pills-home11-tab" data-toggle="pill" href="#pills-home11"
                       role="tab" aria-controls="pills-home11" aria-selected="true">
                        <h5>Inbox</h5><p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
                    </a>
                </li>
                <li class="herotabs-li">
                    <a class="herotabs-active clientAjaxView" id="pills-home22-tab" data-toggle="pill"
                       href="#pills-home22" data-view="invoices"
                       role="tab" aria-controls="pills-home22" aria-selected="false"><h5>Invoices</h5>
                        <p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
                    </a>
                </li>
                <li class="herotabs-li">
                    <a class="herotabs-active clientAjaxView" id="pills-home3-tab" data-toggle="pill"
                       href="#pills-home33" data-view="mysafewatch" role="tab" aria-controls="pills-home33"
                       aria-selected="false">
                        <h5>My SafeWatch</h5><p>Click here<i class="fa fa-angle-right" aria-hidden="true"></i></p>
                    </a>
                </li>
                <li class="nav-item herotabs-li">
                    <a class="herotabs-active clientAjaxView" id="pills-home44-tab" data-toggle="pill"
                       href="#pills-home44" data-view="contact" role="tab" aria-controls="pills-home4"
                       aria-selected="false">
                        <h5>Contact</h5><p>Click here <i class="fa fa-angle-right" aria-hidden="true"></i></p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content herotabs-content container" id="pills-tabContent">
            <div class="tab-pane fade  show active" id="pills-home11" role="tabpanel" aria-labelledby="pills-home11-tab">
                <div class="herotabs-content-in">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="user-index">
                                <h2>Inbox</h2>
                                <ul class="userindex-ul">
                                    @if(count($messages) !=0)
                                        @foreach ($messages as $message)
                                            @if($message->msg_type =='inbox')
                                            <li>
                                                <a class="mark-li" href="#">
                                                    <div class="index-detail">
                                                        <p>{{$message->name}} – {{ $message->subject }}</p>
                                                        <p>{{ $message->message }}</p>
                                                    </div>
                                                    <p class="index-date">{{ date('d/m/Y', strtotime($message->created_at)) }}</p>
                                                </a>
                                            </li>
                                            @elseif($message->msg_type =='Invoices')
                                            <li>
                                                    <a class="dot-li invoicePageView" href="javascript:void(0)">
                                                        <div class="index-detail">
                                                            <p>{{ $message->subject }} – Invoice {{$message->invoice_no }}</p>
                                                            <p>{{$message->message }}</p>
                                                        </div>
                                                        <p class="index-date">{{ date('d/m/Y', strtotime($message->created_at)) }}</p>
                                                    </a>
                                                </li>
                                            @else
                                            <li>
                                                <a class="dot-li" href="#">
                                                    <div class="index-detail">
                                                        <p>{{ $message->subject }} <small>({{$message->name}})</small> – Invoice {{$message->invoice_no }} Paid</p>
                                                        <p>{{$message->message }}</p>
                                                    </div>
                                                    <p class="index-date">{{ date('d/m/Y', strtotime($message->created_at)) }}</p>
                                                </a>
                                            </li>
                                            @endif
                                        @endforeach
                                    @else
                                    <div class="index-detail">
                                        <p style="text-align: center;color: white;">There is no message!<p>
                                    </div>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-home22" role="tabpanel" aria-labelledby="pills-home22-tab">
                <div class="invoices-cls" id="invoices"></div>
            </div>
            <div class="tab-pane fade" id="pills-home33" role="tabpanel" aria-labelledby="pills-home33-tab">
                <div class="mysafewatch-cls" id="mysafewatch"></div>
            </div>
            <div class="tab-pane fade" id="pills-home44" role="tabpanel" aria-labelledby="pills-home44-tab">
                <div class="contact-cls" id="contact"></div>
            </div>
        </div>

    </section>
    <!--hero-tabs ends here-->
<script>
$(document).on('click', '.clientAjaxView', function(e) {
    e.preventDefault();
    var view = $(this).attr("data-view");
    $.LoadingOverlay("show");
    if(view != ''){
        $.ajax({
            type: "post",
            async: true,
            url: "{{route('clientAjaxView')}}",
            data: {
                '_token': "{{csrf_token()}}",
                view: view,
            },
            success: function(data) {
                $('#'+view).html(data.html);
                $.LoadingOverlay("hide");
            },error: function() {
                $.LoadingOverlay("hide");
                toastr.warning('Alert!', 'Loading Problem. Try Again!');
            }
        });
    }
});

$(document).ready(function(){
    $('.invoicePageView').click(function(){
        $('#pills-home11-tab').removeClass("active");
        $('#pills-home11').removeClass("show");
        $('#pills-home11').removeClass("active");

        $('#pills-home22-tab').addClass("active");
        $('#pills-home22').addClass("show");
        $('#pills-home22').addClass("active");
        $('a[href="#pills-home22"]').trigger("click");
    });
});

</script>
@endsection
