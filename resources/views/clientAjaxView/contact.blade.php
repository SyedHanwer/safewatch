<div class="herotabs-content-in">
    <div class="row">
        <div class="col-lg-12">
            <div class="herotabs-content-left">
                <h2>Contact</h2>
                <div class="userindex-slider" style="padding: 0; background: none;">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Contact your Personal Assistant</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Contact SafeWatch HQ</a>
                        </div>
                    </nav>
                    <div class="tab-content contact-tab" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <form class="safewatch-form contactfrm" id="contactfrm" method="post" action="#">
                                <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="subject">Subject</label>
                                            <input type="text" class="form-control" placeholder="" id="subject" name="subject">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="message">Your Message</label>
                                            <textarea type="text" class="form-control" placeholder="" id="message" name="message"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="formbtn-group">
                                            <button type="button" id="sendbtn" class="bg-green sendbtn">Send</button>
                                            <button type="reset" id="delbtn" class="bg-red">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane  fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <form class="safewatch-form contactformsw" id="contactformsw" method="post" action="#">
                                <div class="alert alert-danger print-errorsw" style="display:none">
                                    <ul></ul>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="subjectsw">Subject</label>
                                            <input type="text" class="form-control" placeholder="" id="subjectsw" name="Subject">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="messagesw">Your Message</label>
                                            <textarea type="text" class="form-control" placeholder="" id="messagesw"
                                                      name="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="formbtn-group">
                                            <button class="bg-green sendbutnsw" id="sendbutnsw">Send</button>
                                            <button type="reset" id="deltbtn" class="bg-red">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#sendbtn').click(function(e) {
        e.preventDefault();
        var subject = $('#subject').val();
        var message  = $('#message').val();
        $.ajax({
            type: "post",
            url: "{{route('sendcontactmsg')}}",
            data: {
                '_token': "{{csrf_token()}}",
                subject: subject,
                message:message,
            },
            dataType: "json",
            success: function(res) {
                if($.isEmptyObject(res.error)){
                    $(".print-error-msg").css('display','none');
                    if(res.status == 'save'){
                        toastr.success('Success!', 'Thank! Message Send Successfully.');
                        $("#contactfrm")[0].reset();
                    }else if(res.status == 'error'){
                        toastr.error("error!", "Sorry! You don\'t have agent.");
                    }else {
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }else{
                    printErrorMsg(res.error);
                    toastr.error('error!', 'Some Fields are Required! Please Fill.');
                }
            }
        });
    });
    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }

    $('#sendbutnsw').click(function(e) {
        e.preventDefault();
        var Subject = $('#subjectsw').val();
        var Message  = $('#messagesw').val();
        $.ajax({
            type: "post",
            url: "{{route('msgSendToSW')}}",
            data: {
                '_token': "{{csrf_token()}}",
                Subject: Subject,
                Message:Message,
            },
            dataType: "json",
            success: function(res) {
                if($.isEmptyObject(res.error)){
                    $(".print-errorsw").css('display','none');
                    if(res.status == 'save'){
                        toastr.success('Success!', 'Thank! Message Send Successfully.');
                        $("#contactformsw")[0].reset();
                    }else {
                        toastr.error('error!', 'Some Problem Here. Try Again!');
                    }
                }else{
                    printErrorMsgSW(res.error);
                    toastr.error('error!', 'Some Fields are Required! Please Fill.');
                }
            }
        });
    });
    function printErrorMsgSW(msg) {
        $(".print-errorsw").find("ul").html('');
        $(".print-errorsw").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-errorsw").find("ul").append('<li>'+value+'</li>');
        });
    }
});

</script>