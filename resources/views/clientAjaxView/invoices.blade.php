<style>
    /**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */


/*
 * 	Default theme - Owl Carousel CSS File
 */

.owl-theme .owl-nav {
    margin-top: 10px;
    text-align: center;
    -webkit-tap-highlight-color: transparent;
}

.owl-theme .owl-nav [class*='owl-'] {
    color: #FFF;
    font-size: 14px;
    margin: 5px;
    padding: 4px 7px;
    background: #D6D6D6;
    display: inline-block;
    cursor: pointer;
    border-radius: 3px;
}

.owl-theme .owl-nav [class*='owl-']:hover {
    background: #869791;
    color: #FFF;
    text-decoration: none;
}

.owl-theme .owl-nav .disabled {
    opacity: 0.5;
    cursor: default;
}

.owl-theme .owl-nav.disabled + .owl-dots {
    margin-top: 10px;
}

.owl-theme .owl-dots {
    text-align: center;
    -webkit-tap-highlight-color: transparent;
}

.owl-theme .owl-dots .owl-dot {
    display: inline-block;
    zoom: 1;
    display: inline;
}

.owl-theme .owl-dots .owl-dot span {
    width: 10px;
    height: 10px;
    margin: 5px 7px;
    background: #D6D6D6;
    display: block;
    -webkit-backface-visibility: visible;
    transition: opacity 200ms ease;
    border-radius: 30px;
}

.owl-theme .owl-dots .owl-dot.active span,
.owl-theme .owl-dots .owl-dot:hover span {
    background: #869791;
}
/**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */

 .owl-carousel,
 .owl-carousel .owl-item {
     -webkit-tap-highlight-color: transparent;
     position: relative
 }
 
 .owl-carousel {
     display: none;
     width: 100%;
     z-index: 1
 }
 
 .owl-carousel .owl-stage {
     position: relative;
     -ms-touch-action: pan-Y;
     touch-action: manipulation;
     -moz-backface-visibility: hidden
 }
 
 .owl-carousel .owl-stage:after {
     content: ".";
     display: block;
     clear: both;
     visibility: hidden;
     line-height: 0;
     height: 0
 }
 
 .owl-carousel .owl-stage-outer {
     position: relative;
     overflow: hidden;
     -webkit-transform: translate3d(0, 0, 0)
 }
 
 .owl-carousel .owl-item,
 .owl-carousel .owl-wrapper {
     -webkit-backface-visibility: hidden;
     -moz-backface-visibility: hidden;
     -ms-backface-visibility: hidden;
     -webkit-transform: translate3d(0, 0, 0);
     -moz-transform: translate3d(0, 0, 0);
     -ms-transform: translate3d(0, 0, 0)
 }
 
 .owl-carousel .owl-item {
     min-height: 1px;
     float: left;
     -webkit-backface-visibility: hidden;
     -webkit-touch-callout: none
 }
 
 .owl-carousel .owl-item img {
     display: block;
     width: 100%
 }
 
 .owl-carousel .owl-dots.disabled,
 .owl-carousel .owl-nav.disabled {
     display: none
 }
 
 .no-js .owl-carousel,
 .owl-carousel.owl-loaded {
     display: block
 }
 
 .owl-carousel .owl-dot,
 .owl-carousel .owl-nav .owl-next,
 .owl-carousel .owl-nav .owl-prev {
     cursor: pointer;
     -webkit-user-select: none;
     -khtml-user-select: none;
     -moz-user-select: none;
     -ms-user-select: none;
     user-select: none
 }
 
 .owl-carousel .owl-nav button.owl-next,
 .owl-carousel .owl-nav button.owl-prev,
 .owl-carousel button.owl-dot {
     background: 0 0;
     color: inherit;
     border: none;
     padding: 0!important;
     font: inherit
 }
 
 .owl-carousel.owl-loading {
     opacity: 0;
     display: block
 }
 
 .owl-carousel.owl-hidden {
     opacity: 0
 }
 
 .owl-carousel.owl-refresh .owl-item {
     visibility: hidden
 }
 
 .owl-carousel.owl-drag .owl-item {
     -ms-touch-action: pan-y;
     touch-action: pan-y;
     -webkit-user-select: none;
     -moz-user-select: none;
     -ms-user-select: none;
     user-select: none
 }
 
 .owl-carousel.owl-grab {
     cursor: move;
     cursor: grab
 }
 
 .owl-carousel.owl-rtl {
     direction: rtl
 }
 
 .owl-carousel.owl-rtl .owl-item {
     float: right
 }
 
 .owl-carousel .animated {
     animation-duration: 1s;
     animation-fill-mode: both
 }
 
 .owl-carousel .owl-animated-in {
     z-index: 0
 }
 
 .owl-carousel .owl-animated-out {
     z-index: 1
 }
 
 .owl-carousel .fadeOut {
     animation-name: fadeOut
 }
 
 @keyframes fadeOut {
     0% {
         opacity: 1
     }
     100% {
         opacity: 0
     }
 }
 
 .owl-height {
     transition: height .5s ease-in-out
 }
 
 .owl-carousel .owl-item .owl-lazy {
     opacity: 0;
     transition: opacity .4s ease
 }
 
 .owl-carousel .owl-item .owl-lazy:not([src]),
 .owl-carousel .owl-item .owl-lazy[src^=""] {
     max-height: 0
 }
 
 .owl-carousel .owl-item img.owl-lazy {
     transform-style: preserve-3d
 }
 
 .owl-carousel .owl-video-wrapper {
     position: relative;
     height: 100%;
     background: #000
 }
 
 .owl-carousel .owl-video-play-icon {
     position: absolute;
     height: 80px;
     width: 80px;
     left: 50%;
     top: 50%;
     margin-left: -40px;
     margin-top: -40px;
     background: url(owl.video.play.png) no-repeat;
     cursor: pointer;
     z-index: 1;
     -webkit-backface-visibility: hidden;
     transition: transform .1s ease
 }
 
 .owl-carousel .owl-video-play-icon:hover {
     -ms-transform: scale(1.3, 1.3);
     transform: scale(1.3, 1.3)
 }
 
 .owl-carousel .owl-video-playing .owl-video-play-icon,
 .owl-carousel .owl-video-playing .owl-video-tn {
     display: none
 }
 
 .owl-carousel .owl-video-tn {
     opacity: 0;
     height: 100%;
     background-position: center center;
     background-repeat: no-repeat;
     background-size: contain;
     transition: opacity .4s ease
 }
 
 .owl-carousel .owl-video-frame {
     position: relative;
     z-index: 1;
     height: 100%;
     width: 100%
 }

 .li-light-cls{
     color: #ffffff !important;
     font-size: 20px !important;
     font-weight: 400 !important;
 }
 .owl-carousel .owl-nav{
     display: none !important;
 }
</style>
<div class="tab-pane fade show active" id="pills-home2" role="tabpanel" aria-labelledby="pills-home2-tab">
    <div class="herotabs-content-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="user-index">
                    <h2>Invoices</h2>
                    <div class="userindex-slider-block">
                        <div class="backbtn">
                        <a class="tab-invoice">
                            <i class="fa fa-caret-left" aria-hidden="true"></i>
                            Back
                        </a>
                        </div>
                        <div class="owl-carousel owl-carousel-blog owl-theme" id="invoice">
                            <div class="item">
                                <div class="userindex-slider bgclr">
                                    <div class="user-index">
                                        <ul class="userindex-ul">
                                            @if(count($data['invoices']) !=0)
                                                @foreach($data['invoices'] as $val)
                                                    <li>
                                                        <a class="doller-li invoicePreView" href="javascript:void(0)" data-invoiceID="{{$val->id}}">
                                                            @if($val->status ==1)
                                                                <div class="index-detail">
                                                                    <img class="dollar-img" src="{{ asset('public/theme/images/dollar-green.png')}}">
                                                                    <p>{{$val->invoice_no}}</p>
                                                                    <p>{{$val->details}}</p>
                                                                </div>
                                                                <p class="index-date">
                                                                    <img class="paid-img" src="{{ asset('public/theme/images/paid-img.png')}}">
                                                                    {{ date('d/m/Y', strtotime($val->created_at)) }}
                                                                </p>
                                                            @else
                                                                <div class="index-detail">
                                                                    <img class="dollar-img" src="{{ asset('public/theme/images/dolllar-white.png')}}">
                                                                    <p>{{$val->invoice_no}}</p>
                                                                    <p>{{$val->details}}</p>
                                                                </div>
                                                                <p class="index-date">{{ date('d/m/Y', strtotime($val->created_at)) }}</p>
                                                            @endif
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li style="text-align: center;">There is no data!</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item" id="servicesSlide">
                                <div class="userindex-slider">
                                    <div class="slider-header">
                                        <div class="title">
                                            <p id="invoiceNoJob"></p>
                                            <p id="invoiceDateJob"></p>
                                        </div>
                                        <div class="title">
                                            <h4>Services</h4>
                                            <p>
                                                <a class="doller-li" href="javascript:void(0)" onclick="printDiv('Services')"><i class=" icon fa fa-print" aria-hidden="true"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                    <table class="slider-table table table-borderless">
                                        <thead>
                                            <tr>
                                                <th class="table-th">Service</th>
                                                <th class="table-th">Hrs/Units</th>
                                                <th class="table-th">Normal</th>
                                                <th class="table-th">SafeWatch</th>
                                                <th class="table-th">Saving</th>
                                                <th class="table-th">ToPay</th>
                                            </tr>
                                        </thead>
                                        <tbody id="invoiceBodyJob"></tbody>
                                    </table>
                                    <div class="saved-year">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p class="text-main">Total saved</p>
                                                        <p class="box-green" id="invoiceTSavedJob"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p>Total to pay</p>
                                                        <p id="invoiceTAmountJob"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-footer">
                                        <h4 class="text-main">Payment Information</h4>
                                        <ul>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Bank-Transfer.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Bank Transfer</h5>
                                                    <p>Safewatch <br>36334448  <br>52-30-29</p>
                                                </div>
                                            </li>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Cheques.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Cheques</h5>
                                                    <p>Payable to: <br>Safe Watch  <br></p>
                                                </div>
                                            </li>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Registered-Address.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Registered Address</h5>
                                                    <p>Safe Watch <br>50 St Peg Lane, Cleckheaton,   <br>West Yorkshire, BD19 3SD</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item" id="lhcSlide">
                                <div class="userindex-slider">
                                    <div class="slider-header">
                                        <div class="title">
                                            <p id="invoiceNoLhc"></p>
                                            <p id="invoiceDateLhc"></p>
                                        </div>
                                        <div class="title">
                                            <h4>Lifestyle Healthcheck</h4>
                                            <p>
                                                <a class="doller-li" href="javascript:void(0)" onclick="printDiv('lhc')"><i class=" icon fa fa-print" aria-hidden="true"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                    <table class="slider-table table table-borderless">
                                        <thead>
                                            <tr>
                                                <th class="table-th">Item</th>
                                                <th class="table-th">Provider</th>
                                                <th class="table-th">Benefits/Package</th>
                                                <th class="table-th">End Date</th>
                                                <th class="table-th">Monthly</th>
                                                <th class="table-th">Term</th>
                                                <th class="table-th">Saving (term)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="invoiceBodyLhc"></tbody>
                                    </table>
                                    <div class="saved-year">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p class="text-main">Total Saved</p>
                                                        <p class="box-green" id="invoiceTSavingLhc"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p>Members (25% fee)</p>
                                                        <p class="invoiceTAmountLhcM"></p>
                                                    </div>
                                                    <div class="savedyear-blockin">
                                                        <p>Non-members (50% fee)</p>
                                                        <p class="invoiceTAmountLhcNonM"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="slider-footer">
                                        <h4 class="text-main">Payment Information</h4>
                                        <ul>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Bank-Transfer.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Bank Transfer</h5>
                                                    <p>Safewatch <br>36334448  <br>52-30-29</p>
                                                </div>
                                            </li>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Cheques.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Cheques</h5>
                                                    <p>Payable to: <br>Safe Watch  <br></p>
                                                </div>
                                            </li>
                                            <li>
                                                <img src="{{ asset('public/theme/images/Registered-Address.png')}}">
                                                <div class="payment-tile">
                                                    <h5 class="text-main">Registered Address</h5>
                                                    <p>Safe Watch <br>50 St Peg Lane, Cleckheaton,   <br>West Yorkshire, BD19 3SD</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item" id="AnnualReportSlide">
                                <div class="userindex-slider">
                                    <div class="slider-header">
                                        <div class="title">
                                            <p id="invoiceYearAR"></p>
                                            <p id="invoiceDateAR"></p>
                                        </div>
                                        <div class="title">
                                            <h4>Annual Report</h4>
                                            <p>
                                                <a class="doller-li" href="javascript:void(0)" onclick="printDiv('AnnualReport')"><i class=" icon fa fa-print" aria-hidden="true"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                    <table class="slider-table table table-borderless">
                                        <thead>
                                            <tr>
                                                <th class="table-th">Date</th>
                                                <th class="table-th">Description</th>
                                                <th class="table-th">Cost</th>
                                                <th class="table-th">Saving</th>
                                            </tr>
                                        </thead>
                                        <tbody id="invoiceBodyAR">
                                            <tr>
                                                <td>10/10/16</td>
                                                <td class="w-90">Invoice 1993 – Services</td>
                                                <td>£280.00</td>
                                                <td class="text-green">£50.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="saved-year">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p class="text-main">Total saved this year</p>
                                                        <p class="box-green" id="invoiceTSavedAR">£</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="savedyear-block">
                                                    <div class="savedyear-blockin">
                                                        <p>Annual savings</p>
                                                        <p id="invoiceAnnualSavingAr">£</p>
                                                    </div>
                                                    <div class="savedyear-blockin">
                                                        <p>SafeWatch Fees</p>
                                                        <p id="invoiceSafewatchFeesAR">£</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slider-footer">
                                        <h4 class="text-main">We’re here to save you time and money</h4>
                                        <p>Tradesmen finder | Selling | Buying | Research and advice | PC and Tech Support | Holidays | Private PC tuition | Home improvements</p>
                                        <p class="text-pink">Get in touch with your personal assistant today to find out how we can help you save even more</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
AOS.init();
$('.owl-carousel-blog').owlCarousel({
	loop:false,
	margin:10,
	nav:true,
	items: 1
})

if($(window).width() < 767)
{
   // change functionality for smaller screens
   $('.tab-invoice').click(function(){
    $('.owl-stage').css("transform" , "translate3d(0px, 0px, 0px)");
  });
} else {
   $('.tab-invoice').click(function(){
    $('.owl-stage').css("transform" , "translate3d(0px, 0px, 0px)");
  });
}

$(document).ready(function(){
    $('.invoicePreView').click(function(){
        var invoiceID = $(this).attr("data-invoiceID");
        $.ajax({
            type: "post",
            async: true,
            dataType: "json",
            url: "{{route('invoicePreView')}}",
            data: {
                '_token': "{{csrf_token()}}",
                invoiceID: invoiceID,
            },
            success: function(res) {
                var invoice_type = res.data.invoices.invoice_type;
                if(invoice_type =='Job'){
                    $('.owl-stage').css("transform" , "translate3d(-1090px, 0px, 0px)");
                    $('#invoiceNoJob').text('Invoices '+res.data.invoices.invoice_no);
                    $('#invoiceDateJob').text(res.data.invoices.inv_date);
                    $('#invoiceTSavedJob').text('£ '+res.data.invoices.saving);
                    $('#invoiceTAmountJob').text('£ '+res.data.invoices.total_amount);
                    var html = '';
                    for (var i=0; i<res.data.data.length; i++) {
                        var job = res.data.data[i];
                        if(job.id !=''){
                            html +='<tr><td>'+job.details+'</td><td>'+job.units+'</td><td class="text-pink">£'+job.standard+'</td><td class="text-green">£'+job.member+'</td><td class="text-green">£'+job.saving+'</td><td>£'+job.amount+'</td></tr>';
                        }else{
                            html +='<tr style="text-align: center"><td colspan="6">Data Not Found!</td></tr>';
                        }
                    }
                    $('#invoiceBodyJob').html(html);
                }
                if(invoice_type =='LHC'){
                    $('.owl-stage').css("transform" , "translate3d(-2180px, 0px, 0px)");
                    $('#invoiceNoLhc').text('Invoices '+res.data.invoices.invoice_no);
                    $('#invoiceDateLhc').text(res.data.invoices.inv_date);
                    $('#invoiceTSavingLhc').text('£ '+res.data.invoices.saving);
                    $('.invoiceTAmountLhcM').text('£ '+res.data.invoices.profit);
                    $('.invoiceTAmountLhcNonM').text('£ '+res.data.invoices.nonMember);

                    var html = '';
                    for (var i=0; i<res.data.data.length; i++) {
                        var lhc = res.data.data[i];

                        var curEndDate = '-';
                        var newEndDate = '-';
                        if(lhc.curenddate !='' && lhc.curenddate !=null){
                            var rs1 = lhc.curenddate.split('-');
                            curEndDate = rs1[2]+'/'+rs1[1]+'/'+rs1[0];
                        }
                        if(lhc.newenddate !='' && lhc.newenddate !=null){
                            var rs2 = lhc.newenddate.split('-');
                            newEndDate = rs2[2]+'/'+rs2[1]+'/'+rs2[0];
                        }

                        if(lhc.id !=''){
                            html +='<tr><td>'+lhc.sname+'</td><td><p class="text-pink">'+lhc.curprovider+'</p><p class="text-main">'+lhc.newprovider+'</p></td><td><p class="text-pink">-</p><p class="text-main">£'+lhc.newsaving+'</p></td><td><p class="text-pink">'+curEndDate+'</p><p class="text-main">'+newEndDate+'</p></td><td><p class="text-pink">£'+lhc.curmonthlycost+'</p><p class="text-main">£'+lhc.newmonthlycost+'</p></td><td><p class="text-pink">£'+lhc.curtotalover+'</p><p class="text-main">£'+lhc.newtotalover+'</p></td><td><p class="text-pink"></p><p class="text-main">£'+lhc.newsaving+'</p></td></tr>';
                        }else{
                            html +='<tr style="text-align: center"><td colspan="7">There is not Data!</td></tr>';
                        }
                    }
                    $('#invoiceBodyLhc').html(html);
                }
            },error: function() {
                toastr.warning('Alert!', 'Loading Problem. Try Again OR Page Reload!');
            }
        });
    });
});

function printDiv(val){
    var val = val;

    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>Print View</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.
    frameDoc.document.write('<link href="http://localhost/safewatch/public/css/app.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/style.css" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<link href="http://localhost/safewatch/public/theme/css/fonts.css" rel="stylesheet" type="text/css" />');

    if(val == 'Services'){
        var contents = $("#servicesSlide").html();
        //Append the DIV contents.
        frameDoc.document.write(contents);
    }else if(val == 'lhc'){
        var contents = $("#lhcSlide").html();
        frameDoc.document.write(contents);
    }else if(val == 'AnnualReport'){
        var contents = $("#AnnualReportSlide").html();
        frameDoc.document.write(contents);
    }
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);
}

</script>