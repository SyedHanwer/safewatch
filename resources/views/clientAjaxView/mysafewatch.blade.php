<div class="herotabs-content-in">
    <div class="row">
        <div class="col-lg-12">
            <div class="herotabs-content-left">
                <h2>Your Service Providers</h2>
                <div class="userindex-slider">
                    <form class="safewatch-form">
                        @if(count($data['products']) != 0)
                            @foreach($data['products'] as $val)
                            <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="service">Service</label>
                                    <input type="text" class="form-control service" placeholder="" name="service[]" value="{{$val->name}}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="provider">Provider</label>
                                    <input type="text" class="form-control provider" placeholder="" name="provider[]" value="{{$val->newprovider}}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="">User Name</label>
                                    <input type="text" class="form-control" placeholder="" value="{{$val->username}}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control password" placeholder="" name="password[]" value="{{$val->pro_password}}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="notes">Notes</label>
                                    <input type="text" class="form-control notes" placeholder="" name="notes[]" value="{{$val->pro_notes}}" readonly>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="border-line"></div>
                            </div>
                        </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        There is no data!
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
