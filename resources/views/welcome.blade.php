@extends('layouts.app')
@section('content')
<!--banner start here-->
<section>
   <div class="container">
         <div class="row banner banner-in">
            <div class="col-lg-7" data-aos="fade-right">
               <h1>Take control of your <span>finances with our</span> expert support</h1>
               <div class="banner-btn">
                  <a href="#" class="btn-yellow">
                  Get In Touch
                  </a>
               </div>
            </div> 
            <div class="col-lg-5" data-aos="fade-left">
               <div class="banner-img">
                  <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Safe.svg')}}" />
               </div>
            </div>
         </div>
   </div>
</section>
<!--banner ends here-->
<!--hero-tabs start here-->
<section>
   <div class="container herotabs">
      <ul class="nav nav-pills herotabs-ul" id="pills-tab" role="tablist">
         <li class="nav-item herotabs-li">
            <a class="herotabs-active " id="pills-home1-tab" data-toggle="pill" href="#pills-home1" role="tab" aria-controls="pills-home1" aria-selected="true">
               <h5>Lifestyle healthcheck</h5>
               <p>Find Out more <i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="herotabs-li">
            <a class="herotabs-active" id="pills-home2-tab" data-toggle="pill" href="#pills-home2" role="tab" aria-controls="pills-home2" aria-selected="false">
               <h5>Tech and gadget</h5>
               <p>Find Out more <i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="herotabs-li">
            <a class="herotabs-active active" id="pills-home3-tab" data-toggle="pill" href="#pills-home3" role="tab" aria-controls="pills-home3" aria-selected="false">
               <h5>Buying and selling</h5>
               <p>Find Out more <i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
         <li class="nav-item herotabs-li">
            <a class="herotabs-active" id="pills-home4-tab" data-toggle="pill" href="#pills-home4" role="tab" aria-controls="pills-home4" aria-selected="false">
               <h5>Home development</h5>
               <p>Find Out more <i class="fa fa-angle-right" aria-hidden="true"></i></p>
            </a>
         </li>
      </ul>
   </div>
   <div class="container tab-content herotabs-content" id="pills-tabContent">
      <div class="tab-pane fade " id="pills-home1" role="tabpanel" aria-labelledby="pills-home1-tab">
         <div class="container herotabs-content-in">
            <div class="row">
               <div class="col-lg-7">
                  <div class="herotabs-content-left">
                     <h2>Lifestyle healthcheck</h2>
                     <p>
                        Our goal is to save you TIME by doing all the work for you and save you
                        MONEY by getting you the best deals. Including Gas, Electric, TV packages,
                        broadband, mobile contracts and all your insurances.
                     </p>
                     <ul>
                        <h5>Key features</h5>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/1.png')}}" alt="icon">
                           <p>£500 a year average saving</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/2.png')}}" alt="icon">
                           <p>Quality is our priority</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/3.png')}}" alt="icon">
                           <p>Putting you first</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="tab-img">
                     <img src="{{ asset('public/theme/images/SafeWatch Illustrations_LHC.svg')}}">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="pills-home2" role="tabpanel" aria-labelledby="pills-home2-tab">
         <div class="container herotabs-content-in">
            <div class="row">
               <div class="col-lg-7">
                  <div class="herotabs-content-left">
                     <h2>Tech and gadget</h2>
                     <p>
                        We offer free advice and a wide range of support with buying, selling & operating all types
                        of household tech. Mobile phones,
                        iPads, TV boxes, Laptops, whatever the item we can help you.
                     </p>
                     <ul>
                        <h5>Key features</h5>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/1.png')}}" alt="icon">
                           <p>Free advice</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/2.png')}}" alt="icon">
                           <p>Free telephone support</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/3.png')}}" alt="icon">
                           <p>We’re here to help</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="tab-img">
                     <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Tech_Gadgets.svg')}}">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade show active" id="pills-home3" role="tabpanel" aria-labelledby="pills-home3-tab">
         <div class="container herotabs-content-in">
            <div class="row">
               <div class="col-lg-7">
                  <div class="herotabs-content-left">
                     <h2>Buying and selling</h2>
                     <p>
                        Anything you need we can help, from buying kettles and toasters to
                        cars and houses, we help with it all and everything between. We offer
                        full support including advice, purchasing, delivery and setting up. 
                     </p>
                     <ul>
                        <h5>Key features</h5>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/1.png')}}" alt="icon">
                           <p>Saving you money</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/2.png')}}" alt="icon">
                           <p>Saving you time</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/3.png')}}" alt="icon">
                           <p>Free advice</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="tab-img">
                     <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Buying_Selling.svg')}}">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="pills-home4" role="tabpanel" aria-labelledby="pills-home4-tab">
         <div class="container herotabs-content-in">
            <div class="row">
               <div class="col-lg-7">
                  <div class="herotabs-content-left">
                     <h2>Home development</h2>
                     <p>
                        We help with all types of home improvements from painting and
                        platering to kitchens and bathrooms, we also offer full support
                        with extensions from design through planning to completion.
                        Everything project managed to save you stress and time.
                     </p>
                     <ul>
                        <h5>Key features</h5>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/1.png')}}" alt="icon">
                           <p>Saving you money</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/2.png')}}" alt="icon">
                           <p>Saving you time</p>
                        </li>
                        <li class="hvr-icon-buzz-out">
                           <img class="hvr-icon" src="{{ asset('public/theme/images/3.png')}}" alt="icon">
                           <p>Quality design service</p>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-5">
                  <div class="tab-img">
                     <img src="{{ asset('public/theme/images/SafeWatch Illustrations_Home_Dev.svg')}}">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--hero-tabs ends here-->
@endsection