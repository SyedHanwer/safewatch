<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'id','name', 'telphone', 'message','user_id','status',
    ];

}
