<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'client_id', 'name', 'forename', 'surname', 'dob', 'address1', 'address2', 'address3', 'home_no','mobile_no', 'dateoffirstcontact', 'dateofmembersince', 'email', 'password', 'postcode', 'referredby', 'preferred_contact', 'notes', 'status', 'isdeleted', 'role', 'parent_id','tiers','passwordToken',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
