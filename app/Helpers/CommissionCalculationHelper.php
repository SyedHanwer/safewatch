<?php

namespace App\Helpers;

use App\Invoice;
use App\User;
use App\Commission_setting;
use App\Commission;
use App\Commission_history;
use App\Income;

use Auth;
use DB;

class CommissionCalculationHelper
{

    public static function commissionCalculateFun($id)
    {
        if(!empty($id)){
            $invoices = DB::table('invoices')
                ->join('users', 'invoices.inv_from', '=', 'users.id')
                ->select('invoices.id','invoices.inv_from','invoices.inv_to','invoices.invoice_no','invoices.total_amount','invoices.cost','invoices.profit','users.id as uid','users.parent_id', 'users.tiers')
                ->where('invoices.id', $id)
                ->first();

//            $superAgent = User::select('id')->where('id', 1)
//                ->where('role', 2)
//                ->where('status', 1)
//                ->where('isdeleted', 0)
//                ->first();
//            $superAgentId = $superAgent->id;
            $commissionSetting = Commission_setting::select('id','type','tiers','profit')->where('status', 1)->get();
            $i =0;
            if(!empty($invoices)) {

                $inv_from           = $invoices->inv_from;
                $invoice_id         = $invoices->id;
                $inv_to             = $invoices->inv_to;
                $total_amount       = $invoices->total_amount;
                $profit             = $invoices->profit;
                $cost               = $invoices->cost;
                $invoice_no         = $invoices->invoice_no;
                $invoiceTiers       = $invoices->tiers;
                $invoiceParentId    = $invoices->parent_id;
                $seniorAgent        = NULL;
                $mostSeniorAgentID  = NULL;

                $user_id = $invoices->inv_from;
                $agentResult = User::select('parent_id')->where('id', $user_id)->first();
                $agentId = $agentResult->parent_id;
                $loginUserId = Auth::user()->id;

                if($agentId === $loginUserId){
                    $seniorAgent = $invoiceParentId;
                    if(!empty($seniorAgent)){
                        $mostSeniorAgentID = User::select('parent_id')->where('id',$seniorAgent)->first();
                        $mostSeniorAgentID = $mostSeniorAgentID->parent_id;
                    }
                }else if($agentId !== $loginUserId){
                    $seniorAgent = $agentId;
                    if(!empty($invoiceParentId)){
                        $mostSeniorAgentID = $invoiceParentId;
                    }
                }


                $dataArray = array();
                if($invoiceTiers ==1 || $invoiceTiers ==2 || $invoiceTiers >=3 && is_numeric($profit)){
                    if($commissionSetting[3]['type']=='agent' && $commissionSetting[3]['tiers']=='>=1'){
                        $perAgent = $commissionSetting[3]['profit'];
                        $agentComm = ($perAgent /100) * $profit;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $inv_from;
                        $dataArray[$i]['invoice_id'] = $invoice_id;
                        $dataArray[$i]['invoice_no'] = $invoice_no;
                        $dataArray[$i]['client'] = $inv_to;
                        $dataArray[$i]['agent'] = $inv_from;
                        $dataArray[$i]['invoiceTotalAmount'] = $total_amount;
                        $dataArray[$i]['invoiceProfit'] = $profit;
                        $dataArray[$i]['cost'] = $cost;
                        $dataArray[$i]['commission'] = $agentComm;
                        $dataArray[$i]['commission_type'] = 1;
                        $dataArray[$i]['commission_from_id'] = $inv_from;
                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
                        //$dataArray[$i]['safeWatchID'] = $superAgentId;
                        $dataArray[$i]['agentType'] = 'Agent_Self';
                        $i = $i+1;
                    }
                }

//                if($invoiceTiers ==1 && is_numeric($total_amount)){
//                    if($commissionSetting[0]['type']=='safewatch' && $commissionSetting[0]['tiers']==1){
//                        $percentage = $commissionSetting[0]['profit'];
//                        $safeWatchComm = ($percentage /100) * $total_amount;
//                        $dataArray[$i] = array();
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['invoice_id'] = $invoice_id;
//                        $dataArray[$i]['invoice_no'] = $invoice_no;
//                        $dataArray[$i]['client'] = $inv_to;
//                        $dataArray[$i]['agent'] = $superAgentId;
//                        $dataArray[$i]['invoice_total'] = $total_amount;
//                        $dataArray[$i]['cost'] = $cost;
//                        $dataArray[$i]['commission'] = $safeWatchComm;
//                        $dataArray[$i]['commission_type'] = 1;
//                        $dataArray[$i]['commission_from_id'] = $inv_from;
//                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
//                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
//                        $dataArray[$i]['safeWatchID'] = $superAgentId;
//                        $dataArray[$i]['agentType'] = 'SafeWatch';
//                        $i = $i+1;
//                    }
//                }

                if($invoiceTiers ==2 && is_numeric($profit)){

//                    if($commissionSetting[1]['type']=='safewatch' && $commissionSetting[1]['tiers']==2){
//                        $percentage = $commissionSetting[1]['profit'];
//                        $safeWatchComm = ($percentage /100) * $profit;
//                        $dataArray[$i] = array();
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['invoice_id'] = $invoice_id;
//                        $dataArray[$i]['invoice_no'] = $invoice_no;
//                        $dataArray[$i]['client'] = $inv_to;
//                        $dataArray[$i]['agent'] = $superAgentId;
                          //$dataArray[$i]['invoiceTotalAmount'] = $total_amount;
                          //$dataArray[$i]['invoiceProfit'] = $profit;
//                        $dataArray[$i]['cost'] = $cost;
//                        $dataArray[$i]['commission'] = $safeWatchComm;
//                        $dataArray[$i]['commission_type'] = 1;
//                        $dataArray[$i]['commission_from_id'] = $inv_from;
//                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
//                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
//                        $dataArray[$i]['safeWatchID'] = $superAgentId;
//                        $dataArray[$i]['agentType'] = 'SafeWatch';
//                        $i = $i+1;
//                    }
                    if($commissionSetting[4]['type']=='seniorAgent' && $commissionSetting[4]['tiers']=='>=2' &&
                        !empty($seniorAgent)){
                        $perAgent = $commissionSetting[4]['profit'];
                        $agentComm = ($perAgent /100) * $profit;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $seniorAgent;
                        $dataArray[$i]['invoice_id'] = $invoice_id;
                        $dataArray[$i]['invoice_no'] = $invoice_no;
                        $dataArray[$i]['client'] = $inv_to;
                        $dataArray[$i]['agent'] = $seniorAgent;
                        $dataArray[$i]['invoiceTotalAmount'] = $total_amount;
                        $dataArray[$i]['invoiceProfit'] = $profit;
                        $dataArray[$i]['cost'] = $cost;
                        $dataArray[$i]['commission'] = $agentComm;
                        $dataArray[$i]['commission_type'] = 1;
                        $dataArray[$i]['commission_from_id'] = $inv_from;
                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
                        //$dataArray[$i]['safeWatchID'] = $superAgentId;
                        $dataArray[$i]['agentType'] = 'seniorAgent';
                        $i = $i+1;
                    }
                }

                if($invoiceTiers >=3 && is_numeric($profit)){

//                    if($commissionSetting[2]['type']=='safewatch' && $commissionSetting[2]['tiers']==3){
//                        $percentage = $commissionSetting[2]['profit'];
//                        $safeWatchComm = ($percentage /100) * $profit;
//                        $dataArray[$i] = array();
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['invoice_id'] = $invoice_id;
//                        $dataArray[$i]['invoice_no'] = $invoice_no;
//                        $dataArray[$i]['client'] = $inv_to;
//                        $dataArray[$i]['agent'] = $superAgentId;
//                        $dataArray[$i]['invoiceTotalAmount'] = $total_amount;
//                        $dataArray[$i]['invoiceProfit'] = $profit;
//                        $dataArray[$i]['cost'] = $cost;
//                        $dataArray[$i]['commission'] = $safeWatchComm;
//                        $dataArray[$i]['commission_type'] = 1;
//                        $dataArray[$i]['commission_from_id'] = $inv_from;
//                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
//                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
//                        $dataArray[$i]['safeWatchID'] = $superAgentId;
//                        $dataArray[$i]['agentType'] = 'SafeWatch';
//                        $i = $i+1;
//                    }
                    if($commissionSetting[4]['type']=='seniorAgent' && $commissionSetting[4]['tiers']=='>=2' &&
                        !empty($seniorAgent)){
                        $perAgent = $commissionSetting[4]['profit'];
                        $agentComm = ($perAgent /100) * $profit;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $seniorAgent;
                        $dataArray[$i]['invoice_id'] = $invoice_id;
                        $dataArray[$i]['invoice_no'] = $invoice_no;
                        $dataArray[$i]['client'] = $inv_to;
                        $dataArray[$i]['agent'] = $seniorAgent;
                        $dataArray[$i]['invoiceTotalAmount'] = $total_amount;
                        $dataArray[$i]['invoiceProfit'] = $profit;
                        $dataArray[$i]['cost'] = $cost;
                        $dataArray[$i]['commission'] = $agentComm;
                        $dataArray[$i]['commission_type'] = 1;
                        $dataArray[$i]['commission_from_id'] = $inv_from;
                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
                        //$dataArray[$i]['safeWatchID'] = $superAgentId;
                        $dataArray[$i]['agentType'] = 'seniorAgent';

                        $i =$i+1;
                    }
                    if($commissionSetting[5]['type']=='mostSeniorAgent' && $commissionSetting[5]['tiers']=='>=3' &&
                        !empty($mostSeniorAgentID)){
                        $perAgent = $commissionSetting[5]['profit'];
                        $agentComm = ($perAgent /100) * $profit;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $mostSeniorAgentID;
                        $dataArray[$i]['invoice_id'] = $invoice_id;
                        $dataArray[$i]['invoice_no'] = $invoice_no;
                        $dataArray[$i]['client'] = $inv_to;
                        $dataArray[$i]['agent'] = $mostSeniorAgentID;
                        $dataArray[$i]['invoiceTotalAmount'] = $total_amount;
                        $dataArray[$i]['invoiceProfit'] = $profit;
                        $dataArray[$i]['cost'] = $cost;
                        $dataArray[$i]['commission'] = $agentComm;
                        $dataArray[$i]['commission_type'] = 1;
                        $dataArray[$i]['commission_from_id'] = $inv_from;
                        $dataArray[$i]['seniorAgentID'] = $seniorAgent;
                        $dataArray[$i]['mostSeniorAgentID'] = $mostSeniorAgentID;
                        //$dataArray[$i]['safeWatchID'] = $superAgentId;
                        $dataArray[$i]['agentType'] = 'mostSeniorAgent';
                        $i = $i+1;
                    }
                }
                foreach ($dataArray as $key=>$val){
                    $commissions = Commission::updateOrCreate(
                        ['invoice_id' => $val['invoice_id'], 'agentType' => $val['agentType'], 'client' => $val['client'], 'agent' => $val['agent'], 'commission_from_id' => $val['commission_from_id']],
                        [
                            'user_id'           => $val['user_id'],
                            'invoice_id'        => $val['invoice_id'],
                            'invoice_no'        => $val['invoice_no'],
                            'client'            => $val['client'],
                            'agent'             => $val['agent'],
                            'seniorAgentID'     => $val['seniorAgentID'],
                            'mostSeniorAgentID' => $val['mostSeniorAgentID'],
                            //'safeWatchID'       => $val['safeWatchID'],
                            'agentType'         => $val['agentType'],
                            'invoiceTotalAmount'=> $val['invoiceTotalAmount'],
                            'invoiceProfit'     => $val['invoiceProfit'],
                            'cost'              => $val['cost'],
                            'commission'        => $val['commission'],
                            'commission_type'   => $val['commission_type'],
                            'commission_from_id'=> $val['commission_from_id'],
                        ]
                    );


                }

            }
        }

    }

    public static function commissionCalculateAgentFun($id)
    {
        if(!empty($id)){
            $commissions = Commission::where('id', $id)->first();
//            $superAgent = User::select('id')->where('id', 1)->where('role', 2)->where('status', 1)->where('isdeleted', 0)->first();
//            $superAgentId = $superAgent->id;
            $commissionSetting = Commission_setting::select('id','type','tiers','profit')->where('status', 1)->get();
            $i =0;
            if(!empty($commissions)) {
                $commissionsId      = $commissions->id;
                $agentId            = $commissions->agent;
                $total_amount       = $commissions->commission_total;
                $agentTiers         = NULL;
                $seniorAgent        = NULL;
                $mostSeniorAgentID  = NULL;
                $dataArray          = array();

                $todayDate = date('Y-m-d');
                Commission_history::where('commissions_id', $commissionsId)
                    ->update([
                        'date_paid' => $todayDate
                    ]);

                $agentData          = User::select('id', 'parent_id', 'tiers')->where('id', $agentId)->first();
                $user_id            = $agentData->id;
                $agentTiers         = $agentData->tiers;
                $seniorAgent        = $agentData->parent_id;

                if(!empty($seniorAgent)){
                    $mostSeniorAgentID = User::select('parent_id')->where('id',$seniorAgent)->first();
                    $mostSeniorAgentID = $mostSeniorAgentID->parent_id;
                }

                if($agentTiers ==1 || $agentTiers ==2 || $agentTiers >=3 && is_numeric($total_amount)){
                    if($commissionSetting[3]['type']=='agent' && $commissionSetting[3]['tiers']=='>=1'){
                        $perAgent = $commissionSetting[3]['profit'];
                        $agentComm = ($perAgent /100) * $total_amount;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $user_id;
                        $dataArray[$i]['incomeFromID'] = $agentId;
                        $dataArray[$i]['amount'] = $agentComm;
                        $i = $i+1;
                    }
                }

//                if($agentTiers ==1 && is_numeric($total_amount)){
//                    if($commissionSetting[0]['type']=='safewatch' && $commissionSetting[0]['tiers']==1){
//                        $percentage = $commissionSetting[0]['profit'];
//                        $safeWatchComm = ($percentage /100) * $total_amount;
//                        $dataArray[$i] = array();
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['incomeFromID'] = $agentId;
//                        $dataArray[$i]['amount'] = $safeWatchComm;
//                        $i = $i+1;
//                    }
//                }

                if($agentTiers ==2 && is_numeric($total_amount)){

//                    if($commissionSetting[1]['type']=='safewatch' && $commissionSetting[1]['tiers']==2){
//                        $percentage = $commissionSetting[1]['profit'];
//                        $safeWatchComm = ($percentage /100) * $total_amount;
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['incomeFromID'] = $agentId;
//                        $dataArray[$i]['amount'] = $safeWatchComm;
//                        $i = $i+1;
//                    }
                    if($commissionSetting[4]['type']=='seniorAgent' && $commissionSetting[4]['tiers']=='>=2' &&
                        !empty($seniorAgent)){
                        $perAgent = $commissionSetting[4]['profit'];
                        $agentComm = ($perAgent /100) * $total_amount;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $seniorAgent;
                        $dataArray[$i]['incomeFromID'] = $agentId;
                        $dataArray[$i]['amount'] = $agentComm;
                        $i = $i+1;
                    }
                }

                if($agentTiers >=3 && is_numeric($total_amount)){

//                    if($commissionSetting[2]['type']=='safewatch' && $commissionSetting[2]['tiers']==3){
//                        $percentage = $commissionSetting[2]['profit'];
//                        $safeWatchComm = ($percentage /100) * $total_amount;
//                        $dataArray[$i] = array();
//                        $dataArray[$i]['user_id'] = $superAgentId;
//                        $dataArray[$i]['incomeFromID'] = $agentId;
//                        $dataArray[$i]['amount'] = $safeWatchComm;
//                        $i = $i+1;
//                    }
                    if($commissionSetting[4]['type']=='seniorAgent' && $commissionSetting[4]['tiers']=='>=2' &&
                        !empty($seniorAgent)){
                        $perAgent = $commissionSetting[4]['profit'];
                        $agentComm = ($perAgent /100) * $total_amount;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $seniorAgent;
                        $dataArray[$i]['incomeFromID'] = $agentId;
                        $dataArray[$i]['amount'] = $agentComm;
                        $i =$i+1;
                    }
                    if($commissionSetting[5]['type']=='mostSeniorAgent' && $commissionSetting[5]['tiers']=='>=3' &&
                        !empty($mostSeniorAgentID)){
                        $perAgent = $commissionSetting[5]['profit'];
                        $agentComm = ($perAgent /100) * $total_amount;
                        $dataArray[$i] = array();
                        $dataArray[$i]['user_id'] = $mostSeniorAgentID;
                        $dataArray[$i]['incomeFromID'] = $agentId;
                        $dataArray[$i]['amount'] = $agentComm;
                        $i = $i+1;
                    }
                }

                foreach ($dataArray as $val){
                    $income = new Income;
                    $income->userId = $val['user_id'];
                    $income->incomeFromID =  $val['incomeFromID'];
                    $income->amount = $val['amount'];
                    $income->status = 1;
                    $income->save();
                }

            }
        }

    }

}

