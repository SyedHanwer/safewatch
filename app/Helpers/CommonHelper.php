<?php

namespace App\Helpers;

use App\Generate_no;
use Auth;
use DB;

class CommonHelper
{

    public function myagentClient($agentid)
    {
        $data = DB::table('users')
            ->select('id','forename','surname')
            ->where('role', 3)
            ->where('parent_id', $agentid)
            ->orderBy('created_at', 'DESC')
            ->get();
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }
    public function autoRandomNo($val)
    {
        $no = new Generate_no;
        $no->user_id = Auth::user()->id;
        $no->name = $val;
        $no->save();
        $id = $no->id;
        if (!empty($id)) {
            return $id;
        } else {
            return false;
        }
    }


    public static function instance()
    {
        return new CommonHelper();
    }
}

