<?php

namespace App\Helpers;
use App\User;

use Auth;
use DB;

class EmailHelper
{

    public static function sendEmailClientReg($email='', $forename='', $randomString='')
    {
        $to = $email;
        $toForename = $forename;
        $randomString = $randomString;

        $subject = 'SafeWatch New Client';
        $from = 'mail@safewatch.co.uk';
        $fromName = 'SafeWatch';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n";
        $data = [
            'toForename' => $toForename,
            'randomString' => $randomString,
        ];
        $htmlContent = view('mails.welcomeTemp', compact('data'))->render();
        mail($to, $subject, $htmlContent, $headers);
    }

    public static function sendEmailAgentReg($email='', $forename='', $surname='', $randomString='')
    {
        $to = $email;
        $toForename = $forename;
        $toSurname = $surname;
        $fromForename = Auth::user()->forename;
        $fromSurname = Auth::user()->surname;
        $to2 = Auth::user()->email;
        $randomString = $randomString;

        $superAgent = User::select('id')->where('id', 1)->first();
        $superForename = $superAgent->forename;
        $to3 = $superAgent->email;

        $subject = 'SafeWatch New Agent';
        $from = 'mail@safewatch.co.uk';
        $fromName = 'SafeWatch';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: '.$fromName.'<'.$from.'>' . "\r\n";

        $data = [
            'toForename' => $toForename,
            'toSurname' => $toSurname,
            'randomString' => $randomString,
            'fromForename' => $fromForename,
            'fromSurname' => $fromSurname,
            'superForename' => $superForename,
        ];

        $htmlContent = view('mails.welcome2Temp', compact('data'))->render();
        $htmlContent2 = view('mails.newAgent2Temp', compact('data'))->render();
        $htmlContent3 = view('mails.agentRegTemp', compact('data'))->render();

        mail($to, $subject, $htmlContent, $headers);
        mail($to2, $subject, $htmlContent2, $headers);
        mail($to3, $subject, $htmlContent3, $headers);

    }



}

