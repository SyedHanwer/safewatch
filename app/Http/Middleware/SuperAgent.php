<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SuperAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* define a login page role */
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        /* define a Super agent role */
        if(Auth::user()->id == 1){
            return $next($request);
        }

        /* define a Agent user role */
        if (Auth::user()->role == 2) {
            return $next($request);
        }

        /* define a Client user role */
        if (Auth::user()->role == 3) {
            return $next($request);
        }
        return redirect('login')->with('error','You have not admin access');
    }
}
