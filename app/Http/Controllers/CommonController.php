<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Generate_no;
use Auth;

class CommonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function autoRandomNo(Request $request)
    {
        $no = new Generate_no;
        $no->user_id = Auth::user()->id;
        $no->name = $request->val;
        $no->save();
        $id = $no->id;
        if (!empty($id)) {
            return $id;
        } else {
            return false;
        }
    }


}
