<?php



namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Message;
use App\Job;
use App\Invoice;
use DB;
use Auth;
use Validator;

class ClientAjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clientAjaxView(Request $request)
    {
        $view = $request->view;

        if($view ==='invoices'){
            $invoices = DB::table('invoices')
                ->select('invoices.id','invoices.invoice_no','invoices.details', 'invoices.created_at','invoices.status')
                ->where('invoices.isdeleted','<>', 2)
                ->where('invoices.inv_to', Auth::user()->id)
                ->orderBy('invoices.created_at', 'DESC')
                ->get();

            $oneYearOn = date('Y-m-d', strtotime('+1 year'));
            $annual_reports = Invoice::where('isdeleted', 0)
                ->where('inv_to', Auth::user()->id)
                ->where('created_at','>=', $oneYearOn)
                ->orderBy('created_at','DESC')
                ->get();

            $data = [
                'invoices' => $invoices,
                'annual_reports' => $annual_reports,
            ];
            $view = view('clientAjaxView.invoices', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='mysafewatch'){
            $products = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->Join('products', 'lhcs.id', '=', 'products.provider_id')
                ->select('lhcs.id as lid','lhcs.newprovider', 'services.id as sid','services.name','products.*')
                ->where('products.pro_to', Auth::user()->id)
                ->where('products.status', 1)
                ->where('products.isdeleted', 0)
                ->orderBy('products.created_at', 'DESC')
                ->get();
            $data = [
                'products' => $products,
            ];
            $view = view('clientAjaxView.mysafewatch', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='contact'){
            $data = array();
            $view = view('clientAjaxView.contact', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

    }

    public function sendcontactmsg(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|string|max:200',
            'message' => 'required|string',
        ]);

        $agentid = Auth::user()->parent_id;
        if(empty($agentid)){
            return response()->json(['status'=>'error']);
            exit();
        }
        if ($validator->passes()) {
            $messages = new Message([
                'user_id' => Auth::user()->id,
                'msg_from' => Auth::user()->id,
                'msg_to' => $agentid,
                'msg_type' => 'inbox',
                'name' => Auth::user()->forename.' '.Auth::user()->surname,
                'subject' => $request->post('subject'),
                'message' => $request->post('message'),
                'status' => 1,
            ]);
            $messages->save();
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }
    public function msgSendToSW(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Subject' => 'required|string|max:200',
            'Message' => 'required|string',
        ]);

        if ($validator->passes()) {
            $messages = new Message([
                'user_id' => Auth::user()->id,
                'msg_from' => Auth::user()->id,
                'msg_to' => 3,
                'msg_type' => 'inbox',
                'name' => Auth::user()->forename.' '.Auth::user()->surname,
                'subject' => $request->post('Subject'),
                'message' => $request->post('Message'),
                'status' => 1,
            ]);
            $messages->save();
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function invoicePreView(Request $request)
    {
        $data = array();
        $invoiceID = $request->invoiceID;
        $invoices = Invoice::where('id', $invoiceID)->first();
        $data['invoices'] = $invoices;
        if($invoices->invoice_type =='Job'){
            $data['data'] = Job::where('invoice_id', $invoices->id)->get();
        }
        if($invoices->invoice_type =='LHC'){
            $data['data'] = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->select('lhcs.*', 'services.id as sid','services.name as sname')
                ->where('lhcs.invoice_id', $invoices->id)
                ->where('lhcs.status', '<>', 0)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }
        return response()->json(['status'=>'success', 'data'=>$data]);
        exit();
    }

}

