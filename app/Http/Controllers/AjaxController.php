<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Invoice;
use App\Message;
use App\User;
use App\Job;
use App\Lhc;
use App\Service;
use App\Product;
use App\Supplier;
use App\Commission;
use App\Commission_history;
use CommissionCalculationHelper;
use EmailHelper;
use DB;
use Auth;
use Validator;


class AjaxController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxView(Request $request)
    {
        $view = $request->view;
        if($view ==='planner'){
            $reminderDate = date('Y-m-d', strtotime('+ 30 day'));
            $data = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->select('lhcs.*', 'services.id as sid','services.name as sname')
                ->whereNotNull('lhcs.planner')
                ->whereBetween('lhcs.planner', [now(), $reminderDate])
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $view = view('ajaxview.plannerajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='client'){
            $myclient = User::select('id','forename','surname')
                ->where('role', 3)
                ->where('parent_id', Auth::user()->id)
                ->orderBy('created_at', 'DESC')
                ->get();
            $myagent = User::select('id')
                ->where('role', 2)
                ->where('parent_id', Auth::user()->id)
                ->orderBy('created_at', 'DESC')
                ->get();
            $data = [
                'myclient' => $myclient,
                'myagent' => $myagent,
            ];
            $view = view('ajaxview.clientajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='manage'){
            $myagent = User::select('id','forename','surname')
                ->where('parent_id', Auth::user()->id)
                ->where('role', 2)
                ->where('status', 1)
                ->where('isdeleted', 0)
                ->orderBy('created_at', 'DESC')
                ->get();
            $commission = DB::table('commissions')
                ->join('users', 'commissions.commission_from_id', '=', 'users.id')
                ->select('commissions.id','commissions.invoice_id','commissions.invoice_no','commissions.invoiceTotalAmount', 'commissions.commission','commissions.created_at','users.role','users.forename', 'users.surname')
                ->where('commissions.commission_type', 1)
                ->where('commissions.status', 1)
                ->where('commissions.isdeleted', 0)
                ->where('commissions.agent', Auth::user()->id)
                ->orderBy('commissions.created_at', 'DESC')
                ->get();

            $commission_history = Commission_history::where('status', 1)
                ->where('user_id', Auth::user()->id)
                ->latest('id', 'desc')
                ->first();
            $commissionSum = Commission_history::where('status', 1)
                ->where('user_id', Auth::user()->id)
                ->sum('commission');

            $supplier = Supplier::where('status', 1)->get();
            $data = [
                'myagent' => $myagent,
                'commission' => $commission,
                'commission_history' => $commission_history,
                'commissionSum' => $commissionSum,
                'supplier' => $supplier,
            ];
            $view = view('ajaxview.manageajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();

        }

    }

    public function ajaxViewClient(Request $request)
    {
        $view = $request->view;
        $id = $request->id;
        if($view ==='lhc'){
            $services = Service::select('id', 'name')->where('status', 1)->get();
            $lhcs = Lhc::where('user_id', $id)
                ->where('status', '<>', 1)
                ->where('isdeleted', 0)
                ->orderBy('created_at', 'DESC')
                ->get();
            $data = [
                'services' => $services,
                'lhcs' => $lhcs,
                'user_id' => $id,
            ];
            $view = view('ajaxview.lhcajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='jobs'){
            $jobs = Job::where('user_id', $id)->where('status', '<>', 1)->where('isdeleted', 0)
                ->orderBy('created_at', 'DESC')->get();
            $data = [
                'jobs' => $jobs,
                'user_id' => $id,
            ];
            $view = view('ajaxview.jobsajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='invoices'){
            $invoices = Invoice::where('user_id', $id)->where('isdeleted', 0)->orderBy('created_at', 'DESC')->get()->unique('invoice_no');
            $data = [
                'invoices' => $invoices,
                'user_id' => $id,
            ];
            $view = view('ajaxview.invoicesajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='annual_reports'){
            if($id){
                $oneYearOn = date('Y-m-d', strtotime('+1 year'));
                $annual_reports = Invoice::where('isdeleted', 0)
                    ->where('user_id', $id)
                    ->where('created_at','>=', $oneYearOn)
                    ->orderBy('created_at','DESC')
                    ->get();
                $data = [
                    'annual_reports' => $annual_reports,
                    'user_id' => $id,
                ];
            }else{
                $data = [
                    'annual_reports' => '',
                    'user_id' => '',
                ];
            }
            $view = view('ajaxview.reportajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='products'){
            $products = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->leftJoin('products', 'lhcs.id', '=', 'products.provider_id')
                ->select('lhcs.id as lid','lhcs.newprovider', 'services.id as sid','services.name','products.*')
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->where('lhcs.user_id', $id)
                ->where('lhcs.isdeleted', 0)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $data = [
                'products' => $products,
                'user_id' => $id,
            ];
            $view = view('ajaxview.productsajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }

        if($view ==='agentcontact'){
            $data = [
                'user_id' => $id,
            ];
            $view = view('ajaxview.agentcontactajax', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }


    }

    public function viewclient(Request $request)
    {
        $id = $request->id;
        $data = User::where('id',$id)->first();
        return response()->json(['status'=>'success', 'data'=>$data]);
        exit();
    }

    public function jobdelete()
    {
        $id = $_POST['id'];
        Job::where('id', $id)
            ->update([
                'isdeleted' => 1
            ]);
        return 1;
    }

    public function lhcdelete()
    {
        $id = $_POST['id'];
        Lhc::where('id', $id)
            ->update([
                'isdeleted' => 1
            ]);
        return 1;
    }

    public function commrequest(Request $request)
    {
        $tComm = $request->tcomm;
        $commissions = Commission::where('commission_type', 1)
            ->where('status', 1)
            ->where('commissions.isdeleted', 0)
            ->where('agent', Auth::user()->id)
            ->update([
                'isdeleted' => 1
            ]);
        if(!empty($commissions)){
            $invoices = Invoice::where('user_id', Auth::user()->id)
                ->where('status', 1)
                ->where('isdeleted', 0)
                ->update([
                    'isdeleted' => 1
                ]);
            $ComRun = new Commission([
                'user_id' => Auth::user()->id,
                'agent' => Auth::user()->id,
                'commission_type' => 2,
                'commission_from_id' => Auth::user()->id,
                'commission_total' => $tComm,
                'status' => 3,
            ]);
            $ComRun->save();

            $ComHis = new Commission_history([
                'user_id' => Auth::user()->id,
                'commissions_id' => $ComRun->id,
                'commission' => $tComm,
                'status' =>1,
            ]);
            $ComHis->save();
            //$commission_history = Commission_history::where('user_id', $ComHis['id'])->first();
            $commissionSum = Commission_history::where('status', 1)
                ->where('user_id', Auth::user()->id)
                ->sum('commission');
            return response()->json(['status'=>'success','commissionSum'=>$commissionSum]);
            exit;
        }else{
            return response()->json(['status'=>'error']);
            exit;
        }
    }

    public function clientupdate(Request $request)
    {
            $data = $_POST['data'];
            if(!empty($data) && $data['status'] == 'update'){

                $validator = Validator::make($request['data'], [
                    'forename' => 'required|string|max:100',
                    'surname' => 'required|string|max:100',
                    //'client_id' => 'required|min:4|integer',
                    //'dob' => 'sometimes|required|date_format:"m/d/Y"',
                    'address1' => 'required|string|max:100',
                    'email' => 'unique:users,email,'.$data['id'],
                    'mobile_no' => 'unique:users,mobile_no,'.$data['id'],
                    'membershiptype'=> 'required|string|in:Free,Paid'
                ]);
                if ($validator->passes()) {
                    User::where('id', $data['id'])
                        ->update([
                            'forename' => $data['forename'],
                            'surname' => $data['surname'],
                            'dob' => $data['dob'],
                            'address1' => $data['address1'],
                            'home_no' => $data['home_no'],
                            'dateoffirstcontact' => $data['dateoffirstcontact'],
                            'address2' => $data['address2'],
                            'mobile_no' => $data['mobile_no'],
                            'dateofmembersince' => $data['dateofmembersince'],
                            'address3' => $data['address3'],
                            'email' => $data['email'],
                            'preferred_contact' => $data['preferred_contact'],
                            'postcode' => $data['postcode'],
                            'referredby' => $data['referredby'],
                            'membershiptype' => $data['membershiptype'],
                            'notes' => $data['notes'],
                        ]);
                    return response()->json(['status'=>'update']);
                }
                return response()->json(['error'=>$validator->errors()->all()]);

            }else if(!empty($data) && $data['status'] == 'save'){

                $validator = Validator::make($request['data'], [
                    'forename' => 'required|string|max:100',
                    'surname' => 'required|string|max:100',
                    //'client_id' => 'required|min:4|integer',
                    //'dob' => 'sometimes|required|date_format:"m/d/Y"',
                    'address1' => 'required|string|max:100',
                    'email' => 'required|string|email|max:255|unique:users',
                    'mobile_no' => 'required|unique:users',
                    'membershiptype'=> 'required|string|in:Free,Paid'
                ]);
                if ($validator->passes()) {
                    $randomString = md5(rand(1, 10) . microtime());
                    $user = new User([
                        'forename' => $data['forename'],
                        'surname' => $data['surname'],
                        'client_id' => rand(1000,9999),
                        'dob' => $data['dob'],
                        'address1' => $data['address1'],
                        'home_no' => $data['home_no'],
                        'dateoffirstcontact' => $data['dateoffirstcontact'],
                        'address2' => $data['address2'],
                        'mobile_no' => $data['mobile_no'],
                        'dateofmembersince' => $data['dateofmembersince'],
                        'address3' => $data['address3'],
                        'email' => $data['email'],
                        'preferred_contact' => $data['preferred_contact'],
                        'postcode' => $data['postcode'],
                        'referredby' => $data['referredby'],
                        'membershiptype' => $data['membershiptype'],
                        'notes' => $data['notes'],
                        'status' => 1,
                        'role' => 3,
                        'parent_id' => Auth::user()->id,
                        'tiers' => Auth::user()->tiers +1,
                        'passwordToken' => $randomString,
                    ]);
                    $user->save();
                    EmailHelper::sendEmailClientReg($data['email'], $data['forename'], $randomString);
                    return response()->json(['status'=>'save']);
                }
                return response()->json(['error'=>$validator->errors()->all()]);
            }else {
                return response()->json(['status'=>'error']);
            }
    }


    public function lhcsave(Request $request)
    {
        $data = $request['data'];
        $rules = [];
        //$curEndDatePlanner = '';
        for ($i=0; $i < count($data['services_type']); $i++){
            $rules['current_end_date.' . $i] = 'nullable|date_format:d/m/Y';
            $rules['new_end_date.' . $i] = 'nullable|date_format:d/m/Y';
            //if (!empty($data['exclude_hidden'][$i]) && $data['exclude_hidden'][$i] == 'Exclude') {
//                $rules['services_type.' . $i] = 'required|string|max:100';
//                $rules['current_provider.' . $i] = 'required|string|max:100';
                //$rules['current_end_date.' . $i] = 'nullable|date_format:d/m/Y';
//                $rs = explode('/', $data['current_end_date'][$i]);
//                if(!empty($rs[0]) && !empty($rs[1]) && !empty($rs[2])){
//                    $curEndDatePlanner = $rs[2].'-'.$rs[1].'-'.$rs[0];
//                }else {
//                    $curEndDatePlanner = date('Y-m-d', strtotime($data['current_end_date'][$i]));
//                }
            //}
//            else{
//                $rules['services_type.' . $i] = 'required|string|max:100';
//                $rules['current_provider.' . $i] = 'required|string|max:100';
//                $rules['current_monthly_cost.' . $i] = 'required|numeric';
//                $rules['current_total_over_term.' . $i] = 'required|numeric';
//            }

            //if (!empty($data['services_type'][$i]) && $data['services_type'][$i] ==1 || $data['services_type'][$i]
               // ==2) {
                //$rules['new_provider.' . $i] = 'required|string|max:100';
                //$rules['new_end_date.' . $i] = 'nullable|date_format:d/m/Y';
//                $rules['term.' . $i] = 'required|integer';
//                $rules['new_monthly_cost.' . $i] = 'required|numeric';
//                $rules['new_total_over_term.' . $i] = 'required|numeric';
//                $rules['new_cancel_fee.' . $i] = 'required|numeric';
            //}
        }
        $validator = Validator::make($request['data'], $rules);
        if($validator->passes()){
            for ($i=0; $i < count($data['services_type']); $i++){
                $curenddate = NULL;
                $newenddate = NULL;
                $rs = explode('/', $data['current_end_date'][$i]);
                if(!empty($rs[0]) && !empty($rs[1]) && !empty($rs[2])){
                    $curenddate = $rs[2].'-'.$rs[1].'-'.$rs[0];
                }else if(!empty($data['current_end_date'][$i])){
                    $curenddate = date('Y-m-d', strtotime($data['current_end_date'][$i]));
                }
                $rs1 = explode('/', $data['new_end_date'][$i]);
                if(!empty($rs1[0]) && !empty($rs1[1]) && !empty($rs1[2])){
                    $newenddate = $rs1[2].'-'.$rs1[1].'-'.$rs1[0];
                }else if(!empty($data['new_end_date'][$i])){
                    $newenddate = date('Y-m-d', strtotime($data['new_end_date'][$i]));
                }
                if($data['saveid'][$i] =='lhc111'){
                    $lhc = new Lhc([
                        'service_id' => $data['services_type'][$i],
                        'user_id' => $data['userid'],
                        'lhc_from' => Auth::user()->id,
                        'lhc_to' => $data['userid'],
                        'curprovider' => $data['current_provider'][$i],
                        'newprovider' => $data['new_provider'][$i],
                        'curdetails' => $data['curdetails'][$i],
                        'newdetails' => $data['newdetails'][$i],
                        'curenddate' =>  $curenddate,
                        'newenddate' => $newenddate,
                        'usages' => $data['usages'][$i],
                        'term' => $data['term'][$i],
                        'curmonthlycost' => $data['current_monthly_cost'][$i]?$data['current_monthly_cost'][$i]:0.00,
                        'newmonthlycost' => $data['new_monthly_cost'][$i]?$data['new_monthly_cost'][$i]:0.00,
                        'benefits' => $data['newsaving'][$i],
                        'curtotalover' => $data['current_total_over_term'][$i]?$data['current_total_over_term'][$i]:0.00,
                        'newtotalover' => $data['new_total_over_term'][$i]?$data['new_total_over_term'][$i]:0.00,
                        'curcancelfee' => $data['current_cancel_fee'][$i]?$data['current_cancel_fee'][$i]:0.00,
                        'newcancelfee' => $data['new_cancel_fee'][$i]?$data['new_cancel_fee'][$i]:0.00,
                        'cursaving' => $data['cursaving'][$i]?$data['cursaving'][$i]:0.00,
                        'newsaving' => $data['newsaving'][$i]?$data['newsaving'][$i]:0.00,
                        'newcommissiontype' => $data['newcommissiontype'][$i]?$data['newcommissiontype'][$i]:NULL,
                        'amount' => $data['amount'][$i]?$data['amount'][$i]:0.00,
                        'exclude' => !empty($data['exclude_hidden'][$i])? $data['exclude_hidden'][$i]:NULL,
                        'planner' => $curenddate,
                    ]);
                    $lhc->save();
                }else if(!empty($data['saveid'][$i]) && $data['saveid'][$i] !='lhc111'){
                    Lhc::where('id', $data['saveid'][$i])
                        ->update([
                            'service_id' => $data['services_type'][$i],
                            'user_id' => $data['userid'],
                            'curprovider' => $data['current_provider'][$i],
                            'newprovider' => $data['new_provider'][$i],
                            'curdetails' => $data['curdetails'][$i],
                            'newdetails' => $data['newdetails'][$i],
                            'curenddate' =>  $curenddate,
                            'newenddate' => $newenddate,
                            'usages' => $data['usages'][$i],
                            'term' => $data['term'][$i],
                            'curmonthlycost' => $data['current_monthly_cost'][$i]?$data['current_monthly_cost'][$i]:0.00,
                            'newmonthlycost' => $data['new_monthly_cost'][$i]?$data['new_monthly_cost'][$i]:0.00,
                            'benefits' => $data['newsaving'][$i],
                            'curtotalover' => $data['current_total_over_term'][$i]?$data['current_total_over_term'][$i]:0.00,
                            'newtotalover' => $data['new_total_over_term'][$i]?$data['new_total_over_term'][$i]:0.00,
                            'curcancelfee' => $data['current_cancel_fee'][$i]?$data['current_cancel_fee'][$i]:0.00,
                            'newcancelfee' => $data['new_cancel_fee'][$i]?$data['new_cancel_fee'][$i]:0.00,
                            'cursaving' => $data['cursaving'][$i]?$data['cursaving'][$i]:0.00,
                            'newsaving' => $data['newsaving'][$i]?$data['newsaving'][$i]:0.00,
                            'newcommissiontype' => $data['newcommissiontype'][$i]?$data['newcommissiontype'][$i]:NULL,
                            'amount' => $data['amount'][$i]?$data['amount'][$i]:0.00,
                            'exclude' => !empty($data['exclude_hidden'][$i])? $data['exclude_hidden'][$i]:NULL,
                            'planner' => $curenddate,
                        ]);
                }
            }
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function jobsave(Request $request)
    {
        $data = $request['data'];
        $validator = Validator::make($request['data'], [
            'details.*' => 'required|string|max:100',
            'cost.*' => 'required|numeric',
            'saving.*' => 'required|numeric',
        ]);
        if($validator->passes()){
            for ($i=0; $i < count($data['jobno']); $i++){
                if($data['status'] == 'save' && $data['jobsid'][$i] =='job222'){
                    $job = new Job([
                        'user_id' => $data['userid'],
                        'job_from' => Auth::user()->id,
                        'job_to' => $data['userid'],
                        'jobno' => $data['jobno'][$i],
                        'details' => $data['details'][$i],
                        'units' => $data['units'][$i],
                        'standard' => $data['standard'][$i],
                        'member' => $data['member'][$i],
                        'cost' => $data['cost'][$i]?$data['cost'][$i]:0.00,
                        'saving' => $data['saving'][$i]?$data['saving'][$i]:0.00,
                        'comission_type' => $data['comission_type'][$i]?$data['comission_type'][$i]:NULL,
                        'amount' => $data['amount'][$i]?$data['amount'][$i]:0.00,
                        'amount2' => $data['amount2'][$i]?$data['amount2'][$i]:0.00,
                    ]);
                    $job->save();
                }else if(!empty($data['jobsid'][$i]) && $data['jobsid'][$i] !='job222'){
                    Job::where('id', $data['jobsid'])
                        ->update([
                            'user_id' => $data['userid'],
                            'details' => $data['details'][$i],
                            'units' => $data['units'][$i],
                            'standard' => $data['standard'][$i],
                            'member' => $data['member'][$i],
                            'cost' => $data['cost'][$i]?$data['cost'][$i]:0.00,
                            'saving' => $data['saving'][$i]?$data['saving'][$i]:0.00,
                            'comission_type' => $data['comission_type'][$i]?$data['comission_type'][$i]:NULL,
                            'amount' => $data['amount'][$i]?$data['amount'][$i]:0.00,
                            'amount2' => $data['amount2'][$i]?$data['amount2'][$i]:0.00,
                        ]);
                }
            }
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);

    }


    public function updateproduct(Request $request)
    {
        $data = $request['data'];
        $validator = Validator::make($request['data'], [
            'username.*' => 'required|string|max:100',
            'password.*' => 'required|string|max:100',
        ]);
        if($validator->passes()){
            for ($i=0; $i<count($data['providerID']); $i++){
                $products = Product::updateOrCreate(
                    ['id' => $data['productID'][$i] ],
                    [
                        'provider_id' => $data['providerID'][$i],
                        'services' => $data['services'][$i],
                        'provider_name' => $data['providers'][$i],
                        'username' => $data['username'][$i],
                        'pro_password' => $data['password'][$i],
                        'pro_notes' => $data['notes'][$i],
                        'pro_from' => Auth::user()->id,
                        'pro_to' => $data['productTo'],
                        'status' => 1
                    ]
                );
            }
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function agentview(Request $request)
    {
        $data = User::where('id',$request->id)->first();
        return response()->json(['status'=>'success', 'data'=>$data]);
        exit();
    }

    public function agentDelete(Request $request)
    {
        User::where('id',$request->id)
            ->update([
                'isdeleted' => 1
            ]);
        return 1;
    }

    public function sendmsgtoclient(Request $request)
   {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|string|max:200',
            'message' => 'required|string|max:100',
        ]);

        $clientid = $request->post('userid');
        if(empty($clientid)){
            return response()->json(['status'=>'error']);
            exit();
        }
        if ($validator->passes()) {
            $messages = new Message([
                'user_id' => Auth::user()->id,
                'msg_from' => Auth::user()->id,
                'msg_to' => $clientid,
                'msg_type' => 'inbox',
                'name' => Auth::user()->forename.' '.Auth::user()->surname,
                'subject' => $request->post('subject'),
                'message' => $request->post('message'),
                'status' => 1,
            ]);
            $messages->save();
            return response()->json(['status'=>'save']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
   }

    public function jobPrepareInvoice(Request $request)
    {
        $box = $request->all();
        $myValue = array();
        parse_str($box['formdata'], $myValue);

        $user_id = $myValue['txtuserId'];
        $txttotalamount = $myValue['txttotalamount']!='NaN'?$myValue['txttotalamount']:0;
        $txtTotalProfit = $myValue['txtTotalProfit']!='NaN'?$myValue['txtTotalProfit']:0;
        $txttotalSaving = $myValue['txttotalSaving']!='NaN'?$myValue['txttotalSaving']:0;
        $txttotalCost = $myValue['txttotalCost']!='NaN'?$myValue['txttotalCost']:0;
        $invoiceTotal = $myValue['invoiceTotal']!='NaN'?$myValue['invoiceTotal']:0;

        $Invoice = Invoice::updateOrCreate(
            ['invoice_no' => $invoiceTotal, 'user_id' => $user_id, 'inv_from' => Auth::user()->id ],
            [
                'user_id' => $user_id,
                'inv_from' => Auth::user()->id,
                'inv_to' => $user_id,
                'invoice_type' => 'Job',
                'invoice_no' => $invoiceTotal,
                'total_amount'=>$txttotalamount,
                'details' => 'Services',
                'inv_date' => date('d/m/y'),
                'saving' => $txttotalSaving,
                'cost' => $txttotalCost,
                'profit' => $txtTotalProfit,
            ]
        );

        if(!empty($Invoice->id)){
            $id = $Invoice->id;
            CommissionCalculationHelper::commissionCalculateFun($id);

            for ($i=0; $i < count($myValue['jobsid']); $i++){
                $jobsid = null;
                if($myValue['jobsid'][$i] !='job222'){
                    $jobsid = $myValue['jobsid'][$i];
                }
                $jobs = Job::updateOrCreate(
                    ['id' => $jobsid, 'user_id' => $user_id, 'job_from' => Auth::user()->id ],
                    [
                        'user_id' => $user_id,
                        'job_from' => Auth::user()->id,
                        'job_to' => $user_id,
                        'jobno' => $myValue['jobno'][$i],
                        'invoice_id' => $id,
                        'invoice_no' => $invoiceTotal,
                        'details' => $myValue['details'][$i],
                        'units' => $myValue['units'][$i],
                        'standard' => $myValue['standard'][$i],
                        'member' => $myValue['member'][$i],
                        'cost' => $myValue['cost'][$i]?$myValue['cost'][$i]:0.00,
                        'saving' => $myValue['saving'][$i]?$myValue['saving'][$i]:0.00,
                        'comission_type' => $myValue['comission_type'][$i]?$myValue['comission_type'][$i]:NULL,
                        'amount' => $myValue['amount'][$i]?$myValue['amount'][$i]:0.00,
                        'amount2' => $myValue['amount2'][$i]?$myValue['amount2'][$i]:0.00,
                        'status' => 1,
                    ]
                );
            }
            $this->messageInvoice($user_id, $invoiceTotal, 'Services');
        }
        return response()->json(['status'=>'save']);
    }

    public function lhcPrepareInvoice(Request $request)
    {
        $box = $request->all();
        $myValue =  array();
        parse_str($box['formdata'], $data);
	    $user_id = $data['txtuserId'];
        $invoice_no = $data['invoice_no']!='NaN'?$data['invoice_no']:0.00;
        $txttotalamount = $data['txttotalamount']!='NaN'?$data['txttotalamount']:0.00;
        $txttotalamountNonM = $data['txttotalamountNonM']!='NaN'?$data['txttotalamountNonM']:0.00;
        $totalSavingLhc = $data['totalSavingLhc']!='NaN'?$data['totalSavingLhc']:0.00;
        $tnewmonthlycost = $data['tnewmonthlycost']!='NaN'?$data['tnewmonthlycost']:0.00;
        $Invoice = Invoice::updateOrCreate(
            ['invoice_no' => $invoice_no, 'user_id' => $user_id, 'inv_from' => Auth::user()->id ],
            [
                'user_id' => $user_id,
                'inv_from' => Auth::user()->id,
                'inv_to' => $user_id,
                'invoice_type' => 'LHC',
                'invoice_no' => $invoice_no,
                'total_amount' => $txttotalamount,
                'details' => 'Lifestyle HealthCheck',
                'inv_date' => date('d/m/y'),
                'saving' => $totalSavingLhc,
                'cost' => $tnewmonthlycost,
                'profit' => $txttotalamount,
                'nonMember' => $txttotalamountNonM,
            ]
        );

        if(!empty($Invoice->id)){
            $id = $Invoice->id;
            CommissionCalculationHelper::commissionCalculateFun($id);

            for ($i=0; $i < count($data['saveid']); $i++){
                $saveid = null;
                if($data['saveid'][$i] !='lhc111'){
                    $saveid = $data['saveid'][$i];
                }

                $curEndDate = NULL;
                $newEndDate = NULL;
                $rs = explode('/', $data['curenddate'][$i]);
                if(!empty($rs[0]) && !empty($rs[1]) && !empty($rs[2])){
                    $curEndDate = $rs[2].'-'.$rs[1].'-'.$rs[0];
                }else if(!empty($data['curenddate'][$i])){
                    $curEndDate = date('Y-m-d', strtotime($data['curenddate'][$i]));
                }
                $rs1 = explode('/', $data['newenddate'][$i]);
                if(!empty($rs1[0]) && !empty($rs1[1]) && !empty($rs1[2])){
                    $newEndDate = $rs1[2].'-'.$rs1[1].'-'.$rs1[0];
                }else if(!empty($data['newenddate'][$i])){
                    $newEndDate = date('Y-m-d', strtotime($data['newenddate'][$i]));
                }

                $status = 1;
                if (!empty($data['exclude_hidden'][$i]) && $data['exclude_hidden'][$i] == 'Exclude') {
                    $status = 0;
                }

                $lhc = Lhc::updateOrCreate(
                    ['id' => $saveid, 'user_id' => $user_id, 'lhc_from' => Auth::user()->id ],
                    [
                        'service_id' => $data['servicetype'][$i],
                        'user_id' => $user_id,
                        'lhc_from' => Auth::user()->id,
                        'lhc_to' => $user_id,
                        'invoice_id' => $status ==0?NULL:$id,
                        'invoice_no' => $status ==0?NULL:$invoice_no,
                        'curprovider' => $data['curprovider'][$i],
                        'newprovider' => $data['newprovider'][$i],
                        'curdetails' => $data['curdetails'][$i],
                        'newdetails' => $data['newdetails'][$i],
                        'curenddate' => $curEndDate,
                        'newenddate' => $newEndDate,
                        'usages' => $data['usages'][$i],
                        'term' => $data['term'][$i],
                        'curmonthlycost' => $data['curmonthlycost'][$i]?$data['curmonthlycost'][$i]:0.00,
                        'newmonthlycost' => $data['newmonthlycost'][$i]?$data['newmonthlycost'][$i]:0.00,
                        'benefits' => $data['newsaving'][$i],
                        'curtotalover' => $data['curtotalover'][$i]?$data['curtotalover'][$i]:0.00,
                        'newtotalover' => $data['newtotalover'][$i]?$data['newtotalover'][$i]:0.00,
                        'curcancelfee' => $data['curcancelfee'][$i]?$data['curcancelfee'][$i]:0.00,
                        'newcancelfee' => $data['newcancelfee'][$i]?$data['newcancelfee'][$i]:0.00,
                        'cursaving' => $data['cursaving'][$i]?$data['cursaving'][$i]:0.00,
                        'newsaving' => $data['newsaving'][$i]?$data['newsaving'][$i]:0.00,
                        'newcommissiontype' => $data['newcommissiontype'][$i]?$data['newcommissiontype'][$i]:NULL,
                        'amount' => $data['amountlhc'][$i]?$data['amountlhc'][$i]:0.00,
                        'status' => $status,
                        'planner' => $newEndDate,
                        'exclude' => !empty($data['exclude_hidden'][$i])? $data['exclude_hidden'][$i]:NULL,
                    ]
                );
            }
            $this->messageInvoice($user_id, $invoice_no, 'LHC');
        }
	    return response()->json(['status'=>'save']);
	}

	public function messageInvoice($sendTo='', $invoice_no='', $type=''){
        $messages = new Message([
            'user_id' => Auth::user()->id,
            'msg_from' => Auth::user()->id,
            'msg_to' => $sendTo,
            'invoice_no' => $invoice_no,
            'msg_type' => 'Invoices',
            'name' => Auth::user()->forename.' '.Auth::user()->surname,
            'subject' => $type,
            'message' => 'You have a new invoice',
            'status' => 1,
        ]);
        $messages->save();
    }


}

