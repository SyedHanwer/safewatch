<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Supplier;
use App\Service;
use App\Invoice;
use App\Job;
use App\Lhc;
use DB;


class ManageAjaxController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxViewCash(Request $request)
    {
        $view = $request->view;
        if($view ==='NotYetTracked'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 0)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();

            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 0)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();

            $view = view('ajaxview.NotYetTracked', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='ClaimRaised'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 4)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 4)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();

            $view = view('ajaxview.ClaimRaised', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='Tracked'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 3)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 3)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            $view = view('ajaxview.Tracked', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='Paid'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 2)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 2)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            $view = view('ajaxview.Paid', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='CashDeleted'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 1)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 1)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            $view = view('ajaxview.CashDeleted', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
    }

    public function energySupplier(Request $request)
    {
        $val = $request->val;
        if($val ==='All'){
            $supplier = Supplier::select('name')->where('energy', 1)->where('status', 1)->get();
            $suname = array();
            foreach($supplier as $val){
                $suname[] = $val->name;
            }

            $service = Service::select('id')->whereIn('name', $suname)->get();
            $sid = array();
            foreach($service as $val){
                $sid[] = $val->id;
            }
            $data = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.id',
                    'lhcs.service_id','lhcs.user_id','lhcs.newdetails','lhcs.newtotalover',
                    'lhcs.newsaving','lhcs.newenddate','lhcs.newcancelfee','lhcs.benefits',
                    'lhcs.invoice_no', 'services.name', 'users.forename', 'users.surname')
                ->where('lhcs.status', 1)
                ->where('lhcs.isdeleted', 0)
                ->orWhereIn('lhcs.service_id', $sid)
                ->orWhere(function($query) use ($suname)
                {
                    $query->orWhereIn('lhcs.curprovider', $suname)
                        ->orWhereIn('lhcs.newprovider', $suname);
                })
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }else{
            $service = Service::select('id')->where('name', 'LIKE', "%{$val}%")->get();
            $sid = array();
            foreach($service as $val){
                $sid[] = $val->id;
            }
            $data = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.id',
                    'lhcs.service_id', 'lhcs.curprovider', 'lhcs.newprovider', 'lhcs.user_id','lhcs.newdetails','lhcs.newtotalover',
                    'lhcs.newsaving','lhcs.newenddate','lhcs.newcancelfee','lhcs.benefits',
                    'lhcs.invoice_no', 'services.name', 'users.forename', 'users.surname')
                ->where('lhcs.status', 1)
                ->where('lhcs.isdeleted', 0)
                ->whereIn('lhcs.service_id', $sid)
                ->orWhere('lhcs.curprovider', 'LIKE', "%{$val}%")
                ->orWhere('lhcs.newprovider', 'LIKE', "%{$val}%")
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }
        $html = '';
        if(count($data) !=0) {
            foreach ($data as $val) {
                $html .= '<tr>
                    <td>'.$val->name.'<p class="text-sky">'.$val->forename.' '.$val->surname.'</p></td>
                    <td>'.$val->newdetails.'<p class="text-sky">(ID: '.$val->id.'), '.$val->newtotalover.'</p></td>
                    <td>£'.$val->newsaving.'</td>
                    <td>'.$val->newenddate.'</td>
                    <td>£'.$val->newcancelfee.'</td>
                    <td>'.$val->benefits.'</td>
                    <td>'.$val->invoice_no.'</td>
                </tr>';
            }
        }else{
            $html .= '<tr style="text-align:center;"><td colspan="7">Data Not Found!</td></tr>';
        }
        return response()->json(['status'=>'success', 'data'=>$html]);
        exit();
    }

    public function cashBackStatus(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $status = $request->status;
        if(!empty($id) && $type =='JOBS'){
            Job::where('id', $id)
                ->update([
                    'types' => $status
                ]);
        }
        if(!empty($id) && $type =='LHC'){
            Lhc::where('id', $id)
                ->update([
                    'types' => $status
                ]);
        }
        return 1;
    }

    public function commInvoicePreView(Request $request)
    {
        $data = array();
        $invoiceID = $request->invoiceID;
        $invoices = Invoice::where('id', $invoiceID)->first();
        $data['invoices'] = $invoices;
        if($invoices->invoice_type =='Job'){
            $data['data'] = Job::where('invoice_id', $invoices->id)->get();
        }
        if($invoices->invoice_type =='LHC'){
            $data['data'] = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->select('lhcs.*', 'services.id as sid','services.name as sname')
                ->where('lhcs.invoice_id', $invoices->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }
        return response()->json(['status'=>'success', 'data'=>$data]);
        exit();
    }



}

