<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Contact;

class ContactController extends Controller
{
    public function contactus()
    {
    	return view ('contact');
    }

    public function addcontact(Request $request)
    {
    	$validatedData = $request->validate([
            'name' => 'required',
            'telphone' => 'required',
            'message' => 'required',
        ]);

    	$contact = new Contact();
        $contact->name = $request->input('name');
        $contact->telphone = $request->input('telphone');
        $contact->message = $request->input('message');
        $contact->save();
    	return redirect()->back();
    }
}
