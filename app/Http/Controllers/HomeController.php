<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use App\Message;
use App\User;
use DB;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Gate::allows('isSuperAgent')) {
            $messages = Message::where('msg_to', Auth::user()->id)
                ->where('status', 1)
                ->where('isdeleted', 0)
                ->orderBy('id', 'DESC')
                ->get();
            $tmyclient = User::where('role', 3)
                ->where('membershiptype', 'Paid')
                ->where('parent_id', Auth::user()->id)
                ->count();
            $agents = User::select('id', 'parent_id')
                ->where('role', 2)
                ->where('parent_id', Auth::user()->id)
                ->get();
            $tmyagent = $agents->count();
            $tAgentPaidClient = 0;
            foreach ($agents as $val){
                $agentPaidClient = User::where('role', 3)
                    ->where('membershiptype', 'Paid')
                    ->where('parent_id', $val->id)
                    ->count();
                $tAgentPaidClient = $tAgentPaidClient +$agentPaidClient;
            }

            return view('superAgent_home', compact('messages', 'tmyclient', 'tmyagent','tAgentPaidClient'));
            exit();
        }

        if (Gate::allows('isAgent')) {
            $messages = Message::where('msg_to', Auth::user()->id)
                ->where('status', 1)
                ->where('isdeleted', 0)
                ->orderBy('id', 'DESC')
                ->get();
            $tmyclient = User::where('role', 3)
                ->where('membershiptype', 'Paid')
                ->where('parent_id', Auth::user()->id)
                ->count();
            $agents = User::select('id', 'parent_id')
                ->where('role', 2)
                ->where('parent_id', Auth::user()->id)
                ->get();
            $tmyagent = $agents->count();
            $tAgentPaidClient = 0;
            foreach ($agents as $val){
                $agentPaidClient = User::where('role', 3)
                    ->where('membershiptype', 'Paid')
                    ->where('parent_id', $val->id)
                    ->count();
                $tAgentPaidClient = $tAgentPaidClient +$agentPaidClient;
            }
            return view('home', compact('messages', 'tmyclient', 'tmyagent'));
            exit();
        }

        if (Gate::allows('isClient')) {
            $messages = Message::where('msg_to', Auth::user()->id)
                ->where('status', 1)
                ->where('isdeleted', 0)
                ->orderBy('id', 'DESC')
                ->get();
            return view('client_home', compact('messages'));
            exit();
        }

    }


}
