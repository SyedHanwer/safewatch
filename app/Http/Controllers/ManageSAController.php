<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Supplier;
use App\Service;
use App\Invoice;
use App\Job;
use App\Lhc;
use App\Commission;
use App\Message;
use App\User;
use CommissionCalculationHelper;
use DB;

class ManageSAController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxViewCashSA(Request $request)
    {
        $view = $request->view;
        if($view ==='NotYetTracked'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 0)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();

            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 0)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            
            $view = view('superAgent.NotYetTracked', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='ClaimRaised'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 4)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 4)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            $view = view('superAgent.ClaimRaised', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='Tracked'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 3)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 3)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
         
            $view = view('superAgent.Tracked', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='Paid'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 2)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 2)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            
            $view = view('superAgent.Paid', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }
        if($view ==='CashDeleted'){
            $jobs = DB::table('jobs')
                ->join('users', 'jobs.user_id', '=', 'users.id')
                ->select('jobs.*','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('jobs.comission_type')
                ->where('jobs.amount2', '<>', 0.00)
                ->whereNotNull('jobs.invoice_no')
                ->where('jobs.types', 1)
                ->where('jobs.isdeleted', 0)
                ->where('jobs.job_from', Auth::user()->id)
                ->orderBy('jobs.created_at', 'DESC')
                ->get();
            $lhcs = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.*','services.name','users.id as uid', 'users.forename','users.surname')
                ->whereNotNull('lhcs.newcommissiontype')
                ->where('lhcs.amount', '<>', 0.00)
                ->whereNotNull('lhcs.invoice_no')
                ->where('lhcs.types', 1)
                ->where('lhcs.isdeleted', 0)
                ->where('lhcs.lhc_from', Auth::user()->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
            $merged  = $jobs->merge($lhcs)->sortByDesc('created_at');
            $data = $merged->all();
            
            $view = view('superAgent.CashDeleted', compact('data'))->render();
            return response()->json(['html'=>$view]);
            exit();
        }


    }

    public function energySupplierSA(Request $request)
    {
        $val = $request->val;
        if($val ==='All'){
            $supplier = Supplier::select('name')->where('energy', 1)->where('status', 1)->get();
            $suname = array();
            foreach($supplier as $val){
                $suname[] = $val->name;
            }

            $service = Service::select('id')->whereIn('name', $suname)->get();
            $sid = array();
            foreach($service as $val){
                $sid[] = $val->id;
            }
            $data = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.id',
                    'lhcs.service_id','lhcs.user_id','lhcs.newdetails','lhcs.newtotalover',
                    'lhcs.newsaving','lhcs.newenddate','lhcs.newcancelfee','lhcs.benefits',
                    'lhcs.invoice_no', 'services.name', 'users.forename', 'users.surname')
                ->where('lhcs.status', 1)
                ->where('lhcs.isdeleted', 0)
                ->orWhereIn('lhcs.service_id', $sid)
                ->orWhere(function($query) use ($suname)
                {
                    $query->orWhereIn('lhcs.curprovider', $suname)
                        ->orWhereIn('lhcs.newprovider', $suname);
                })
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }else{
            $service = Service::select('id')->where('name', 'LIKE', "%{$val}%")->get();
            $sid = array();
            foreach($service as $val){
                $sid[] = $val->id;
            }
            $data = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->join('users', 'lhcs.user_id', '=', 'users.id')
                ->select('lhcs.id',
                    'lhcs.service_id', 'lhcs.curprovider', 'lhcs.newprovider', 'lhcs.user_id','lhcs.newdetails','lhcs.newtotalover',
                    'lhcs.newsaving','lhcs.newenddate','lhcs.newcancelfee','lhcs.benefits',
                    'lhcs.invoice_no', 'services.name', 'users.forename', 'users.surname')
                ->where('lhcs.status', 1)
                ->where('lhcs.isdeleted', 0)
                ->whereIn('lhcs.service_id', $sid)
                ->orWhere('lhcs.curprovider', 'LIKE', "%{$val}%")
                ->orWhere('lhcs.newprovider', 'LIKE', "%{$val}%")
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }
        $html = '';
        if(count($data) !=0) {
            foreach ($data as $val) {
                $html .= '<tr>
                    <td>'.$val->name.'<p class="text-sky">'.$val->forename.' '.$val->surname.'</p></td>
                    <td>'.$val->newdetails.'<p class="text-sky">(ID: '.$val->id.'), '.$val->newtotalover.'</p></td>
                    <td>£'.$val->newsaving.'</td>
                    <td>'.$val->newenddate.'</td>
                    <td>£'.$val->newcancelfee.'</td>
                    <td>'.$val->benefits.'</td>
                    <td>'.$val->invoice_no.'</td>
                </tr>';
            }
        }else{
            $html .= '<tr style="text-align:center;"><td colspan="7">Data Not Found!</td></tr>';
        }
        return response()->json(['status'=>'success', 'data'=>$html]);
        exit();
    }

    public function cashBackStatusSA(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $status = $request->status;
        if(!empty($id) && $type =='JOBS'){
            Job::where('id', $id)
                ->update([
                    'types' => $status
                ]);
        }
        if(!empty($id) && $type =='LHC'){
            Lhc::where('id', $id)
                ->update([
                    'types' => $status
                ]);
        }
        return 1;
    }

    public function commissionMSA(Request $request)
    {
        $id = $request->cid;
        $ctype = $request->ctype;

        $commissions = Commission::where('id', $id)->first();
        if($ctype ==='invoices' && !empty($commissions)){
            Commission::where('invoice_no', $commissions->invoice_no)
                ->update([
                    'status' => 1
                ]);
            if(!empty($commissions->invoice_id)){
                Invoice::where('id', $commissions->invoice_id)
                    ->update([
                        'status' => 1
                    ]);
            }
            $messages = new Message([
                'user_id' => Auth::user()->id,
                'msg_from' => Auth::user()->id,
                'msg_to' => $commissions->agent,
                'invoice_no' => $commissions->invoice_no,
                'msg_type' => 'safewatch',
                'name' => Auth::user()->forename.' '.Auth::user()->surname,
                'subject' => 'SafeWatch',
                'message' => 'Commission Earned',
                'status' => 1,
            ]);
            $messages->save();
        }
        if($ctype ==='agent'  && !empty($commissions->id)){
            CommissionCalculationHelper::commissionCalculateAgentFun($commissions->id);
            $messages = new Message([
                'user_id' => Auth::user()->id,
                'msg_from' => Auth::user()->id,
                'msg_to' => $commissions->agent,
                'msg_type' => 'safewatch',
                'name' => Auth::user()->forename.' '.Auth::user()->surname,
                'subject' => 'SafeWatch',
                'message' => 'Commission Paid',
                'status' => 1,
            ]);
            $messages->save();
            Commission::where('id', $commissions->id)
                ->update([
                    'status' => 2
                ]);
        }
        return 1;
    }

    public function agentDetailsSA(Request $request)
    {
        $id = $request->id;
        $data = User::where('id',$id)->first();

        $tAgClient = User::where('role', 3)
            ->where('parent_id', $id)
            ->count();
        $tAgent = User::where('role', 2)
            ->where('parent_id', $id)
            ->count();

        $agent = User::select('id')->where('role', 2)
            ->where('parent_id', $id)
            ->get();
        $tAgCli = 0;
        foreach ($agent as $val){
            $cli = User::where('role', 3)
                ->where('parent_id', $val->id)
                ->count();
            $tAgCli = $tAgCli + $cli;
        }

        return response()->json(['status'=>'success', 'data'=>$data, 'tClient'=>$tAgClient,'tAgent'=>$tAgent,'tAgCli'=>$tAgCli]);
        exit();
    }

    public function commInvoicePreViewSA(Request $request)
    {
        $data = array();
        $invoiceID = $request->invoiceID;
        $invoices = Invoice::where('id', $invoiceID)->first();
        $data['invoices'] = $invoices;
        if($invoices->invoice_type =='Job'){
            $data['data'] = Job::where('invoice_id', $invoices->id)->get();
        }
        if($invoices->invoice_type =='LHC'){
            $data['data'] = DB::table('lhcs')
                ->join('services', 'lhcs.service_id', '=', 'services.id')
                ->select('lhcs.*', 'services.id as sid','services.name as sname')
                ->where('lhcs.invoice_id', $invoices->id)
                ->orderBy('lhcs.created_at', 'DESC')
                ->get();
        }
        return response()->json(['status'=>'success', 'data'=>$data]);
        exit();
    }



}

