<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Job;
use App\Lhc;
use App\User;
use App\Invoice;
use App\Commission;
use App\Message;
use Auth;
use EmailHelper;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editInvoice(Request $request)
    {
        $id = $request->id;
        $invoice_type = $request->invoice_type;

        $Invoice = Invoice::where('id',$id)->first();
        $invoice_no = $Invoice->invoice_no;
        if(!empty($invoice_no)){
            Message::where('invoice_no', $invoice_no)->where('msg_type', 'Invoices')->delete();
        }

        Invoice::where('id', $id)->delete();
        Commission::where('invoice_id', $id)->delete();
        if(!empty($invoice_type) && $invoice_type =='Job'){
            Job::where('invoice_id', $id)
                ->update([
                    'status' => 2
                ]);
            return 1;
        }
        if(!empty($invoice_type) && $invoice_type =='LHC'){
            Lhc::where('invoice_id', $id)
                ->update([
                    'status' => 2
                ]);
            return 1;
        }
    }

    public function addagent(Request $request)
    {
        $data = $_POST['data'];
        if(!empty($data) && $data['status']== 'update'){
            $update = User::where('id',$data['id'])
                ->update([
                    'forename' => $data['forename'],
                    'surname' => $data['surname'],
                    'client_id' => $data['client_id'],
                    'dob' => $data['dob'],
                    'address1' => $data['address1'],
                    'home_no' => $data['home_no'],
                    'dateoffirstcontact' => $data['dateoffirstcontact'],
                    'address2' => $data['address2'],
                    'mobile_no' => $data['mobile_no'],
                    //'dateofmembersince' => $data['dateofmembersince'],
                    'address3' => $data['address3'],
                    'email' => $data['email'],
                    'preferred_contact' => $data['preferred_contact'],
                    'postcode' => $data['postcode'],
                    'referredby' => $data['referredby'],
                    'notes' => $data['notes'],
                ]);
            if($update){
                return response()->json(['status'=>'update']);
            }
        }else if(!empty($data) && $data['status'] == 'save'){

            $randomString = md5(rand(1, 10) . microtime());
            $user = new User([
                'forename' => $data['forename'],
                'surname' => $data['surname'],
                'client_id' => rand(1000,9999),
                'address1' => $data['address1'],
                'home_no' => $data['home_no'],
                'address2' => $data['address2'],
                'mobile_no' => $data['mobile_no'],
                'address3' => $data['address3'],
                'email' => $data['email'],
                // 'preferred_contact' => $data['preferred_contact'],
                // 'postcode' => $data['postcode'],
                'referredby' => $data['referredby'],
                //  'notes' => $data['notes'],
                'status' => 1,
                'role' => 2,
                'parent_id' => Auth::user()->id,
                'tiers' => Auth::user()->tiers+1,
                'passwordToken' => $randomString,
            ]);
            $user->save();
            EmailHelper::sendEmailAgentReg($data['email'], $data['forename'], $data['surname'], $randomString);
            return response()->json(['status'=>'save']);
        }else {
            return response()->json(['status'=>'error']);
        }
    }


}
