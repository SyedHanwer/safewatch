<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*****General Route**********/
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contactus', 'ContactController@contactus')->name('contactus');
Route::post('/addcontact', 'ContactController@addcontact')->name('addcontact');
Route::post('/autoRandomNo', 'CommonController@autoRandomNo')->name('autoRandomNo');
Route::get('/registration/complete/{token}', 'Auth\RegisterController@regComplete')->name('registration.complete');
Route::post('/registrationSave', 'Auth\RegisterController@registrationSave')->name('registrationSave');

/*****Agent Route Start**********/
Route::post('/ajaxView', 'AjaxController@ajaxView')->name('ajaxView');
Route::post('/ajaxViewClient', 'AjaxController@ajaxViewClient')->name('ajaxViewClient');
Route::post('/ajaxViewCash', 'ManageAjaxController@ajaxViewCash')->name('ajaxViewCash');
Route::post('/jobdelete', 'AjaxController@jobdelete')->name('jobdelete');
Route::post('/viewclient', 'AjaxController@viewclient')->name('viewclient');
Route::post('/clientupdate', 'AjaxController@clientupdate')->name('clientupdate');
Route::post('/jobsave', 'AjaxController@jobsave')->name('jobsave');
Route::post('/addagent', 'AgentController@addagent')->name('addagent');
Route::post('/lhcsave', 'AjaxController@lhcsave')->name('lhcsave');
Route::post('/lhcdelete', 'AjaxController@lhcdelete')->name('lhcdelete');
Route::post('/updateproduct', 'AjaxController@updateproduct')->name('updateproduct');
Route::post('/jobPrepareInvoice', 'AjaxController@jobPrepareInvoice')->name('jobPrepareInvoice');
Route::post('/lhcPrepareInvoice', 'AjaxController@lhcPrepareInvoice')->name('lhcPrepareInvoice');
Route::post('/editInvoice', 'AgentController@editInvoice')->name('editInvoice');

Route::get('/editegent', 'AjaxController@agentview')->name('editegent');
Route::post('/agentview', 'AjaxController@agentview')->name('agentview');
Route::post('/agentdelete', 'AjaxController@agentDelete')->name('agentdelete');
Route::post('/sendmsgtoclient', 'AjaxController@sendmsgtoclient')->name('sendmsgtoclient');
Route::post('/commrequest', 'AjaxController@commrequest')->name('commrequest');

Route::post('/energySupplier', 'ManageAjaxController@energySupplier')->name('energySupplier');
Route::post('/cashBackStatus', 'ManageAjaxController@cashBackStatus')->name('cashBackStatus');
Route::post('/commInvoicePreView', 'ManageAjaxController@commInvoicePreView')->name('commInvoicePreView');
/*****Agent Route End's**********/


/*****Client Route Start**********/
Route::post('/clientAjaxView', 'ClientAjaxController@clientAjaxView')->name('clientAjaxView');
Route::post('/sendcontactmsg', 'ClientAjaxController@sendcontactmsg')->name('sendcontactmsg');
Route::post('/msgSendToSW', 'ClientAjaxController@msgSendToSW')->name('msgSendToSW');
Route::post('/invoicePreView', 'ClientAjaxController@invoicePreView')->name('invoicePreView');

/*****Client Route End's**********/


/*****Super Agent Route Start**********/
Route::post('/ajaxViewSA', 'SuperAgentController@ajaxViewSA')->name('ajaxViewSA');
Route::post('/ajaxViewClientSA', 'SuperAgentController@ajaxViewClientSA')->name('ajaxViewClientSA');
Route::post('/ajaxViewCashSA', 'ManageSAController@ajaxViewCashSA')->name('ajaxViewCashSA');
Route::post('/jobdeleteSA', 'SuperAgentController@jobdeleteSA')->name('jobdeleteSA');
Route::post('/viewclientSA', 'SuperAgentController@viewclientSA')->name('viewclientSA');
Route::post('/clientupdateSA', 'SuperAgentController@clientupdateSA')->name('clientupdateSA');
Route::post('/jobsaveSA', 'SuperAgentController@jobsaveSA')->name('jobsaveSA');
Route::post('/addagentSA', 'AgentSAController@addagentSA')->name('addagentSA');
Route::post('/lhcsaveSA', 'SuperAgentController@lhcsaveSA')->name('lhcsaveSA');
Route::post('/lhcdeleteSA', 'SuperAgentController@lhcdeleteSA')->name('lhcdeleteSA');
Route::post('/updateproductSA', 'SuperAgentController@updateproductSA')->name('updateproductSA');
Route::post('/jobPrepareInvoiceSA', 'SuperAgentController@jobPrepareInvoiceSA')->name('jobPrepareInvoiceSA');
Route::post('/lhcPrepareInvoiceSA', 'SuperAgentController@lhcPrepareInvoiceSA')->name('lhcPrepareInvoiceSA');
Route::post('/editInvoiceSA', 'AgentSAController@editInvoiceSA')->name('editInvoiceSA');
Route::get('/editegentSA', 'SuperAgentController@agentviewSA')->name('editegentSA');
Route::post('/agentviewSA', 'SuperAgentController@agentviewSA')->name('agentviewSA');
Route::post('/agentdeleteSA', 'SuperAgentController@agentDeleteSA')->name('agentdeleteSA');
Route::post('/sendmsgtoclientSA', 'SuperAgentController@sendmsgtoclientSA')->name('sendmsgtoclientSA');
Route::post('/commrequestSA', 'SuperAgentController@commrequestSA')->name('commrequestSA');
Route::post('/energySupplierSA', 'ManageSAController@energySupplierSA')->name('energySupplierSA');
Route::post('/cashBackStatusSA', 'ManageSAController@cashBackStatusSA')->name('cashBackStatusSA');
Route::post('/commissionMSA', 'ManageSAController@commissionMSA')->name('commissionMSA');
Route::post('/agentDetailsSA', 'ManageSAController@agentDetailsSA')->name('agentDetailsSA');
Route::post('/commInvoicePreViewSA', 'ManageSAController@commInvoicePreViewSA')->name('commInvoicePreViewSA');
/*****Super Agent Route End's**********/
